<?php

//TODO: rename this users-to-s4

require_once '../wp-load.php';

global $wpdb;

$sql_post = "";
$sql_roles = "";

$t1 = microtime(true);
$roles = array_filter(get_option('wp_user_roles', []), function($role) {    
    return isset($role['capabilities'])
    && isset($role['capabilities']['perform_coaching'])
    && $role['capabilities']['perform_coaching'];
});

$arr_roles = array_keys($roles);

array_push($arr_roles, 'subscriber');
//echo "<pre>". print_r($arr_roles, true) . "</pre>";

foreach($arr_roles as $roles) {
    $sql_roles .= " AND h.meta_value not like '%".$roles."%' ";
};

/*$sql = "SELECT
        a.ID id
        ,         
        user_login emailaddress1, 
        fn.meta_value cust_firstname, 
        ln.meta_value cust_lastname, 
        pho.meta_value telephone1, 
        h.meta_value cust_currentmasterylevel, 
        tags.meta_value tags,
        isid.meta_value cust_iscontactid,
        insDyn.meta_value _dyn
     FROM
        wp_users a
        left join wp_usermeta fn on (a.ID = fn.user_id and fn.meta_key like 'first_name')
        left join wp_usermeta ln on (a.ID = ln.user_id and ln.meta_key like 'last_name')
        left join wp_usermeta isid on (a.ID = isid.user_id and isid.meta_key like 'Id')
        left join wp_usermeta pho on (a.ID = pho.user_id and pho.meta_key like 'Phone1')
        #left join wp_bp_xprofile_data j on (a.ID = j.user_id and j.field_id = 5)
        #inner JOIN wp_usermeta g on (a.ID = g.user_id and g.meta_key like 'Groups' $sql_post)
        inner JOIN wp_usermeta tags on (a.ID = tags.user_id and tags.meta_key like 'Groups')
        left JOIN wp_usermeta insDyn on (a.ID = insDyn.user_id and insDyn.meta_key = '_dyn') 
        inner JOIN wp_usermeta h on (a.ID = h.user_id and h.meta_key like 'wp_capabilities' $sql_roles)
        
        WHERE tags.meta_value not like '2623,%'
        
        GROUP BY user_login
        having _dyn is null 
        ORDER BY a.user_registered DESC

        LIMIT 10
        ";
*/

$sql = "
SELECT a.ID id 
FROM wp_users a 
INNER JOIN wp_usermeta tags on (a.ID = tags.user_id and tags.meta_key like 'Groups')
#LEFT JOIN wp_usermeta insDyn on (a.ID = insDyn.user_id and insDyn.meta_key = '_dyn') 
LEFT JOIN wp_usermeta insDyn on (a.ID = insDyn.user_id and insDyn.meta_key = '_ins4') 
WHERE (tags.meta_value not like '2623,%' AND tags.meta_value not like '%,2623,%')
AND (insDyn.umeta_id is null OR insDyn.meta_value <> 1)
LIMIT 40;
";

 // die('<pre>'.$sql . '</pre>');
$arr = $wpdb->get_results($sql, ARRAY_A);

$partnerTag = 2623;

function cleanLevel($levelString) {
    $la = unserialize($levelString);    

    foreach ($la as $key => $value) {
        if (strpos('have_member_listing', $key) !== false) continue;

        $l = str_replace('js', 'Jumpstart ', $key);
        $l = str_replace('fb_', '', $l);
        $l = str_replace('_', ' ', $l);
        $l = ucwords($l);
    }

    return $l;
}

$FB_ED = FB_EXP_DATES::getInstance();

function cleanCurrentExpirationDates($userId) {
    global $FB_ED;

    $all_FB_EDs = $FB_ED->get_all_user_expiration_dates($userId);

    $currentExpirationDates = [];

    $includeEDs = [
        'lms_exp',
        'mastery_exp',
        'mind_protein_exp',
        'ic_advisory_exp',
        'vt_exp',
        'deal_rs_exp',
        'live_events_exp',
        'live_event_replays_exp',
        'academies_exp',
        'mp_iq_content_exp'
    ];

    foreach ($all_FB_EDs as $key => $value) {
        if (in_array($key, $includeEDs)) {
            //dynamics team request, please confirm
            $value = str_replace('2030', '2075', $value);
            $currentExpirationDates[$key] = $value;
        }
    }

    return $currentExpirationDates;
}

$fbExpDates = 

//will only pick this type:
$processUserType = 'primary';
$c = 0;
$exCount = 0;
$exTest = 0;
$exSubscribers = 0;
$exNoTags = 0;
foreach ($arr as $u) {    

    $user = get_user_by('ID', $u['id']);

    //skip test accounts
    if ( strpos($user->user_login, 'wholesalingprofit.com') != false || $user->ID == "1" ) {
        $exTest ++;
        continue; //subs are recent users
    }

    $umeta = get_user_meta($user->ID);

    $isId = isset($umeta['Id']) && isset($umeta['Id'][0]) ? $umeta['Id'][0] : 0;
    $phone = isset($umeta['Phone1']) && isset($umeta['Phone1'][0]) ? $umeta['Phone1'][0] : 0;
    $level = cleanLevel($umeta['wp_capabilities'][0]);

    $sTags = isset($umeta['Groups']) && isset($umeta['Groups'][0]) ? $umeta['Groups'][0] : '';
    $aTags = explode(',', $sTags);
    
    $customerType = in_array($partnerTag, $aTags) ? 'partner' : 'primary';

    if( $level == 'Subscriber') {
        $exSubscribers ++;
        continue;
    }

    if (count($aTags) == 0) {
        $exNoTags ++;
        continue;
    }

    //excludes partners
    if ($customerType != $processUserType) { 
        $exCount ++;
        continue;
    }


    $originalDates = cleanCurrentExpirationDates($user->ID);

    $dateList = listExpirablePermissions($user, $originalDates);

    $hasDates = count($dateList) > 0;

    if ($hasDates) {

        //PREP
        $dynData = [
            'emailaddress1' => $user->user_email, 
            'cust_firstname' => $user->first_name, 
            'cust_lastname' => $user->last_name, 
            'telephone1' => $phone, 
            'cust_currentmasterylevel' => $level, 
            'cust_iscontactid' => $isId,
            'customertypecode' => $customerType,
            'expirablePermissions' => $dateList
        ];

        echo "<pre>". print_r($dynData, true) . "</pre>";
        //CALL DYN
        //echo "<pre>". print_r($dynData, true) . "</pre>";
        $c ++;
    }

    //$inserted = push_user_to_s4($user);
    // if ($inserted)
    //     $c ++;

    // $r1 = [];
    // if ($inserted)
    //     $r1 = load_s4_data($user->ID);

    // if (isset($r1['expirablePermissions']) && count($r1['expirablePermissions']) > 0) {
    //     update_user_meta($user->ID, '_dyn', true);

    //     //PREP
    //     $dynData = [
    //         'emailaddress1' => $user->user_email, 
    //         'cust_firstname' => $user->first_name, 
    //         'cust_lastname' => $user->last_name, 
    //         'telephone1' => $phone, 
    //         'cust_currentmasterylevel' => $level, 
    //         'cust_iscontactid' => $isId,
    //         'customertypecode' => $customerType,
    //         'expirablePermissions' => $r1['expirablePermissions']
    //     ];
    //     echo "<pre>". print_r($dynData, true) . "</pre>";
    //     $c ++;
    // }

    // $dynData = [
    //     'emailaddress1' => $user->user_email, 
    //     'cust_firstname' => $user->first_name, 
    //     'cust_lastname' => $user->last_name, 
    //     'telephone1' => $phone, 
    //     'cust_currentmasterylevel' => $level, 
    //     'cust_iscontactid' => $isId,
    //     'customertypecode' => $customerType,
    //     'expirablePermissions' => []
    // ];  
    // echo "<pre>". print_r($dynData, true) . "</pre>";  
    // $c ++;
}

$t2 = microtime(true);

echo "Total imported (inserted) $c";
echo "<br/>";
echo "Excluded Non '$processUserType' $exCount";
echo "<br/>";
echo "Excluded Test Accounts $exTest";
echo "<br/>";
echo "Excluded No Tags $exNoTags";
echo "<br/>";
$tx1 = ($t2 - $t1) . ' sec.';
echo "<br/><br/>";
echo $tx1;
