<?php 

include_once('../../wp-load.php');
// http://members1.fortunebuilders.com/wp-lms-test-scripts/reschedule-api/sim-open-calendar-page.php?token=82a8548160da228de227870d938cc882612ce62a
// read params:
// if token present -- read
// use API getToken -- that will tell call info -- and if we're good to reschedule
// use API get used tokens for the current call or create another entry to keep history
	// reschedule history:
	// cancelledid => newId ----> cancelledNewId => newerId ----> this is weird ---> where to store it


//read token
if ( !isset( $_GET['token'] ) ) {
	die('No Token Found. Cannot simulate reschedule.');
}

$token = $_GET['token'];

//load token
$urlPart = '/token/' . $token . '/'; //UH I need this trailing slash???
$url = S4_API_URL . $urlPart;

$res = FB_Curl::get($url);

// grab the info
echo "<pre>" . print_r($res, true ) . "</pre>";

//validate is used or expiration
$today = date('Y-m-d');
$dToday = new DateTime( $today );
$dExp = new DateTime( $res['expiration_date'] );

if ( $dToday > $dExp || $res['is_used'] ) {
	die(' Expired or Used!');
}

//validate student 
$student_id = 47611; //will be grabbed from get_current_user_id();
$studentOK = $res['call_info']['student_id'] == $student_id;

echo "Student OK: $studentOK";
echo "<br/>";

$resCoachId = $res['call_info']['coach_id'];
echo "Selecting Coach: " . $resCoachId;
echo "<br/>";

$resCallId = $res['call_info']['id'];

echo "Call ID: " . $resCallId;
echo "<br/>"; // check comps! need times (cancelled at) (cancelled by)
// --- at this point -- calendar page is open and call is ready to be scheduled

// simulate reschedule
// status: 1
// reschedules_call_id: resCallId
// call API to 'userToken'
echo "<br/>";
echo "<a href='sim-reschedule-call.php'>RESCHEDULE!</a>";
// when rescheduling, do I need an open slot? or can I open a new one?


// if rescheduled successfuly -- store last call --- 


// on cancel -- store any existing history -- in the payload
// find Cancellation History -- among tokens? --- get Cancellation History
// from used tokens -- retrieve payloads -- 

// on reschedule -- used_token to be attached -- in activity?
// in call reschedules --- this call reschedules id
// on book




// upcoming calls: 
// - was this call cancelled -- has used token -- 
// - load history in payload
