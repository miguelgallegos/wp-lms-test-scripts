<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
ini_set('error_reporting', E_ALL);

require_once '../../wp-load.php';

//$cid = 325274;
$cid = $_GET['ccid'];

$coachingReminder = fb_srv('fb_coaching.email_reminder');

if ( isset($_GET['token']) ) {
	$token = $_GET['token'];
	$coachingReminder->setCancellationToken( $token );
}

$coachingReminder->scheduleCancel( $cid );

// $coachingReminder->cancel($cid);
//$coachingReminder->confirm($cid);

echo "<h3>DELAYED Cancellation Email Sent for CID: $cid";

die();