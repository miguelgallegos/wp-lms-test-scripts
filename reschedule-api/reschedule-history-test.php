<?php 


$calls = [
	[
		'id' => 0,
		'coach_id' => 1,
		'student_id' => 1,
		'status' => 2,
		'type' => 'calls',
		'reschedules_call_id' => null,
		'simple_date' => 'Monday',
	],
	[
		'id' => 1,
		'coach_id' => 1,
		'student_id' => 1,
		'status' => 2,
		'type' => 'calls',
		'reschedules_call_id' => 0,
		'simple_date' => 'Tuesday',
	],
	[
		'id' => 2,
		'coach_id' => 1,
		'student_id' => 1,
		'status' => 2,
		'type' => 'calls',
		'reschedules_call_id' => 1,
		'simple_date' => 'Wed',		
	],
	[
		'id' => 3,
		'coach_id' => 1,
		'student_id' => 1,
		'status' => 2,
		'type' => 'calls',
		'reschedules_call_id' =>2,
		'simple_date' => 'Thur',
	],
	[
		'id' => 4,
		'coach_id' => 1,
		'student_id' => 1,
		'status' => 1,
		'type' => 'calls',
		'reschedules_call_id' => 3,
		'simple_date' => 'Friday',

	]

];

foreach ($calls as $call) {

	//show upcoming - booked
	if ( $call['status'] != 1) continue; 
		
	echo "Upcoming: ";
	echo '>> '.$call['id'] . ' <<';

	$hCalls = getCancellationHistory( $call, [] );

	echo "<br/>";
	// echo "<pre>" . print_r($hCalls, true ) . "</pre>";

	echo 'History: ';
	echo "<br/>";
	foreach ($hCalls as $hCall) {
		echo ' -- [' . $hCall['id'] . ']';
		echo ' Cancelled: ' . $hCall['simple_date'];
		echo "<br/>";
	}

}
// reschedules_seq_id -- 
// new thought -- the payload will contain a list of IDs that belong to each cancelled call for this rescheduled
// no recursion will be needed, just append to payload the last cancelled id --- YEAH!
// no meta will be needed -- just id and details will be calculated on load 
// SCRATCH ALL ABOVE -- need to payload can't take cancellation history, it should go directly on the calls table! period!
// recursion?
// OK - in real life:
// query all cancelled calls up to three generations -- then view all history -- or load more button -- OK
function getCancellationHistory( $call_, $calls_ ) {
	global $calls; //just ref

	if ( null == $call_['reschedules_call_id'] ) return $calls_;

	$rCall = $calls[ $call_['reschedules_call_id'] ];

	$calls_[] = $rCall;

	return getCancellationHistory( $rCall, $calls_ );
}
