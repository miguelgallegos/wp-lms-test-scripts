<?php 

include_once('../../wp-load.php');

//read token
if ( !isset( $_GET['token'] ) ) {
	die('No Token Found. Cannot simulate reschedule.');
}

$token = $_GET['token'];

//load token
$urlPart = '/token/' . $token . '/'; //UH I need this trailing slash???
$url = S4_API_URL . $urlPart;

$res = FB_Curl::get($url);

// THIS SHOULD BE AJAX --- goes to S2 too
// ajax decomposes data the same way as angular and it's used to book, store stuff and make updates (including API)
// need: 
// call id
// student id
// coach id
// call type -- given by the token -- app will also provide it 
// reschedules_call_id
// use the call service from S2 here! add additional data (reschedules_call_id: old_call_id)
// the 'reschedules_call_id' shoud be capable of performing recursion or traversing of all linked calls in history of rescheduling till getting to the current call (last rescheduled)


// grab the info
echo "<pre>" . print_r($res, true ) . "</pre>";

//validate is used or expiration
$today = date('Y-m-d');
$dToday = new DateTime( $today );
$dExp = new DateTime( $res['expiration_date'] );

if ( $dToday > $dExp || $res['is_used'] ) {
	die(' Expired or Used!');
}

//validate student 
$student_id = 47611; //will be grabbed from get_current_user_id();
$studentOK = $res['call_info']['student_id'] == $student_id;

echo "Student OK: $studentOK";
echo "<br/>";

$resCoachId = $res['call_info']['coach_id'];
echo "Selecting Coach: " . $resCoachId;
echo "<br/>";

$resCallId = $res['call_info']['id'];

echo "Call ID: " . $resCallId;
echo "<br/>"; 

// CASES TO ADD THIS LOGIC:
// -- THIS HAPPENS in AJAX -- maybe in S2 and bang? no... leave it to wp
// open book
// book

// coaching-call-ajax.php -- (doOpenAndBook)
// calendar_widget (fb_srv_bookACall)

