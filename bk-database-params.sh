#!/bin/bash
#LOCAL

PASSWORD=guFCbn5zPWqz8PD

SSH_HOST=$1
HOST=$2
#HOST=mastery-$2.cbo19mwhsqto.us-west-2.rds.amazonaws.com
#mastery-aurora-cluster.cluster-cbo19mwhsqto.us-west-2.rds.amazonaws.com
USER=mastery_db_admin
DATABASE=mastery
# DB_FILE=dump-$2-$3.sql
# $3 can be formatted as INS_DATETIME
DB_FILE=dump-WP-$3.sql

# TODO
#NEW_WP_DB=$3



### example:
### ./bk-database-params.sh 18.236.36.179 qa-rds 2020-01-21_01
### ./bk-database-params.sh ssh.qa.fortunebuildersdev.com mastery-aurora-cluster.cluster-cbo19mwhsqto.us-west-2.rds.amazonaws.com 2020-01-21_01 <<< jun 20 - "aurora-cluster" for QA
## jun 20:
# mastery-aurora-cluster.cluster-cbo19mwhsqto.us-west-2.rds.amazonaws.com
# API: fb-api-qa-rds-cluster.cluster-cbo19mwhsqto.us-west-2.rds.amazonaws.com
#stuct only, no data
EXCLUDED_TABLES=(
service_activity_log
wp_lpt
wp_oauth_refresh_tokens
wp_oauth_access_tokens
wp_fb_coaching_user_notes 
wp_swp_log
)
#wp_fb_coaching_user_notes not so sure

EXCLUDED_FULL_TABLES=(
archive_jose_possible_unmatched_calls
archive_old_call_notes
archive_old_call_notes_postmeta
archive_old_calls
archive_old_calls_notes_p2p
archive_old_calls_postmeta
archive_old_user_notes
archive_old_user_notes_postmeta
archive_usermeta_expiration_dates
archive_usermeta_no_users
archive_vendors_postmeta
archive_vendors_posts
panche_test_ajax_time
temp_wp_posts_vimeo_forward
temp_wp_posts_vimeo_rollback
)	

#ignore completely

IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_TABLES[@]}"
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done

FULL_IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_FULL_TABLES[@]}"
do :
   FULL_IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done


echo "Dump structure"
# echo "ssh ubuntu@${SSH_HOST} -i /Users/miguel/Downloads/mastery-dev-key.pem mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --no-data ${FULL_IGNORED_TABLES_STRING} ${DATABASE} > ${DB_FILE}"
/Applications/XAMPP/bin/mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --no-data ${FULL_IGNORED_TABLES_STRING} ${DATABASE} > ${DB_FILE}

echo "Dump content"
# echo "ssh ubuntu@${SSH_HOST} -i /Users/miguel/Downloads/mastery-dev-key.pem mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} --no-create-info ${IGNORED_TABLES_STRING} ${FULL_IGNORED_TABLES_STRING} >> ${DB_FILE}"
/Applications/XAMPP/bin/mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} --no-create-info ${IGNORED_TABLES_STRING} ${FULL_IGNORED_TABLES_STRING} --max_allowed_packet=1G --single-transaction --quick --lock-tables=false >> ${DB_FILE}


#DO THE IMPORT HERE!!
