<?php 

include '../wp-load.php';

$ss = [
	"select 'UPDATE fb_lms_learningobject SET content_url = ', quote(meta_value), ' WHERE id = ', post_id, ';' from wp_postmeta where (meta_key like 'fblms_phase_downloads' OR meta_key like 'fblms_lesson_content') and meta_key <> '';",

	"select 'UPDATE fb_lms_learningobject SET learning_time = ', meta_value, ' WHERE id = ', post_id, ';' from wp_postmeta where (meta_key = 'fblms_lesson_content_length' or meta_key = 'fblms_course_length' or meta_key = 'length') and meta_value <> '' and meta_value > 0;",

	"select 'UPDATE fb_lms_learningobject SET learning_time = ', meta_value * 3600, ' WHERE id = ' , post_id,';' from wp_postmeta where meta_key = 'fblms_phase_learning_hours';",

	"select 'UPDATE fb_lms_learningobject SET sort = ', meta_value , ' WHERE id = ', p2p_to, ';' from wp_p2p p2p inner join wp_p2pmeta p2m on p2m.`p2p_id` = p2p.`p2p_id`;",
		
	"select 'UPDATE IGNORE fb_lms_learningobject SET instructor_id = ',u.ID iid,' WHERE id = ',p.ID pid,';' from wp_posts p inner join wp_users u on p.post_author = u.ID where 1 and post_author not in (0, 1) and post_status = 'publish' and post_type in ('fblms_course');", // #, 'fblms_lesson', 'fblms_program', 'fblms_action_plan', 'fblms_task', 'fblms_chapter' order by p.post_modified desc;

	"select 'UPDATE fb_lms_learningobject SET featured_image_url = ',quote(pi.guid) featured_image_url,' WHERE id = ',p.ID id,';' from wp_posts p inner join wp_postmeta pm1 on pm1.post_id = p.ID and pm1.meta_key = '_thumbnail_id' inner join wp_posts pi on pi.ID = pm1.meta_value;",

	"select 'UPDATE fb_lms_learningobject SET description = ', quote(post_content), ' WHERE id = ', ID pid, ';' from wp_posts where post_type in ('fblms_action_plan', 'fblms_task') and post_content <> '';",

	"select 'UPDATE fb_lms_learningobject SET description = ', quote(meta_value), ' WHERE id = ', post_id, ';' from wp_postmeta where meta_key = 'fblms_course_description_id' and meta_value <> '';",

	"select 'UPDATE fb_lms_learningobject SET content_url = ', quote(meta_value) , ' WHERE id = ', post_id id, ';' from wp_postmeta where meta_key = 'fblms_resource_url';",
	
	"select 'INSERT IGNORE INTO fb_playlist (id, title, description, slug, is_public, created_at, deleted_at) VALUES ', c.id, quote(c.title), quote(c.description), quote(c.slug), c.is_public, quote(c.created_at), quote(c.deleted_at) from fb_collection c;", //rem slug on this version

	"select 'INSERT IGNORE INTO fb_playlistitem (playlist_id, sort, created_at, deleted_at, learning_object_id) VALUES ', collection_id playlist_id, sort, quote(created_at), quote(deleted_at), resource_id learning_object_id from fb_collection_item;",

	"select 'INSERT IGNORE INTO fb_user_playlist (playlist_id, user_id) VALUES ', user_id, collection_id from fb_collection_tracking;",
	
	"select 'INSERT IGNORE INTO fb_lms_category (id, name, slug) VALUES ', t.term_id id, quote(t.name), quote(t.slug) from wp_term_taxonomy tt inner join wp_terms t on t.term_id = tt.term_id where tt.taxonomy = 'fblms_course_cats';",

	"select 'INSERT IGNORE INTO fb_lms_learningobject_category (learning_object_id, category_id) VALUES ', tr.object_id learning_object_id, tt.term_taxonomy_id category_id from wp_term_relationships tr inner join `wp_term_taxonomy` tt on tr.term_taxonomy_id = tt.`term_taxonomy_id` where tr.object_id > 0 and tt.`taxonomy` = 'fblms_course_cats';",

	"select 'INSERT IGNORE INTO fb_bookmark (user_id, learning_object_id, created_at) VALUES ', user_id, meta_value learning_object_id, quote(now()) created_at from wp_usermeta where meta_key = '_fb_user_bookmark';",
	
	"select 'INSERT IGNORE INTO fb_learningobject_learningobject (learning_object_source, learning_object_target) VALUES  ', p2p_from, p2p_to from wp_p2p",

	// "select 'INSERT IGNORE INTO fb_learningnote (id, user_id, learning_object_id, title, text, created_at, updated_at) VALUES ', n.id, n.author_id, n.object_id, quote(p.post_title), quote(n.note), quote(n.created_at), quote(n.updated_at) from wp_fb_user_notes n inner join wp_posts p on p.ID = n.object_id;",

]; //0-16

// LPT -- manual? table copy

global $wpdb; //USE alternate config!


//test downloadable link
// $fileName = 'AWESOME-20181220_002940.sql';
// echo "DONE, check file <a href='$fileName'>$fileName</a>";

$help = isset($_GET['h']);

if ($help) {

	echo "Available Statements: " . count($ss);
	echo "<br/>";

	foreach ($ss as $k => $s) {
		echo "<strong>[$k]</strong>: $s";
		echo "<br/>";
	}

	die();
}

$statementsToRun = isset($_GET['ss']) ? explode(',', $_GET['ss']) : array_keys($ss);

$makeFile = !isset($_GET['file']) || $_GET['file'] == 0 ? false : true;
$doOutput = !isset($_GET['output']) || $_GET['output'] == 0 ? false : true;
$fileName = 'AWESOME-__DATETIME__.sql';
// $fileName = 'AWESOME-001.sql';

$dt = date('Ymd_His');

$fileName = str_replace('__DATETIME__', $dt, $fileName);

if ($makeFile)
	$file = fopen($fileName, 'w');


if ($makeFile && !$file) die("File cannot be created $fileName");

//END GOAL a final file of INSERT or UPDATE statements -- that can be executed agains API DB
foreach ($ss as $k => $s) {

	//skips or runs by ss query string
	if (!in_array($k, $statementsToRun)) {
		continue;
	}

	$res = $wpdb->get_results($s, ARRAY_A);

	//loop thru adjust and make file
	foreach ($res as $r) {

		// any has INSERT
		$first = array_shift($r);

		if (strpos($first, 'INSERT IGNORE INTO') !== false) {

			$preLine = implode(',', $r);

			$preLine = $first . ' (' . $preLine . ');';
		} else {
			array_unshift($r, $first);
			$preLine = implode('', array_values($r));
		}

		if ($doOutput) {
			
			echo $preLine;
			echo "<br/>";
		}

		if ($makeFile)
			fwrite($file, $preLine . PHP_EOL);
	}
}

if ($makeFile) {

	fclose($file);

	//provide downloadable link
	echo "DONE, check file <a href='$fileName'>$fileName</a>";
}
