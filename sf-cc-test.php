<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

include 'jqueryMaterial.php';

// $userId = $_GET['uid'];

// if(isset($_GET['mode']) && $_GET['mode'] != 'ajax'){
// $args = [
//   'student_id' => $userId,
//   //'coaches' => [30023],
//   //'coaches' => [36859],
//   //'coaches' => [36859, 30023],
//   //'types' => ['ss_calls'], //, 'ic_calls'
//   'start_date' => '2017-06-19',
//   'end_date' => '2017-06-20',
//   'start_time' => '00:00:00',
//   'end_time' => '23:45:00'
// ];

// $data = fb_srv_getCoachingCalls($args);
// echo "<pre>". print_r($data, true) . "</pre>";
// return ;

$cm = fb_srv_get_coaching_manager();

$ccid = $_GET['ccid'];

$cc = $cm->findCoachingCallById($ccid);

//echo is_null($cc);
echo 'Got CC for ID: ' . $ccid;
echo ":";
echo assert(!is_null($cc));
echo "<br/>";
echo 'Booked At: '.$cc->getBookedTime()->format('Y-m-d H:i:s');
echo "<br/>";
echo 'Testing Student... ';
echo "<br/>";
$student = $cc->getStudent();

echo ' found student: ';
echo assert(!is_null($student));
echo "<br/>";
echo $student->getDisplayName();
echo "<br/>";
echo $student->getProgramType();
echo "<br/>";
echo $student->getProgramLevel();
echo "<br/>";
//echo "Call SEQ: " . $student->getCoachingCallSeq();
echo "<br/>";
echo "PastCalls: " . $student->getPastCoachingCallsCount();
echo "<br/>";
echo ' -- Student Calls: --';
echo "<br/>";
foreach ($student->getCoachingCalls() as $cc) {
	echo ' ++>';
	echo $cc->getId() . ' ' . $cc->getType();
	echo "<br/>";
}

//------------

$booker = $cc->getBooker();

echo 'Testing Booker...';
echo ' found: ' . assert(!is_null($booker));
echo "<br/>";
if(assert(!is_null($booker))){
	echo $booker->getDisplayName();	
	echo "<br/>";
	echo $booker->getEmail();	
}
