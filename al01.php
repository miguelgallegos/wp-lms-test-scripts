<!-- 
action:get_fb_activity_log
limit:50
offset:0
user_id:47611
filters:website,nss_step,resource
 -->

<?php 

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once '../wp-load.php';


// $eds = FB_EXP_DATES::getInstance();
// echo $eds->tempExtraRolesForMenus(408718);

$sExpirablePermissions = "~cust_commercial101expirationdateliveevent:5/5/2021~cust_commercial101expirationreplay:5/5/2021~cust_commercialexpirationdatelc:5/5/2020~cust_communityexpirationdatelc:5/5/2020~cust_dealreviewconsultationexpirationcoachcall:5/5/2020~cust_dealreviewexpirationdatelc:5/5/2020~cust_facebookgroupexpirationdatelc:5/5/2020~cust_fieldexpertexpirationcoachcalls:5/5/2020~cust_fullimmersionexpirationdateliveevent:5/5/2020~cust_futurefbexpirationdateliveevent:5/5/2021~cust_futurefortunebuildersexpirationdatelc:5/5/2020~cust_innercircleexpirationcoachcalls:5/5/2019~cust_innercircleexpirationgroupcoach:5/5/2020~cust_leadershipexpirationdatelc:5/5/2020~cust_lmsaccessexpiration:5/5/2020~cust_marketingandwholesalingexpirationliveeven:5/5/2020~cust_marketingimmersionexpirationdateliveevent:5/5/2020~cust_marketingimmersionexpirationreplay:5/5/2021~cust_mastery1expirationdatelc:5/5/2020~cust_masteryexpirationgroupcoach:5/5/2020~cust_mindsetacademyexpirationdateliveevent:5/5/2021~cust_mindsetacademyexpirationreplay:5/5/2021~cust_moneyacademyexpirationdateliveevent:5/5/2021~cust_moneyacademyexpirationreplay:5/5/2021~cust_officesystemacademyexpirationreplay:5/5/2021~cust_osaexpirationdateliveevent:5/5/2021~cust_passiveincome1stexpirationcoachcalls:5/5/2020~cust_passiveincome2ndexpirationcoachcalls:5/5/2020~cust_passiveincomefollowupexpirationcoachcalls:5/5/2020~cust_passiveincomehandoffexpirationcoachcalls:5/5/2020~cust_passiveincomeintroexpirationcoachcalls:5/5/2020~cust_passiveinvestorexpirationdatelc:5/5/2020~cust_rehabbingbootcampexpirationdateliveevent:5/5/2021~cust_rehabbingbootcampexpirationreplay:5/5/2021~cust_rentalpropertyexpirationdateliveevent:5/5/2021~cust_rentalpropertyexpirationreplay:5/5/2021~cust_reossexpirationdateliveevent:5/5/2021~cust_reossexpirationdatereplay:5/5/2021~cust_taxassetacademyexpirationdateliveevent:5/5/2021~cust_taxassetacademyexpirationreplay:5/5/2021~cust_taxlienexpirationdatelc:5/5/2020~cust_techchallengedexpirationcoachcalls:5/5/2020~cust_wholesalingbootcampexpirationdateliveeven:5/5/2021~cust_wholesalingbootcampexpirationreplay:5/5/2021";


$a1 = array_filter(explode("~", $sExpirablePermissions));

$expirablePermissions = [];
foreach ($a1 as $pair) {
    $a3 = explode(':', $pair);    
    
    if (count($a3) == 1) continue;

    $dt = new \DateTime($a3[1]);

    $ep = [
        'code' => $a3[0],
        'end_date' => $dt->format('Y-m-d')
        //no start_date        
    ];
    $expirablePermissions[] = $ep;
}

if (count($expirablePermissions) == 0) {
    echo json_encode('Missing Expiration Dates.');
    die();
}

//adds pre-req as will be not sent from dyn
//TODO: handle in S4 better
$dat7 = date('Y-m-d', strtotime('+7 days', time()));
$expirablePermissions[] = [
    'code' => 'cust_LaunchgroupcoachingExpirationPreReq',
    'end_date' => $dat7
];

echo "<pre>". print_r(json_encode($expirablePermissions), true) . "</pre>";

die();
require_once('jqueryMaterial.php');

require_once('paramValidator.php');

require_once('arrayToTable.php');

$al = new FB_Activity();

$args = [
	'user_id' => $_GET['user_id'],
	'limit' => $_GET['limit'],
	'offset' => $_GET['offset'],
	'filters' => $_GET['filters'] //website,nss_step,resource
];

$acts = $al->getActivities($args);

arrayToTable($acts);
//echo "<pre>". print_r($acts, true) . "</pre>";

//select target_id, count(*) c from service_activity_log where target_id > 1000 group by target_id order by c desc;