<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';
require 'jqueryMaterial.php';

$userId = $_GET['uid'];

// $args = [
// 	'student_id' => $userId,
// 	//'coaches' => [30023],
//   //'coaches' => [36859],
//   //'coaches' => [36859, 30023],
//   //'types' => ['ss_calls'], //, 'ic_calls'
//   'start_date' => '2017-06-10',
//   'end_date' => '2017-06-15',
// 	'start_time' => '00:00:00',
// 	'end_time' => '23:45:00'
// ];

// $data = fb_srv_get_coaching_manager()->getCoaches($args);
// echo "<pre>". print_r($data, true) . "</pre>";
// return ;

?>

<script>
   jQuery(document).ready(function() {
       $.ajax({
           url: '/wp-admin/admin-ajax.php',
           data: {action: 'get_all_coaches', 
           student_id: <?php echo $userId ?>,
	       	'start_date' : '2017-07-22',
			'end_date' : '2017-08-15',
			'start_time' : '00:00:00',
			'end_time' : '23:45:00' },
           type: 'post',
           success: function (output) {
           $('.res').append( output );
           }
       });
   });
</script>
<div class="res"></div>

