<?php

//set POST variables
$url = 'http://members.fortunebuilders.com/srv/api/learningobjects';
$ts = date('Y-m-d H:i:s');

$dateObj = [
    'date' => [
        'year' => 2018,
        'month' => 01,
        'day' => 05
    ],
    'time' => [
        'hour' => 10,
        'minute' => 42
    ]
];

$att = [
        'id' => 1,
        'title' => 'MG Attachment ' . $ts,
        'description' => 'Description ' . $ts,
        'is_active' => 1,
        'is_default' => 1,
        'url' => 'myURL',
        'created_at' => $dateObj,
        'created_by_id' => 1,
    ];

//as stdObject
$att = json_decode(json_encode($att));

$fields = array(
    'title' => 'MG Title PUT ' . $ts,
    'content' => 'Content ' . $ts,
    'is_active' => 1,
    'created_at' => $dateObj,
    'created_by_id' => 1,
    'type' => 1

    //'attachments' => [$att],
    //'attachment' => 1
    //'attachment' => $att
    //'attachment'
);

$fields_string = json_encode($fields);

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_HEADER, false);

curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($fields_string))
);  

//execute post
$result = curl_exec($ch);

$result = json_decode($result);

echo "<pre>". print_r($result, true) . "</pre>";

//close connection
curl_close($ch);