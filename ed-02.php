<?php 

include_once('../wp-load.php');

//curriculum analyzer -- checks *ExpirationDateLC stuff 

$postId = $_GET['pid'];
$user_id = $_GET['uid'];

$post = get_post( $postId );
$expDates = FB_EXP_DATES::getInstance();

if ($post->post_type == 'fblms_program' || $post->post_type == 'page') {

    $terms = get_the_terms( $postId, 'fb-permissions' );

    foreach ( $terms as $t ) {
        echo "Searching term: " . $t->slug;
        echo "<br/>";
        $ed = $expDates->getAPIExpirationDateByWPCategory( $user_id, $t->slug, $permissionCategory = 'learning-content' );

        if ( count( $ed ) > 0 ) {
            echo " ED: " . $ed['code'];
            echo "<br/>";
            if ( $ed['is_expired'] ) {
                echo " - - @" . $ed['end_date'];
                echo "<br/>";
            //    $this->redirectPageTo( self::UP_SELL_PAGE );
                echo "REDIRECTING TO UP_SELL_PAGE"; die();
            }
        }
    }

    //return 'ALLOW_VIEW';
    echo "ALLOW_VIEW";
    die();
} else {

    echo "Not a valid page: " . $post->post_type;
}