<?php 


include '../wp-load.php';

$legacyName = 'live_events_exp';

$userId = $_GET['uid']; //<<<

$userId = get_current_user_id();
$eds    = FB_EXP_DATES::getInstance()->getAPIExpirationDateByCategoryId( $userId, $legacyName );

$edsByWPPermission = [];

foreach ( $eds as $ed ) {
    if ( isset( $ed['wppermissions'] ) ) {
        foreach ( $ed['wppermissions'] as $catSlug ) {
            $edsByWPPermission[$catSlug] = $ed;
        }
    }
}

$eventcat_query = get_latest_events( '-1' );

$count = 0;

while ( $eventcat_query->have_posts() ) : $eventcat_query->the_post();
    $lookupWPCategories = ['eventcategory', 'fb-permissions'];

    $goAhead               = false;
    $myExpirablePermission = null;
    foreach ( $lookupWPCategories as $tax ) {
        $terms = get_the_terms( $post->ID, $tax );
        foreach ( $terms as $term ) {
            if ( array_key_exists( $term->slug, $edsByWPPermission ) ) {
                $myExpirablePermission = $edsByWPPermission[$term->slug];
                $goAhead               = true;
            }
        }
    }

    $isAdmin = current_user_can( 'use_admin_access' );
    $isUserRegistered = FB_LiveEvents::getInstance()->isUserRegistered( $userId, $post->ID );
    if ( ! $isAdmin && ! $goAhead && ! $isUserRegistered ) {
        continue;
    }
    //2018 --- /\

    // get_template_part( 'panel', $post->post_type );
    echo "-> " . $post->ID;
    echo "<br/>";
    $count++;
    //}

endwhile;