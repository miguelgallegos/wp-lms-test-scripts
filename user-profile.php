<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$ps = fb_srv('fb_user_profile.service');

//$uid = 47611;
//$uid = 13500;
$uid = $_GET['uid'];

// //$p = $ps->buildVisionExercise($uid);
//$p = $ps->retrieveVisionExercise($uid); //JUST BUILD
//echo "<pre>". print_r($p, true) . "</pre>";
// if($p){
// 	echo 'Profile: ' . $p->getName();
// 	echo "<br/>";
// 	foreach ($p->getSections() as $section) {
// 		echo ' -Section: ' . $section->getTitle();
// 		echo "<br/>";
// 		foreach ($section->getItems() as $item) {
// 			echo ' - - > Q: ' . $item->getTitle();
// 			echo "# R:";
// 			echo $item->getDescription();
// 			echo "<br/>";
// 		}
// 	}
// }
//echo get_class($p);

$upProvider = FB_UserProfile::instance();

$jsonData = $upProvider->retrieveVisionExercise($uid);
echo "<div>";
echo "<pre>". print_r(json_decode($jsonData), true) . "</pre>";
echo '</div>';

die();
//PHP mode
//$qid = 8;
//$upProvider->updateVisionExercise($qid, 'Answer'. $qid .', Sample Text, Lorem Ipsum Dolo Sit Amet Added: ' . date('Y-m-d H:i:s'), ['updated_by_id' => $uid]);

include('jqueryMaterial.php');
?>

<textarea id="aText" name="aText" cols="100" rows="4" data-qid="<?php echo $_GET['qid']?>">This is new text ajax updated! <?php echo date('Y-m-d H:i:s');?></textarea>
<br/>
<button id="push">PUSH!</button>
<script type="text/javascript">
var ajaxurl = '/wp-admin/admin-ajax.php';
jQuery(document).ready(function(){
	jQuery('#push').click(function(e){
		var action = 'update_vision_exercise_item';
		var qid = jQuery('#aText').data('qid');
		var text = jQuery("textarea[name='aText']").val();
		console.log(text);
		jQuery.post(ajaxurl, {action: action, item_id: qid, text: text}, function(data){
			if(data == 1){
				jQuery('.alert-success').show();
			}else {
				jQuery('.alert-danger').show();
			}
		});

	});
});
</script>
<div class="alert alert-success" role="alert" style="display: none; width: 200px;">Updated :)</div>
<div class="alert alert-danger" role="alert" style="display: none; width: 200px;">An Error Ocurred :(</div>