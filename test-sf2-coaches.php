<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$roles = [
	// 'fb_coach', 
	// 'fb_mp_coach', 
	// 'fb_liftoff_coach', 
	'fb_team_assist_coach',
];

$user_query = new WP_User_Query( array( 'role' => $roles ) );
// $user_query = new WP_User_Query( array( 'ID' => [ 83714 ] ) );

echo implode(', ', $roles);
echo "<br/>";
echo 'FOUND: ' . $user_query->get_total();
echo "<br/>";

$um = fb_srv('ekino.wordpress.manager.user');

$errCount = 0;

foreach ($user_query->get_results() as $wuser) {

	$u = $um->findOneById( $wuser->ID );

	$caps = $u->getWPCapabilities();

	echo $u->getId() . ' ---> | ' . $u->getEmail() . ' | type: <strong>'. $u->getAdminType() . ' </strong> | level: ' . $u->getLevel();
	if (!$u->getAdminType()) {
		echo ' <span style="background-color: red;">XXX</span>';
		$errCount ++;
	}
	echo "<br/>";
}

echo "<br/>";
echo 'ERRORS: ' . $errCount;
// $uid = $_GET['uid'];

