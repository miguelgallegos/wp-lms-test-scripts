<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';


$coachIds = [17615,
33479, //jherrera
50276,
20680
];

$dformat = 'Y-m-d H:i:s';
$hformat = 'H:i:s';

$yesterday = new \DateTime('now');
$yesterday->modify('-1 day');

$hoursInDay = range(0, 23);
if(!isset($_GET['go'])){
	echo " --- TEST MODE ---";
}

echo "<h2>Generating Calls for Yesterday</h2>";

foreach ($hoursInDay as $hour) {
	$theTime = clone $yesterday;
	echo "CALL<br/>";
	foreach ($coachIds as $coachId) {

		$theTime->setTime($hour, 0, 0);

		$insParams = [
			'coach_id' => $coachId,
			'start_time' => $theTime->format($dformat),
			'end_time' => $theTime->modify('+45 minutes')->format($dformat),
			'status' => 0,
			'type' => 'ss_calls'
		];
		echo "<pre>". print_r($insParams, true) . "</pre>";
		if(isset($_GET['go'])){
			$ins = $wpdb->insert('wp_fb_coaching_calls', $insParams);
		}	
	}	
}