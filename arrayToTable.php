<?php
function arraySimpleTable($a, $params = ''){
	// echo "<table>";
	// echo "<tr>";
	// foreach ($a as $key => $value) {
	// 	echo "<th>";
	// 	echo $key;
	// 	echo "</th>";
	// }
	// echo "</tr>";
	// echo "<tr>";
	// foreach ($a as $key => $value) {
	// 	echo "<td>";
	// 	echo $value;
	// 	echo "</td>";
	// }
	// echo "</tr>";
	// echo "</table>";
	echo "<pre>". print_r($a, true) . "</pre>";
}
function arrayToTable($a, $params = ''){

	//first row grabs the titles
	//then proceed to fill out

	$headers = array_keys($a[0]);

	echo "<table class='table table-striped table-sm table-hover'>";
	echo 	"<thead class=\"thead-inverse\">";
	foreach ($headers as $h) {
		echo "<th>". $h ."</th>";
	}
	echo 	"</thead>";
	echo "<tbody>";
	foreach ($a as $key => $r) {
		echo "<tr>";
		foreach ($r as $v) {
			echo "<td>";
			if(is_array($v)){
				arraySimpleTable($v);
			}else{
				echo $v;
			}
			echo "</td>";
		}

		echo "</tr>";
	}
	echo "</tbody>";
	echo "</table>";
}