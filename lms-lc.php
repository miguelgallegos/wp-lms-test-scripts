<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

if(isset($_GET['d'])){
	$lookupTime = $_GET['d'];
	$start = date($lookupTime . ' 16:00:00');
	$stop = date( "Y-m-d H:i:s", strtotime( $lookupTime . " +1 day 10 hours" ) ); // PHP:  2009-03-03

}
// else{
// 	$lookupTime = 'Y-m-d';
// 	$start = date($lookupTime . ' H:i:s', strtotime('now -7 hours'));
// 	$stop = date( "Y-m-d H:i:s", strtotime( $lookupTime . " next day " ) ); // PHP:  2009-03-03
// }

// $sql = "select CONVERT_TZ(end_time,'UTC','-07:00') ends, end_time from wp_fb_coaching_calls where end_time between '$start' and '$stop'
// and status = 1
// group by end_time 
// order by end_time desc
// limit 1000;";

$sql = "select end_time from wp_fb_coaching_calls where end_time between '$start' and '$stop'
and status = 1
group by end_time 
order by end_time desc
limit 1000;";

echo $sql;
echo "<br/>";
echo "<br/>";
$res = $wpdb->get_results($sql, ARRAY_A);

echo 'ENDS (Pacific), ENDS (UTC)<br/>';
foreach ($res as $time) {
	//echo sprintf("%s | %s<br/>", $time['ends'], $time['end_time']);
	echo sprintf("%s | %s<br/>", date('Y-m-d H:i:s', strtotime($time['end_time']. ' -7 hours')), $time['end_time']);
}
