<?php 

$ch = curl_init();

$URL_WINSAPI = 'http://members.fortunebuilders.com/srv/api/wins';
//$URL_WINSAPI = 'http://fbw-145.fbmastery.com/srv/api/wins';
$id = 1;
$fieldsToChange = [
//    'company_name' => 'mg-api-1'
    'profit' => 2
];

curl_setopt($ch, CURLOPT_POST, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($ch, CURLOPT_URL, $URL_WINSAPI."/".$id);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH"); // updates existing piece of record
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fieldsToChange));

$r = curl_exec($ch);

// Check for errors and display the error message
if($errno = curl_errno($ch)) {
    $error_message = curl_strerror($errno);
    echo "cURL error ({$errno}):\n {$error_message}";
} else {
    echo "<pre>ORIGI as json ". print_r($r, true) . "</pre>";

    echo "<pre>". print_r(json_decode($r, true), true) . "</pre>";

    // $o = json_decode($r);
    // $o = json_encode($o, JSON_FORCE_OBJECT);
    // $o = json_decode($o, false, 512, JSON_OBJECT_AS_ARRAY );

    // echo "<pre>". print_r($o, true) . "</pre>";
}

curl_close($ch);