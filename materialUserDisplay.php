<section class="section team-section">

    <!--Section heading-->
    <h1 class="section-heading">User Info</h1>
    <!--Section description-->
    <p class="section-description">INTRO</p>

    <!--First row-->
    <div class="row text-xs-center">

        <!--First column-->
        <div class="col-lg-3 col-md-6 mb-r">

            <div class="avatar">
            <?php echo bp_core_fetch_avatar( array( 'item_id' => $user->ID) );?>
                <!-- <img src="" class="rounded-circle"> -->
            </div>
            <h4><?php echo $user->display_name?></h4>
            <h5><?php echo $level?> <?php echo fb_user_is_primary($user->ID) ? '<span class="tag tag-success">PRIMARY</span>' : '<span class="tag tag-info">PARTNER</span>'?></h5>
            <p>ID: <?php echo $user->ID?></p>
            <p>Login: <?php echo $user->user_login?></p>

            <!--Facebook-->
            <a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
            <!--Twitter-->
            <a class="icons-sm tw-ic"><i class="fa fa-twitter"> </i></a>
            <!--Dribbble-->
            <a class="icons-sm drib-ic"><i class="fa fa-dribbble"> </i></a>

        </div>
        <!--/First column-->
    </div>
</section>        