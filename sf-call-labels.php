<?php 


include '../wp-load.php';


$labelsExamples = [
    'MPU1',
    'MPKO10',
    'RE10',
    'TS99',
    'PC210',
    'PC199', 
    'M1',
    'JS34', 
    'IK12',
];

$labels = [
    'MPU',
    'MPKO',
    'RE',
    'TS',
    'PC2',
    'PC1',
	'M',
	'JS',   
];

foreach ($labelsExamples as $callLabel) {
	$mylabel = 'M';
	echo 'search: '.$callLabel . ' ' ;
	foreach ($labels as $label) {

		$numeric = str_replace($label, '', $callLabel);
		$literal = str_replace($numeric, "", $callLabel);

		if (in_array($literal , $labels)) {
			$mylabel = $literal;
			break;
		} 
	}
echo $mylabel . '-' . $numeric;
echo "<br/>";
}

