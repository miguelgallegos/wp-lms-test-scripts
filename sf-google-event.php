<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);


// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$g = fb_srv('fb_coaching.google_calendar_service');

// $student_id = 47611;
// $slot_id = 325274;
// $phone_number = '123-123-1234';
// $a = fb_srv_bookACall($student_id, $slot_id, ['phone_number' => $phone_number]);

// echo "<pre>". print_r($a, true) . "</pre>";

//----------------

$summary = 'GCal API test by MG';

$description = 'GCal API test description - created with live credentials';

$startTime = new \DateTime();

$endTime = clone $startTime;
$endTime->add(new \DateInterval($forNext = 'PT30M'));

$start = ['dateTime' => $startTime->format('c'), 'timeZone' => 'America/Los_Angeles'];
$end = ['dateTime' => $endTime->format('c'), 'timeZone' => 'America/Los_Angeles'];

//this gets setup when dev mode and fbtech credentials are used
//$attendeeEmail = 'fortunebuilderstech@gmail.com';

//$attendeeEmail = 'mgallegos@fortunebuilders.com';
//$attendeeEmail = 'miguel.gallegos@gmail.com';
$attendeeEmail = 'jherrera@fortunebuilders.com';

$attendees = [
    //['email' => $attendeeEmail, 'responseStatus' => 'accepted'],
    //['email' => 'mgallegos@fortunebuilders.com', 'responseStatus' => 'accepted'],
    ['email' => $attendeeEmail, 'responseStatus' => 'accepted'],
    //['email' => 'lms-google-test@lms-interaction-test.iam.gserviceaccount.com', 'responseStatus' => 'accepted']
];

// if (true) {
//     $email = 'fortunebuilderstech@gmail.com';

//     $attendees = [[
//         'email' => $email, 'responseStatus' => 'accepted'
//     ]];

// }


$extendedProperties = [
    'shared' => ['fbcid' => 'CALL_ID_HERE']
];

$colorId = 3;

$event = $g->push($summary, $description, $start, $end, $attendees, $extendedProperties, $colorId);

echo "<pre>". print_r($event, true) . "</pre>";