<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once '../wp-load.php';

if ( !isset($_GET['uid']) ) die('Where is uid param?');

$uid = $_GET['uid'];

$max = 20;

if ( isset($_GET['max']) ) {
	$max = $_GET['max'];
}

function htmlSTRONG($string) {
	return "<strong>$string</strong>";
}
function htmlH4($string) {
	return "<h4>$string</h4>";
}

function htmlDIV($string) {
	return "<div>$string</div>";
}


echo htmlH4("User: $uid, Max: $max");

//update to Getting Started Orientation
$webinars = FBTrainingHandler::getInstance()->get_virtual_trainings($max, false);

// echo "<pre>" . print_r($webinars, true ) . "</pre>";
foreach ($webinars as $key => $value) {
	echo htmlDIV($key . ' -- ' . $value['title']);
}