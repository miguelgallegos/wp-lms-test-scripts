<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once '../wp-load.php';


$to = 'miguel.gallegos@gmail.com';
$message = 'This is a message of a tagged student';

$add_headers = [];                
$add_headers[] = "Bcc: chris@fortunebuilders.com";
$add_headers[] = "Bcc: tko@fortunebuilders.com";//TODO: under investigation since this recipient doesn't get these messages

//TODO: tmp added to this list until tko email list gets fixed - defined in wp-config.php
if(defined('TAGGED_STUDENT_BCC_NOTIFY_TO')){
	$additionalBccs = explode(',', TAGGED_STUDENT_BCC_NOTIFY_TO);
	foreach ($additionalBccs as $bccEmail) {
		if(strlen($bccEmail) > 0)
			$add_headers[] = 'Bcc: ' . $bccEmail;
	}
}

echo "<pre>Sending to... ". print_r($add_headers, true) . "</pre>";