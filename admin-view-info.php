<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$student_id = $_GET['sid'];

echo 'FE calls used: ' . fb_get_user_used_calls($student_id);
echo "<br/>";
echo "<br/>";
$type = 'Mastery'; //is this related to 'calls'?
$call_count = $GLOBALS['call_count'];

//echo "<pre>". print_r($call_count, true) . "</pre>";

$currentCount = $call_count->get($type, $student_id);
echo 'Current CallCount: ' . $currentCount;
echo "<br/>";
$newCount = $call_count->increment($type, $student_id);

echo 'Incremented to: '  .$newCount;