<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';


$uid = $_GET['user_id'];

header('Content-Type: application/json; charset=utf-8');
header("Access-Control-Allow-Origin: *");

$level = fb_get_user_level($uid);
echo json_encode(['level' => $level, 'user_id' => $uid]);