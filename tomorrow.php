<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$todaySelection = new \DateTime('2016-12-20 01:00:00');

echo $todaySelection->format('Y-m-d H:i:s');
echo "<br/>";
$now = new \DateTime('now');
echo $now->format('Y-m-d H:i:s');

echo "<br/>";
if($todaySelection < $now){
	echo "###";
	//$todaySelection->add(new \DateInterval(''));
	$todaySelection->modify('+1 day');
}

echo "<br/>";

echo "MODIF! ";
echo $todaySelection->format('Y-m-d H:i:s');