<?php

//DELETE (soft delete)

$url = 'http://members.fortunebuilders.com/srv/api/learningobjects/2';
$ts = date('Y-m-d H:i:s');

$dateObj = [
    'date' => [
        'year' => 2018,
        'month' => 01,
        'day' => 05
    ],
    'time' => [
        'hour' => 10,
        'minute' => 42
    ]
];

$fields = array(
    'is_active' => 0,
    'deleted_at' => $dateObj,
    'deleted_by_id' => 47611
);

$fields_string = json_encode($fields);

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($fields_string))
);  

//execute post
$result = curl_exec($ch);

$result = json_decode($result);

echo "<pre>". print_r($result, true) . "</pre>";

//close connection
curl_close($ch);