<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

//quick check

// $params = [
// 'label' => 'IC',
// 'call'
// ];
// echo sprintf("%s%d", $params['label'], $params['call_seq']);


$sched = fb_srv('fb_coaching.scheduler');

$coachId = $_GET['cid'];

//These dates are UTC
//Front end or WP needs to convert to this TZ
// $startDate = '2017-07-07 20:00:00';
// //$startDate = 'NADA';
// $endDate = '2017-07-07 20:45:00';

// $type = 'ss_calls';

$studentId = $_GET['sid'];

// $params = [
// 	'call_label' => 'M1_01',
// 	'phone_number' => '(619) 123-test'
// ];

/*
// when opening slot, select coach's timezone!

*/


// $call = $sched->openSlot($cid, $callType, $startDate, $endDate);

// if($sched->hasErrors()){
// 	echo "Wow: errors!";
// 	echo "<br/>";
// 	$errors = $sched->getErrors();
// 	foreach ($errors as $err) {
// 		echo $err->getMessage();
// 		echo "<br/>";
// 	}
// } else {
// 	echo $call->getId();
// 	$params = ['call_label' => 'M1_', 'phone_number' => '(619)123-test'];
// 	$res = $sched->book($params);
// }
$params = [
	//'call_label' => 'M2_OB1',
	'phone_number' => '(619) 123-1234',
//	'openForce' => true  <<< ignored for now
];

//$callLabel = 'IC1-'; //<<< look if user is JS and SS, then make it JS type otherwise M
//$callLabel = 'SS444-444-4444';
$callLabel = 'M1';
$now = new \DateTime();
echo 'Current Time: ' . $now->format('Y-m-d H:i:s'); //this is in UTC!!!
echo "<br/>";

//inputs from ng
$startDate = '2017-08-6';
$startTime = '08:00:00';
$type = 'ss_calls';
$ssType = 'm';
// $um = fb_srv('ekino.wordpress.manager.user');

// $coach = $um->findOneById($coachId);

// echo 'Inputs as is: ';

// $iDate = new \DateTime($startDate . ' ' . $startTime);
// echo $iDate->format('Y-m-d H:i:s');
// echo "<br/>";

// $ctza = $coach->getTimeZone()->getSelected();
// $cTime = new \DateTime();
// echo 'Orig is in timezone: ';
// echo $cTime->format('Y-m-d H:i:s');
// $cTZ = new \DateTimeZone($ctza);
// $cTime->setTimeZone($cTZ);
// echo "<br/>";

// echo 'Coach date time on tz: ';
// echo $cTime->format('Y-m-d H:i:s');
// die(' TZ ');

$res = $sched->doOpenAndBook($coachId, $studentId, $type, $ssType, $callLabel, $startDate, $startTime, $params);

//WRAPPINGT THIS CALL by doOpenAndBook
//$res = $sched->openAndBook($coachId, $type, $studentId, $startDate, $endDate, $params);

if(!$res['success'] && isset($res['errors']))
{	
	echo 'ERRORS';
	echo "<br/>";
	foreach ($res['errors'] as $err) {
		echo $err;
		echo "<br/>";
	}
}
else
	echo "<pre>". print_r($res, true) . "</pre>";


/**
use cases:
    FE - turn into M
        label M + #
        type calls

    SS - IC or JS or M
        label JS or M
        type  ss_calls

    IC - is IC
        label IC
        type ic_calls

    MP - is M
        label M
        type  mp_calls


*/
