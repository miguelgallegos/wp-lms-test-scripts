<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once '../wp-load.php';

$user_id = 47611;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/4.0.2/bootstrap-material-design.css">

<script type="text/javascript">
var myurl = '/wp-admin/admin-ajax.php';

(function($){
	$(document).ready(function(){

		var resultsContainer = $('.results');

		$('.loadData').click(function(e){
			e.preventDefault();
			var params = {
				action:'get_user_isoft_tags',
				user_id: <?php echo $user_id ?>,
			};
			$.post(myurl, params, function(data){
				resultsContainer.append(JSON.stringify(data));
			});

		});

	});
})(jQuery);
</script>

<button class="loadData">LOAD TAGS</button>
<div class="results"></div>
