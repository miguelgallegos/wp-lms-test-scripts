#!/bin/bash
#LOCAL

PASSWORD=guFCbn5zPWqz8PD
#HOST=mastery-$2.cbo19mwhsqto.us-west-2.rds.amazonaws.com
HOST=$2
#mastery-aurora-cluster.cluster-cbo19mwhsqto.us-west-2.rds.amazonaws.com
USER=mastery_db_admin
DATABASE=mastery
# DB_FILE=dump-$2-$3.sql
# $3 can be formatted as INS_DATETIME
DB_FILE=dump-WP-$3.sql
SSH_HOST=$1

# ADD THIS PROCESS TO THE FULL DUMP:
NEW_WP_DB=$3

# run this in API side too...
CREATE_DB_FILE=sql-test-create.sql

# echo $CREATE_DB_FILE
rm $CREATE_DB_FILE
echo "CREATE DATABASE $3;" > $CREATE_DB_FILE
mysql < CREATE_DB_FILE
mysql < $DB_FILE