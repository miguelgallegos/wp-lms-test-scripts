<?php 

include_once('../wp-load.php');


$user_id = $_GET['uid'];

$groups = $_GET['groups'];

$operation = $_GET['operation'];

if ($operation == 'removeTags')
    remove_usergroups($user_id, $groups, $skip_api_call = false);

if ($operation == 'addTags')
    add_usergroups($user_id, $groups, $skip_api_call = false);