<?php

/**
* Case 1:
* 	New call is requested on existing call
*   New call is requested on times that can't be moved
*/

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$um = fb_srv('ekino.wordpress.manager.user');

$uid = $_GET['uid'];

$coach = $um->findOneById($uid);

echo 'COACH: '.$coach->getEmail();
echo "<br/>";

///// EXISTING SLOTS
//OK
// $startTime = '2019-04-06 09:00:00';
// $endTime = '2019-04-06 09:45:00';
//OK
// $startTime = '2019-04-06 09:45:00';
// $endTime = '2019-04-06 10:30:00';
//OK
// $startTime = '2019-04-06 10:30:00';
// $endTime = '2019-04-06 11:15:00';
//OK
$startTime = '2019-04-06 11:15:00';
$endTime = '2019-04-06 12:00:00';

//OVERLAPPING SLOTS
// $startTime = '2019-04-06 09:15:00';
// $endTime = '2019-04-06 10:00:00';

//OVERLAPS LAST - 2
// $startTime = '2019-04-06 10:00:00';
// $endTime = '2019-04-06 10:45:00';

//OVERLAPPING (LAST - 1) == OK
// $startTime = '2019-04-06 10:45:00';
// $endTime = '2019-04-06 11:30:00';

//OVERLAPPING LAST == OK == can select it
// $startTime = '2019-04-06 11:30:00';
// $endTime = '2019-04-06 12:15:00';

$callAvailable = null;

$callIdx = 0;

if ($coach->hasCoachAvailabilityAround($startTime, $endTime)) {
	$availableCalls = $coach->getCoachAvailabilityAround($startTime, $endTime);
	foreach ($availableCalls as $call) { //must only one exists?
		$callIdx ++;
		$callAvailable = $call;        		
		break;
	}

	echo 'CallPos: '.$callIdx . ', CallTotal: ' . count($availableCalls);
	echo " **** DX: " . (count($availableCalls) - $callIdx);
	echo "<br/>";

	if ( (count($availableCalls) - $callIdx) > 0 && null != $callAvailable) {
		echo " CAN'T GO: (" . $callAvailable->getId().')';
		echo "<br/>";
		$callAvailable = null;
	}
}

if ($callAvailable) {
	echo 'FOUND CALL TO REPLACE: [' . $callAvailable->getId() . '] --> ' . $callAvailable->getStartTime()->format('Y-m-d H:i:s') . ' ' . $callAvailable->getEndTime()->format('Y-m-d H:i:s');
} else {
	echo "GO!";
	echo "<br/>";
}


