<?php 

require_once '../wp-load.php';

global $wpdb;

$uid = $_GET['uid'];

$partnerTag = 2623;

function cleanLevel($levelString) {
    $la = unserialize($levelString);    

    foreach ($la as $key => $value) {
        if (strpos('have_member_listing', $key) !== false) continue;

        $l = str_replace('js', 'Jumpstart ', $key);
        $l = str_replace('fb_', '', $l);
        $l = str_replace('_', ' ', $l);
        $l = ucwords($l);
    }

    return $l;
}

$user = get_user_by('ID', $uid);

//skip test accounts
if ( strpos($user->user_login, 'wholesalingprofit.com') != false || $user->ID == "1" ) {
    echo "ERR: is test acc."; 
}

$umeta = get_user_meta($user->ID);

$isId = isset($umeta['Id']) && isset($umeta['Id'][0]) ? $umeta['Id'][0] : 0;
$phone = isset($umeta['Phone1']) && isset($umeta['Phone1'][0]) ? $umeta['Phone1'][0] : 0;
$level = cleanLevel($umeta['wp_capabilities'][0]);

$sTags = isset($umeta['Groups']) && isset($umeta['Groups'][0]) ? $umeta['Groups'][0] : '';
$aTags = explode(',', $sTags);

$customerType = in_array($partnerTag, $aTags) ? 'partner' : 'primary';

if( $level == 'Subscriber') {
    echo "ERR: is subscriber";
}

if (count($aTags) == 0) {
    echo "ERR: no tags";
}

//excludes partners
if ($customerType != $processUserType) { 
    echo "ERR: no $processUserType";    
}

$inserted = push_user_to_s4($user);

$r1 = [];
if ($inserted)
    $r1 = load_s4_data($user->ID);

if (isset($r1['expirablePermissions']) && count($r1['expirablePermissions']) > 0) {
    update_user_meta($user->ID, '_dyn', true);

    //PREP
    $dynData = [
        'emailaddress1' => $user->user_email, 
        'cust_firstname' => $user->first_name, 
        'cust_lastname' => $user->last_name, 
        'telephone1' => $phone, 
        'cust_currentmasterylevel' => $level, 
        'cust_iscontactid' => $isId,
        'customertypecode' => $customerType,
        'expirablePermissions' => $r1['expirablePermissions']
    ];
 
    echo "<pre>". print_r($dynData, true) . "</pre>";
}