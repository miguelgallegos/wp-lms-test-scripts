<?php 
//this is on request only
//needs a persistent cache plugin -- let's user transient API then
include_once('../wp-load.php');

$userId = 47611;
$cacheKey = 'api_student_data_' . $userId;
$myData = wp_cache_get( $cacheKey );
if ( false === $myData ) {
    //$result = $wpdb->get_results( $query );
    $myData = 'COOL STORED!';
    echo "storing...";
    echo "<br/>";
    wp_cache_set( $cacheKey, $myData );
} 

echo "Data: " . $myData;
