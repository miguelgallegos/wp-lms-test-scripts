<?php 
/*
* Script to get all records from wp_lpt table
* Check for duplicates
* Create an array of all the correct records
* Create a new table (with composite index if not exist)
* Insert correct records into database
*/
class WP_LPT
{
	private $options;
	private $lpt_table;

	function __construct($options = array()) {

		self::console('WP_LPT init ...');

		// Increase PHP limit
		ini_set('memory_limit', '10G');

		// show errors
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);


		self::console(ini_get('memory_limit'));

		$this->options = $options;
		$this->lpt_table = "wp_lpt";

		$this->connectDB();
	}

	public static function console($msg) {
		echo $msg . "\r\n";
	}

	public function error($msg) {
		echo "Error: " . $msg . "\r\n";
		exit;
	}

	private function connectDB() {

		$to_check = ['host', 'user', 'pass', 'database'];

		foreach ( $to_check as $key ) {

			// if ( empty($this->options[$key]) )
			// 	$this->error("Missing required option - " . $key);
		}

		extract($this->options);

		$conn = new mysqli( $host, $user, $pass, $database );

		if ( $conn->connect_errno ) 
			$this->error("Failed to connect to MySQL: " . $conn->connect_error);
		else
			self::console('Successfully connected to ' . $database . ' on ' . $host);

		/* ======================================================================= */

		$result = $conn->query("SELECT count(*) from " . $this->lpt_table);
		$start_count = $result->fetch_row()[0];
		self::console('Start Count: ' . $start_count);


		$query = "SELECT * from " . $this->lpt_table;

		$unique_rows = [];
		$duplicates = [];

		// if ($results = $conn->query($query, MYSQLI_USE_RESULT)) {
		if ($results = $conn->query($query)) {

			// fetch single row
			while ($row = $results->fetch_row()) {

				// Set composite index as the unique hash key
				// ex. $result[{user_id}_{action}_{object_id}]
				$composite_index = $row[1] . '_' . $row[2] . '_' . $row[3];

				// if unique array already has composite index as key
				if (isset($unique_rows[$composite_index])) {

					// if current row has higher learned value
					if ($unique_rows[$composite_index][4] < $row[4]) {

						// put old unique row into duplicate array (using id as key)
						$duplicates[$unique_rows[$composite_index][0]] = $unique_rows[$composite_index];

						// hash current row into uniques
						$unique_rows[$composite_index][4] = $row[4];

						// print composite index to console
						self::console($composite_index);
					}
					// if same learned value, keep older created_at time
					elseif ($unique_rows[$composite_index][4] == $row[4]) {

						// if current row time is less than hashed time (keeping oldest time)
						if (strtotime($unique_rows[$composite_index][5]) > strtotime($row[5])) {

							// put old unique row into duplicate array (using id as key)
							$duplicates[$unique_rows[$composite_index][0]] = $unique_rows[$composite_index];

							// hash current row into uniques
							$unique_rows[$composite_index][5] = $row[5];

							// print composite index to console
							self::console($composite_index);
						}
						// otherwise, current row time is newer, so add to duplicates
						else 
							$duplicates[$row[0]] = $row; 
					}
					// otherwise, current row has lower learned value, so add to duplicates
					else 
						$duplicates[$row[0]] = $row;

				}
				// otherwise, current composite index doesn't exist, so add to unique array
				else $unique_rows[$composite_index] = $row;
			}

			$results->close();
		}

		// test
		var_dump(array_keys($duplicates));
		$duplicates_found = count($duplicates);
		self::console($duplicates_found . ' duplicates found');
		// exit;

		self::console('Unsetting unique rows...');
		unset($unique_rows);

		self::console('Begin DELETE query...');

		$query = "DELETE from " . $this->lpt_table . " where (id) in (";

		foreach ($duplicates as $key => $row) {
			$query .= $row[0] . ",";
		}

		$query = rtrim($query, ',') . ")";

		self::console($query);

		unset($duplicates);

		if ($result = $conn->query($query)) {
			self::console('DELETE QUERY SUCCESSFUL');
		} else {
			$this->error($conn->error);
		}

		/*
		* End Numbers
		*/
		$result = $conn->query("SELECT count(*) from " . $this->lpt_table);
		$end_count = $result->fetch_row()[0];

		$removed_count = $start_count - $end_count;
		
		self::console('Start Count: ' . $start_count);
		self::console('End Count: ' . $end_count);
		self::console('Duplicates Found: ' . $duplicates_found);
		self::console('Duplicates Removed: ' . $removed_count);

		$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
		self::console("Memory Usage: " . memory_get_peak_usage());
		self::console("Process Time: {$time}");
	}

	
}

/*
* Set database options here
*/
$options = array(
	'host' => 'localhost', 
	'user' => 'root', 
	'pass' => '', 
	'database' => 'mastery'
);

new WP_LPT( $options );
?>