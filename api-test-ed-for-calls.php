<?php 

include '../wp-load.php';

$userids = [
	47611,
	21014,
	37763,
	37762,
	73326
];

$categorySlug = 'coaching-calls';

foreach ($userids as $userId) {
	$ed = FB_EXP_DATES::getInstance()->getAPINextAvailableExpirationDateByCategory($userId, $categorySlug);

	// echo "<pre>" . print_r($ed, true) . "</pre>";
	echo sprintf("%s | %s - %s - exp:%s <br/>", $userId, $ed['name'], $ed['end_date'], $ed['is_expired']);
}
