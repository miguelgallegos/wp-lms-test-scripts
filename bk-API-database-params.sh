#!/bin/bash
#LOCAL

PASSWORD=0ceanBeach!
HOST=fb-api-$2-cluster.cluster-cbo19mwhsqto.us-west-2.rds.amazonaws.com
USER=fb_api_db_admin
DATABASE=fb-api-db
DB_FILE=dump-api-$2-$3.sql
SSH_HOST=$1
#DB host: fb-api-dev-rds-cluster.cluster-cgnszbkc23ot.us-west-1.rds.amazonaws.com
### example:
### ./bk-API-database-params.sh 18.236.36.179 qa-rds 2020-01-21_01
### ./bk-API-database-params.sh ssh.api-qa.fortunebuildersdev.com qa-rds 2020-01-21_01

#stuct only, no data
EXCLUDED_TABLES=(
fb_caching
fb_playtracker
)
#wp_fb_coaching_user_notes not so sure

EXCLUDED_FULL_TABLES=(
fb_expirable_permission_BK
)	

#ignore completely

IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_TABLES[@]}"
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done

FULL_IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_FULL_TABLES[@]}"
do :
   FULL_IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done


echo "Dump structure for API DB"
# echo "ssh ubuntu@${SSH_HOST} -i /Users/miguel/Downloads/mastery-dev-key.pem mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --no-data ${FULL_IGNORED_TABLES_STRING} ${DATABASE} > ${DB_FILE}"
/Applications/XAMPP/bin/mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --no-data ${FULL_IGNORED_TABLES_STRING} ${DATABASE} > ${DB_FILE}

echo "Dump content for API DB"
# echo "ssh ubuntu@${SSH_HOST} -i /Users/miguel/Downloads/mastery-dev-key.pem mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} --no-create-info ${IGNORED_TABLES_STRING} ${FULL_IGNORED_TABLES_STRING} >> ${DB_FILE}"
/Applications/XAMPP/bin/mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} --no-create-info ${IGNORED_TABLES_STRING} ${FULL_IGNORED_TABLES_STRING} >> ${DB_FILE}


#DO THE IMPORT HERE!!
