<?php 

include_once('../wp-load.php');

$is = fb_srv('fb_infusionsoft.infusionsoft_service');

$data = [
    // 'Id'        => 12,
    'FirstName' => 'QA-Test',
    'LastName'  => 'QA-Test',
    'Phone1'    => '123-99997',
    'Email'     => 'qa-test-01@gmail.com',
    'OptInReason' => 'WebForm filled out',
    'TagIds'      => [92, 91],  //OK
    'RemoveTagIds'      => [101/*,92, 91*/], //OK
    'SendTemplate' => 145
];

$is->addOrUpdateContact($data);    
