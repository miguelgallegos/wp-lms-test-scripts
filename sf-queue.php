<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once '../wp-load.php';

$is = fb_infusionSoft();

$queue = $is->getProcessQueue();


foreach ($queue as $qi) {
    echo $qi->getId() . ' ' . $qi->getOperation() . ' ' . $qi->getCreatedAt()->format('Y-m-d H:i:s');
    echo "<br/>";
}