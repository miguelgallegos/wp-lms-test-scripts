<?php 

if (!isset($_GET['o'])) {
    header('Location: http://members.fortunebuilders.com/');
}

//options:
//1 = check: queued, errored | default
//2 = clear
//as of 12/4, there is an issue that accumulates error items and queue will keep cycling, the error is due to a missing contact id when removing a tag

include_once('../wp-load.php');
global $wpdb;

$opt = $_GET['o'];

$s1 = "select count(*) count from fb_queue_item where executed = 1 and completed = 0";
$r1 = $wpdb->get_row($s1);

$s2 = "select count(*) count from fb_queue_item where executed = 0 and completed = 0";
$r2 = $wpdb->get_row($s2);

echo "<pre>Error Records:". $r1->count . "</pre>";
echo "<pre>Pending Processing:". $r2->count . "</pre>";

if ($r1->count > 0) {
    echo "<a href=\"lms-is-mon.php?o=2\">Fix</a>";
    echo "<br/>";
}

switch ($opt) {
    case 1:
        break;
    case 2:

        $qdata = [
            'completed' => 0,
            'executed' => 1
        ];

        $udata = [
            'completed' => 1,
            'executed' => 1
        ];

        $rows = $wpdb->update('fb_queue_item', $udata, $qdata);

        echo "Execute fix: " . $rows;
        echo "<br/>";
        break;
    
    default:
        header('Location: http://members.fortunebuilders.com/');
        break;
}