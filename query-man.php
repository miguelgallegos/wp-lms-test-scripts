<?php 

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

include 'jqueryMaterial.php';

global $wpdb;

$s = "SELECT distinct(w0_.id), w0_.id AS id0, w0_.status AS status1, w0_.start_time AS start_time2, w0_.end_time AS end_time3, w0_.survey_entry AS survey_entry4, w0_.survey_rating AS survey_rating5, w0_.type AS type6, w0_.post_id AS post_id7, w0_.call_booked_time AS call_booked_time8, w0_.call_updated AS call_updated9, w0_.created_at AS created_at10, w0_.student_id AS student_id11, w0_.coach_id AS coach_id12 
FROM wp_fb_coaching_calls w0_ 
LEFT JOIN wp_users w1_ ON w0_.coach_id = w1_.ID 
LEFT JOIN wp_users w2_ ON w0_.student_id = w2_.ID 
LEFT JOIN wp_fb_user_timezone w3_ ON w2_.ID = w3_.user_id 
LEFT JOIN wp_fb_user_timezone w4_ ON w1_.ID = w4_.user_id 
WHERE w0_.coach_id IN (123, 446, 1326, 438, 4238, 4748, 10856, 12020, 12021, 17615, 18903, 20332, 22766, 25428, 26184, 27637, 28641, 28953, 30021, 30022, 30023, 33479, 36346, 36859, 37194, 37764, 40987, 44002) 
AND w0_.status IN (0, 1) 
AND w0_.type IN ('ss_calls', 'ic_calls') 
AND w0_.start_time >= '2016-12-30 00:00:00' 
AND w0_.start_time <= '2017-02-25 23:59:59' 
ORDER BY w0_.start_time ASC";


$r = $wpdb->get_results($s);

$t1 = microtime(true);
echo count($r);

$t2 = microtime(true);

echo " " . ($t2 - $t1);
