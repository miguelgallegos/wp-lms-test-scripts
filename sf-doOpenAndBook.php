<?php

// http://members1.fortunebuilders.com/wp-lms-test-scripts/sf-doOpenAndBook.php?cid=17615&sid=47611

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$sched = fb_srv('fb_coaching.scheduler');

$coachId = $_GET['cid'];

$studentId = $_GET['sid'];

$params = [
	//'call_label' => 'M2_OB1',
	'phone_number' => '(619) 123-1234 ext 30',
//	'openForce' => true  <<< ignored for now
];

$callLabel = 'M1';
$now = new \DateTime();
echo 'Current Time: ' . $now->format('Y-m-d H:i:s'); //this is in UTC!!!
echo "<br/>";

//inputs from ng
$startDate = '2019-03-06';
$startTime = '04:00:00';
$type = 'ic_calls';
$ssType = 'm';

$sched->setCurrentUserId(17615);
$res = $sched->doOpenAndBook($coachId, $studentId, $type, $ssType, $callLabel, $startDate, $startTime, $params);

if(!$res['success'] && isset($res['errors']))
{	
	echo 'ERRORS';
	echo "<br/>";
	foreach ($res['errors'] as $err) {
		echo $err;
		echo "<br/>";
	}
}
else
	echo "<pre>". print_r($res, true) . "</pre>";
