<?php

define('FB_SYNC_DYN_DEBUG_LEVEL', 3); //make it binary


/* 
definition:
	- 1 post
	- 2 email notifications OFF 
	- 3 exp dates
*/


// echo FB_SYNC_DYN_DEBUG_LEVEL & 5;

 // PHP code to demonstrate Bitwise Operator. 
          
// Bitwise AND 
$First = 5; 
$second = 3; 
$answer = $First & $second; 
 

echo bindec('11011');
echo "<br/>";

print_r("Bitwise & of 5 and 3 is $answer"); 
  
print_r("<br/>"); 
  
// Bitwise OR 
$answer = $First | $second; 
print_r("Bitwise | of 5 and 3 is $answer"); 
  
print_r("<br/>"); 
  
// Bitwise XOR 
$answer = $First ^ $second; 
print_r("Bitwise ^ of 5 and 3 is $answer"); 
  
print_r("<br/>"); 
  
// Bitwise NOT 
$answer = ~$First; 
print_r("Bitwise ~ of 5 is $answer"); 
  
print_r("<br/>"); 
  
// Bitwise Left shift 
$second = 1; 
$answer = $First << $second; 
print_r("5 << 1 will be $answer"); 
  
print_r("<br/>"); 
  
// Bitwise Right shift 
$answer = $First >> $second; 
print_r("5 >> 1 will be $answer"); 
  
print_r("<br/>"); 