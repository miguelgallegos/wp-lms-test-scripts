<?php 

$args['ss_levels'] = ['js'];
//$args['ss_levels'] = ['js', 'm'];
//$args['ss_levels'] = ['js', 'm', 'ic'];
//$args['ss_levels'] = ['ic'];


$coach1 = new stdClass;
$coach1->id = 100;
$coach1->isSSJSCoach = true;
$coach1->isSSMCoach = false;
$coach1->isSSICCoach = false;

$coach2 = new stdClass;
$coach2->id = 101;
$coach2->isSSJSCoach = true;
$coach2->isSSMCoach = true;
$coach2->isSSICCoach = false;

$coach3 = new stdClass;
$coach3->id = 102;
$coach3->isSSJSCoach = true;
$coach3->isSSMCoach = true;
$coach3->isSSICCoach = true;

$coach4 = new stdClass;
$coach4->id = 103;
$coach4->isSSJSCoach = false;
$coach4->isSSMCoach = false;
$coach4->isSSICCoach = false;


$calls = [
	[
		'id' => 1,
		'coach' => $coach1,	
	],
	[
		'id' => 2,
		'coach' => $coach2,	
	],
	[
		'id' => 3,
		'coach' => $coach3,	
	],
	[
		'id' => 4,
		'coach' => $coach4,	
	],		

];

$coachesMatching = [];
foreach ($calls as $call) {

	$coach = $call['coach'];

	if(isset($args['ss_levels'])){

	    $c = 0;

	    foreach ($args['ss_levels'] as $i => $ssLevel) {
	        if($ssLevel == 'js' && $coach->isSSJSCoach || 
	           $ssLevel == 'ic' && $coach->isSSICCoach || 
	           $ssLevel == 'm'  && $coach->isSSMCoach)
	            $c ++;
	    }
	    if($c != count($args['ss_levels'])){
	    	continue;
	    }

	    $coachesMatching[] = $coach;
	}
}

echo "<pre>". print_r($coachesMatching, true) . "</pre>";