<?php

include '../wp-load.php';

$times = '';

$max = 100;

$host = 'http://members.fortunebuilders.com';

//CODE FROM POSTMAN as PHP

$t1 = microtime(true);

$curl = curl_init();
// $time = time();
$time = '1538699240';

$emailPrefix = 'bulk-'. $time .'-__IDX__@gmail.com';
$firstName = 'BulkFirstName-';
$lastName = 'BulkLastName-';

for ($i=0; $i < $max; $i++) { 
	$goData = [
		'email' => str_replace('__IDX__', $i, $emailPrefix),
		'isPrimary' => 'True',
		'firstName' => $firstName .'-UPD2-' . $i,
		'lastName' => $lastName . $i,
		'dynamics_id' => 'bulk-dyn-' . $time .'-' . $i,
		'infusionsoft_id' => 'bulk-is-' . $time . '-' . $i,
		'managedById' => 47611,
		// 'programlevel' => 'diamond',
		'timezone' => 'America/Los_Angeles',
		// 'productAddons' => '~Spaced Add-On w/(Parens):10/10/2018~Internet Quickstart - PRIME (2018):9/21/2018~IQ:9/23/2018~IQ1:9/23/2018~IQ2:9/23/2018',
		// 'expirablePermissions' => '~cust_lmsaccessexpiration:1/25/2018~cust_marketingandwholesalingexpirationliveeven:9/25/2021~cust_marketingandwholesalingexpirationreplay:9/25/2021~cust_marketingimmersionexpirationdateliveevent:9/25/2021~cust_marketingimmersionexpirationreplay:9/25/2020~cust_mastery3expirationdatelc:9/25/2020',
		'expirablePermissions' => '~cust_lmsaccessexpiration:1/25/2018',
		// 'street' => '100' . $i . ' Street Dr.',
		// 'city' => 'San Diego' . $i,
		// 'state' => 'CA' . $i,
		// 'zipcode' => '10001',
		// 'country' => 'USA',
		// 'phone' => '(123) 123-1234',
	];

	$encGoData = http_build_query($goData);

	curl_setopt_array($curl, array(
	  CURLOPT_URL => $host . "/wp-json/fb/v1/users",
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS => $encGoData,
	  CURLOPT_HTTPHEADER => array(
	    "Cache-Control: no-cache",
	    "Content-Type: application/x-www-form-urlencoded",
	    "Postman-Token: aca8501b-9fac-4ea0-838b-e8a2abe077b2",
	    "X-AUTH-TOKEN: +6os7QB*8=pU|PjZ5q1|{`^ W}&wUm;8=[>u+G6y(CM|tx&=a$(b1[0T-v;}Ufhe"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	// echo "<pre>" . print_r($goData, true ) . "</pre>";
}

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

echo "\n";
echo "\n";
$t2 = microtime(true);

echo ($t2 - $t1) . ' seconds.';

echo "\n";
echo "\n";
