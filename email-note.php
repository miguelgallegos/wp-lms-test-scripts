<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$nid = $_GET['nid'];

// $cm = fb_srv('fb_coaching.coaching_manager');

// $call = $cm->findCoachingCallById($nid);

// echo 'OK Call: '.$call->getId();
// echo 'OK Note: '.$call->getNote()->getId();


// //NOTE: in notes, coach_id represents the coach that created the note, not the booked coach

$coachingReminder = fb_srv('fb_coaching.email_reminder');
$coachingReminder->coachingNote($nid);

echo "<h3>Note Email Sent for NID: $nid</h3>";

/*

book call
generate a note (this case uses MP note)
use this script (set note id)

SwiftMailerAdapter add this:

echo "<pre>TO: ". print_r($recipients, true) . "</pre>";
echo "<pre>FROM: ". print_r(array($senderAddress => $senderName), true) . "</pre>";
echo $renderedEmail->getBody();
die();


*/