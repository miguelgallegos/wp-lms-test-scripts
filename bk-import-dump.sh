#!/bin/bash
#LOCAL
DB_FILE=$2

NEW_DB=$1

CREATE_DB_FILE=sql-$1.sql

# echo $CREATE_DB_FILE
rm $CREATE_DB_FILE
echo "CREATE DATABASE $NEW_DB;" > $CREATE_DB_FILE
mysql < $CREATE_DB_FILE
mysql $NEW_DB --max_allowed_packet=32M < $DB_FILE