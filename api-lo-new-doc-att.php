<?php

//set POST variables
$host = 'http://127.0.0.1:8000/';
$url = $host . 'api/learning_objects';
$ts = date('Y-m-d H:i:s');

//SF 4:
$dateObj = '2018-01-11T15:03:01.012345Z';

$fields = array(
    'title' => 'MG Title POST (create) ' . $ts,
    'content' => 'Content ' . $ts,
    'isActive' => true,
    'createdAt' => $dateObj,
    'createdById' => 1,
    'type' => 1
);

$fields_string = json_encode($fields);

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_HEADER, false);

curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($fields_string))
);  

//execute post
$result = curl_exec($ch);

// Check for errors and display the error message
if($errno = curl_errno($ch)) {
    $error_message = curl_strerror($errno);
    echo "!!! cURL error ({$errno}):\n {$error_message}";
    die();
}


$result = json_decode($result);

echo "<pre>". print_r($result, true) . "</pre>";

//close connection
curl_close($ch);

///CREATE ATTACHMENT -- docs will contain at least 1 attachment
//open connection
$ch = curl_init();

die(' --next create attachment --');

///$aurl = $host . 'api/attachments';
$aurl = $host . 'api/learning_objects';

if (!property_exists($result, 'id')) return false;

$lo = $result->id;

$dateObj = [
    'date' => [
        'year' => 2018,
        'month' => 01,
        'day' => 05
    ],
    'time' => [
        'hour' => 10,
        'minute' => 42
    ]
];

//file name
//url
//slug?
//full path
$att = [
        'learning_object' => $lo,
        'title' => 'MG Attachment POST ' . $ts,
        'description' => 'Description ' . $ts,
        'is_active' => 1,
        'is_default' => 1,
        'url' => 'myURL',
        'created_at' => $dateObj,
        'created_by_id' => 1,
    ];

$fields_string = json_encode($att);

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $aurl);
curl_setopt($ch,CURLOPT_POST, true);
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($fields_string))
);  

//execute post
$result = curl_exec($ch);


// Check for errors and display the error message
if($errno = curl_errno($ch)) {
    $error_message = curl_strerror($errno);
    echo "!!! cURL error ({$errno}):\n {$error_message}";
}


$result = json_decode($result);

echo "<pre>RES:". print_r($result, true) . "</pre>";

//close connection
curl_close($ch);


/*
use slug to query/update
add slug to att and lo
slug is grabbed from WP -- otherwise, slugify (future)


then: 
    when in WP:
        lookup by slug
            if any perform update (PUT), get id LO and ATT
            else perform POST


*/
