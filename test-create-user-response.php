<?php 

include '../wp-load.php';

$userId = 47611;
$httpCode = 200;
$message = 'User Created/Updated Successfully';
$debugData = [
	'original_message' => 'Hey test OK!',
	'test_value1' => 123,
	'user_created' => false,
];

if ( isset( $_GET['uid'] ) ) {
	$userId = $_GET['uid'];
}

if ( isset( $_GET['code'] ) ) {
	$httpCode = $_GET['code'];
}

$res = FB_CreateUserResponse::create( $userId, $httpCode, $message, $debugData );

echo $res->send();

die();