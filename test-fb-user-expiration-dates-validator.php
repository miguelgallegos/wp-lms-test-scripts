<?php

include '../wp-load.php';

$ins = FB_UserExpirationDateValidator::getInstance();

$uid = $_GET['uid'];

$postIds = [
	//use pages, programs, live events, conferences, etc...
	162829, //IQ program
	364289, //JS curr page 
	459888, //JS program
	459887, // Core Curr
	429872, // Commercial Ac. Program
	400849, // sales and neg 
	150338,
	461425,
	459546,
	459535,
	459728, //SN live event
	459591, //Money
	459540, //rehab
	459538,//wholesaling
	459694,//mindset intensive
	459517, //REO

	386586,

			//oh oh these guys don't use the logic on user-exp-date-validator for ED/permissions checking!
			//it uses similar though, on FBTrainingHandler
			// so: do not test tranings here!
	// 461389, // webinar
	// 454928,
];

include 'test-fb-user-expiration-dates-validator-USERS.php';

// process:
// grab students, different levels and exp dates capable
// 	assign EDs one by one and test them against the same pages
// grab a bunch of pages, all types... page, program, live events, webinars
// run the function below
// make a pretty output and something to compare

$showErrorsOnly = true;
$showMessaging = !$showErrorsOnly;

$runSingle = true;

if ($runSingle) {
	$userIds = [$uid];

	$showErrorsOnly = false;
	$showMessaging = true;
}

foreach ($userIds as $uid) {

	$userPermissions = [];

	$user = get_user_by('id', $uid);

	$ins->getUserExpirationDates($uid);

	echo "<br/>";
	echo "**************************************************";
	echo "<br/>";
	echo "**************************************************";
	echo "<br/>";
	echo ' ######## <b>'.$uid . ' - ' .$user->user_login . '</b> ##########' ;
	echo "<br/>";

	echo "<br/>Expiration Date setup:";
	echo "<br/>";	

	foreach ($ins->expirationDateCollection as $key => $ed) {

		printEDs($ed);

	}

	echo "=============<br/>";

	//the goal is to use old permissions or new permissions
	$ins->skipRedirectToSalesPage = true;

	foreach ($postIds as $postId) {

		$post = get_post($postId);

		echo '<b>>> '.$postId . ' ** ' . $post->post_name . ' (' . $post->post_type . ')</b>';
		echo "<br/>";

		$terms = grabPostTerms($postId);

		echo "Has any? ";
		echo "<br/>";

		foreach ($terms as $value) {

			$color = in_array($value, $userPermissions) ? 'green' : 'white';

			echo "<div style='background-color: $color;'><b>--></b> " . $value . '</div>';
			
		}

		//TODO: don't do redir -- 
		$ins->redirectToUpsellPage( $postId, $uid ); //user should be already captured

		echo "<br/>";

	}	
}

function grabPostTerms($postId, $terms = ['fb-permissions', 'eventcategory', 'trainingcat', 'pagecategory']) {

	$list = [];
	foreach ($terms as $t) {

		$pTerms = get_the_terms($postId, $t);

		if ($pTerms) {
			foreach ($pTerms as $pTerm) {
				$list[] = $pTerm->slug;
			}
		}
	}

	return $list;
}

function getEDPermissionsAssigned($ed) {

	global $userPermissions;

	if (isset($ed['wppermissions'])) {
		
		foreach ($ed['wppermissions'] as $key => $value) {
			$userPermissions[] = $value;
		}	
	}
}

function printEDs($ed) {

	global $userPermissions;

	echo '<b>'.$ed['id'] . ' - ' . $ed['code'] . ' (' . $ed['name'] . ') @'. $ed['end_date'] . ' ' . ($ed['is_expired'] ? '<em style="background-color: red;"> >>EXP!!<< </em>' : '') . '</b>';
	echo "<br/>";

	if (isset($ed['wppermissions'])) {
		
		foreach ($ed['wppermissions'] as $key => $value) {
			$userPermissions[] = $value;
			echo ' * '.$value;
			echo "<br/>";
		}	
	}
}


// test 1:
// 52741 - eastmantest@wholesalingprofit.com -- JS?
