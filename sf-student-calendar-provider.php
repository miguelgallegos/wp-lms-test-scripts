<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

// $cid = $_GET['cid'];

//181166
// $uid = $_GET['uid'];

// fv_get_available_call_slots_totals
// fv_get_available_coach_by_calltype
// fv_get_available_slot_detail_per_day
// fv_get_available_call_slot


$rt = $_GET['rt'];

$rangeTime = explode('|', $rt);
$rangeTime = json_encode($rangeTime);

// -----
$types = $_GET['types'];
// -----
$sd = $_GET['sd'];
$ed = $_GET['ed'];

$postTotals = [
	'start_date' => $sd,
	'end_date' => $ed,
	'status' => 0,
	'range_time' => $rangeTime,
	'types' => $types,
	'timezone' => 'America/Los_Angeles',
];


// $post['range_time'] ='["00:00:01,06:00:00","06:00:01,11:00:00"]';
// $post['types'] = 'mp_u_calls,calls';

// //'["00:00:01,06:00:00","06:00:01,11:00:00","11:00:01,16:00:00","16:00:01,20:00:00","20:00:01,23:59:59"]'
// 	// $post['range_time'] = '["00:00:01,06:00:00","06:00:01,11:00:00","11:00:01,16:00:00","16:00:01,20:00:00","20:00:01,23:59:59"]';
// 	$post['range_time'] = '["00:00:01,06:00:00"]';

// 	// $post['range_time'] ='["00:00:01,06:00:00","06:00:01,11:00:00"]';
// 	// $post['range_time'] ='["00:00:01,06:00:00","16:00:01,20:00:00"]';
// 	// $post['range_time'] ='["16:00:01,20:00:00"]';
// 	$post['types'] = 'mp_u_calls,calls';

// ["00:00:01,06:00:00","06:00:01,11:00:00","11:00:01,16:00:00","16:00:01,20:00:00","20:00:01,23:59:59"]
///----------
$tots = fv_get_available_call_slots_totals($postTotals);
echo "<h4>Slot Counts</h4>";
foreach ($tots as $t) {
	echo sprintf("%s | %s | %s<br/>", $t['date'] ,$t['type'], $t['total']);
}

$postByCallType = $postTotals;

$byCallType = fv_get_available_coach_by_calltype($postByCallType);

echo "<h4>Slots By Call Type</h4>";
foreach ($byCallType as $slot) {
	echo sprintf("%s | %s<br/>", $slot['coach_id'], $slot['type']);
}
//--------
$postPerDay = [
	'status' => 0,
	'range_time' => '["2018-08-08T03:00:01Z,2018-08-08T06:59:59Z"]',
	'types' => $types
];

$perDay = fv_get_available_slot_detail_per_day($postPerDay);

echo "<h4>Slots By Call Day</h4>";
foreach ($perDay as $slot) {
	// echo "<pre>" . print_r($slot, true) . "</pre>";die();
	echo sprintf("CID: %s| coach: %s | T: %s DT: %s<br/>", $slot['slot_id'], $slot['coach_id'], $slot['call_type'], $slot['slot_time']);
}


// status: 0
// range_time: ["2018-07-25T07:00:01Z,2018-07-25T13:00:00Z","2018-07-25T13:00:01Z,2018-07-25T18:00:00Z","2018-07-25T18:00:01Z,2018-07-25T23:00:00Z","2018-07-25T23:00:01Z,2018-07-26T03:00:00Z","2018-07-26T03:00:01Z,2018-07-26T06:59:59Z"]
// types: mp_u_calls
