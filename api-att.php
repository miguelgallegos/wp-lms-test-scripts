<?php

//set POST variables
$host = 'http://127.0.0.1:8000/';
$url = $host . 'api/properties';
$ts = date('Y-m-d H:i:s');

$lo = 1;

$dateObj = '2018-01-11T15:06:01.012345Z';

$fields = [
        'learningObject' => '/api/learning_objects/1', //<<< SF 4! - it's called IRI
        'name' => 'MG Property ' . $ts,
//        'description' => 'Description ' . $ts,
//        'isActive' => true,
//        'isDefault' => true,
        'value' => 'myURL',
        'createdAt' => $dateObj,
        'createdById' => 1,
    ];

$fields_string = json_encode($fields);

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, true);
curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    'Content-Length: ' . strlen($fields_string))
);  

//execute post
$result = curl_exec($ch);


// Check for errors and display the error message
if($errno = curl_errno($ch)) {
    $error_message = curl_strerror($errno);
    echo "!!! cURL error ({$errno}):\n {$error_message}";
}


$result = json_decode($result);

echo "<pre>RES:". print_r($result, true) . "</pre>";

//close connection
curl_close($ch);