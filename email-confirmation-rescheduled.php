<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$cid = $_GET['ccid'];

$coachingReminder = fb_srv('fb_coaching.email_reminder');

// if ( isset($_GET['token']) ) {
// 	$token = $_GET['token'];
// 	$coachingReminder->setCancellationToken( $token );
// }

$coachingReminder->setLastCancelledCallId( 452274 );

$coachingReminder->confirm($cid);

echo "<h3>Reschedule Confirmation Email Sent for CID: $cid";

die();