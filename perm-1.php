<?php 

require_once('../wp-load.php');

    $post_id = $_GET['pid'];

    $terms = wp_get_post_terms( $post_id, $taxonomy = ['eventcategory', 'fb-permissions']);
// echo "<pre>". print_r($terms, true) . "</pre>";
    $p = get_post($post_id);

    //supports fb-permissions and eventcategory taxonomies
    $s4WPs = FB_Symfony4Helpers::get_lms_categories_assoc();
    $pushableCats = [];
    $permission = '';

    // $t1 = new stdClass();
    // $t1->slug = 'marketing-systems-immersion';
    // $terms = [$t1];

    foreach ($terms as $t) {
        if (array_key_exists($t->slug, $s4WPs) && $s4WPs[$t->slug]['permission'] != '') {
            //assigne category also!
            //get permission to get list of LOs! to prevent overwriting

            $permissionIRI = $s4WPs[$t->slug]['permission'];

            $permission = FB_Symfony4Helpers::get($permissionIRI);

            $permission['learningObjects'][] = '/api/learning_objects/' . $post_id;

            //call put
            $data = [
                'learningObjects' => $permission['learningObjects']
            ];
            $r = FB_Symfony4Helpers::update_permission($permissionIRI, $data);
            //todo: look for errors
        }
    }


