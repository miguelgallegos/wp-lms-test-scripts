#!/bin/bash
# example:
# ./bk-db-import-file.sh mastery-2020-01-21 dump-develop-rds-2020-01-21_01.sql
DATABASE=$1
DB_FILE=$2

# local only
#/Applications/XAMPP/bin/
# try this: --max_allowed_packet=1073741824
# set it on command line mysql: mysql -uroot --- run SET GLOBAL max_allowed_packet = 1073741824;

mysql --host=127.0.0.1 --user=root --max_allowed_packet=1073741824 --force ${DATABASE} < ${DB_FILE}