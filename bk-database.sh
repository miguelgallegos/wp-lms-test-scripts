#!/bin/bash
#LOCAL

# TODO: add example
# works for dev only
# FORMAT: ./bk-database.sh IP_SSH_of_HOST host_name_RDS_keyword timestamp
# Example (staging): ./bk-database.sh 54.219.60.181 staging-rds 2019-04
# NOTES: is this the good one?
# Create 2 databases -- give prefix (mastery-, api-)
# Give date
# Run below
# Create script to download from API.staging
# mysql -P 3308 -h 127.0.0.1 -uroot --max_allowed_packet=1073741824 mastery-2019-05 < dump-staging-rds-2019-05.sql
# mysql -P 3308 -h 127.0.0.1 -uroot api-2019-05 < ~/Downloads/20190530-fb-api-db---API\ Staging.sql
# WIP: more ~/dev/test-scripts/replace-dbs.sh 

# ADMIN:
# mysqladmin -uroot -h 127.0.0.1 -P 3308 drop mastery-2019-05
# mysqladmin -uroot -h 127.0.0.1 -P 3308 create mastery-2019-05
# mysqladmin -uroot -h 127.0.0.1 -P 3308 variables | grep packet
#	| max_allowed_packet                                     | 1048576                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
#	| slave_max_allowed_packet                               | 1073741824 
# updated buffer log two vars!
#   ERROR 1118 (42000) at line 4476: The size of BLOB/TEXT data inserted in one transaction is greater than 10% of redo log size. Increase the redo log size using innodb_log_file_size.
# show variables like 'max_allowed_packet';
# show variables like 'innodb_log_file_size';

# TEMP
 # 570* mysql -P 3308 -h 127.0.0.1 -uroot 
 #  571  mysql -P 3308 -h 127.0.0.1 -uroot mastery-2019-05
 #  572  history
 #  573  mysql -P 3308 -h 127.0.0.1 -uroot api-2019-05 < ~/Downloads/20190530-fb-api-db---API\ Staging.sql


# locate 
# sudo find / -name "*.cnf"
# /usr/local/etc/my.cnf   NOT?
# /usr/local/Cellar/mysql@5.7/5.7.25/.bottle/etc/my.cnf

# XAMPP
# /Applications/XAMPP/xamppfiles/etc/my.cnf  <<<< ---- for Sf & WP on port=3308 --- 
# /Applications/XAMPP/xamppfiles/share/mysql/my-huge.cnf
# /Applications/XAMPP/xamppfiles/share/mysql/my-innodb-heavy-4G.cnf
# /Applications/XAMPP/xamppfiles/share/mysql/my-medium.cnf
# /Applications/XAMPP/xamppfiles/share/mysql/my-large.cnf
# /Applications/XAMPP/xamppfiles/share/mysql/my-small.cnf

PASSWORD=guFCbn5zPWqz8PD
HOST=mastery-$2.cgnszbkc23ot.us-west-1.rds.amazonaws.com
USER=mastery_db_admin
DATABASE=mastery
DB_FILE=dump-$2-$3.sql
SSH_HOST=$1

#stuct only, no data
EXCLUDED_TABLES=(
service_activity_log
wp_lpt
wp_oauth_refresh_tokens
wp_oauth_access_tokens
wp_fb_coaching_user_notes 
wp_swp_log
)
#wp_fb_coaching_user_notes not so sure

EXCLUDED_FULL_TABLES=(
archive_jose_possible_unmatched_calls
archive_old_call_notes
archive_old_call_notes_postmeta
archive_old_calls
archive_old_calls_notes_p2p
archive_old_calls_postmeta
archive_old_user_notes
archive_old_user_notes_postmeta
archive_usermeta_expiration_dates
archive_usermeta_no_users
archive_vendors_postmeta
archive_vendors_posts
panche_test_ajax_time
temp_wp_posts_vimeo_forward
temp_wp_posts_vimeo_rollback
)

#ignore completely

IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_TABLES[@]}"
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done

FULL_IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_FULL_TABLES[@]}"
do :
   FULL_IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done

echo "Dump structure"
# echo "mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --no-data ${DATABASE} > ${DB_FILE}"
#/Applications/XAMPP/bin/mysqldump --host=${HOST} --user=${USER}  --single-transaction --no-data ${FULL_IGNORED_TABLES_STRING} ${DATABASE} > ${DB_FILE}
ssh ubuntu@${SSH_HOST} -i /Users/miguel/Downloads/mastery-dev-key.pem mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --no-data ${FULL_IGNORED_TABLES_STRING} ${DATABASE} > ${DB_FILE}

echo "Dump content"
# echo "mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} --no-create-info ${IGNORED_TABLES_STRING} >> ${DB_FILE}"
#/Applications/XAMPP/bin/mysqldump --host=${HOST} --user=${USER}  ${DATABASE} --no-create-info ${IGNORED_TABLES_STRING} ${FULL_IGNORED_TABLES_STRING} >> ${DB_FILE}
ssh ubuntu@${SSH_HOST} -i /Users/miguel/Downloads/mastery-dev-key.pem mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} --no-create-info ${IGNORED_TABLES_STRING} ${FULL_IGNORED_TABLES_STRING} >> ${DB_FILE}
#DO THE IMPORT HERE!!