<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$coachid = $_GET['cid'];

$selected_time = strtotime('2017-08-10 11:00:00');


$time_start = conv_epoch($selected_time);
$inc = fb_is_startup_specialist_coach($coachid) ? 45 : 30;

$time_end = conv_epoch($selected_time + ($inc * 60));
echo $coachid;
echo "<br/>";
echo 'Start Time: ' .$time_start;
echo "<br/>";
echo 'End Time ' . date('Y-m-d H:i:s', strtotime($time_end));
