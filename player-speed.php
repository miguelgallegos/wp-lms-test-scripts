<!DOCTYPE html>
<!--[if IEMobile 7 ]> <html id="iem7" lang="en-US"><![endif]-->
<!--[if lt IE 7]>     <html id="ie6" lang="en-US"> <![endif]-->
<!--[if IE 7]>        <html id="ie7" lang="en-US"> <![endif]-->
<!--[if IE 8]>        <html id="ie8" lang="en-US"> <![endif]-->
<!--[if IE 9]>        <html id="ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en-US"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
<link rel="dns-prefetch" href="//ajax.googleapis.com" />
<link rel="dns-prefetch" href="//s3.amazonaws.com" />
<link rel="dns-prefetch" href="//mastery-site.s3.amazonaws.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="cleartype" content="on" />
<title>Overview and Basic Learning Objectives | FortuneBuilders</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="http://develop.fbmastery.com/xmlrpc.php">


<!-- FB Icons -->
<link rel="icon" href="http://develop.fbmastery.com/fb-favicon.ico">
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#DBF2FF">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="application-name" content="FortuneBuilders"/>

<!-- Google Web Fonts -->
<link href='http://fonts.googleapis.com/css?family=Sanchez|Raleway:100,400,700,900' rel='stylesheet' type='text/css'>

<meta name='robots' content='noindex,follow' />
<link rel="alternate" type="application/rss+xml" title="FortuneBuilders &raquo; Feed" href="http://develop.fbmastery.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="FortuneBuilders &raquo; Comments Feed" href="http://develop.fbmastery.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="FortuneBuilders &raquo; Overview and Basic Learning Objectives Comments Feed" href="http://develop.fbmastery.com/lesson/overview-and-basic-learning-objectives/feed/" />
<link rel='stylesheet' id='wp-admin-notes-css'  href='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/UserNotes/admin/../css/wp-admin-notes.css?ver=4.1.2' type='text/css' media='all' />
<link rel='stylesheet' id='fblms-css'  href='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/core-plugins/LMS/css/fblms.css?ver=4.1.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-ui-css'  href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css?ver=4.1.2' type='text/css' media='all' />
<link rel='stylesheet' id='fb-coaching-styles-css'  href='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/css/fb-coaching-styles.css?ver=2.2' type='text/css' media='all' />
<link rel='stylesheet' id='redactor-styles-css'  href='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/redactor/redactor.css?ver=2.0' type='text/css' media='all' />
<link rel='stylesheet' id='daterangepicker-styles-css'  href='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/css/daterangepicker.css?ver=2.0' type='text/css' media='all' />
<link rel='stylesheet' id='fb_coaching_teams-frontend-css'  href='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-teams/assets/css/frontend.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='owl-carousel-css-css'  href='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-wins-slider/assets/css/owl.carousel.css?ver=4.1.2' type='text/css' media='all' />
<link rel='stylesheet' id='bp-parent-css-css'  href='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/buddypress/css/buddypress.css?ver=2.0.1' type='text/css' media='screen' />
<link rel='stylesheet' id='slidenav-css-css'  href='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/inc/slidenav/slidenav.css?ver=2.1' type='text/css' media='all' />
<link rel='stylesheet' id='googleFonts-css'  href='http://fonts.googleapis.com/css?family=Droid+Sans%3A400%2C700&#038;ver=4.1.2' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/inc/fontawesome/css/font-awesome.min.css?ver=2.0' type='text/css' media='all' />
<link rel='stylesheet' id='fb_mastery-style-css'  href='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/style.css?ver=4.1.2' type='text/css' media='all' />
<link rel='stylesheet' id='fb_mastery-less-css'  href='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/layouts/mastery.css?ver=2.5.36' type='text/css' media='all' />
<link rel='stylesheet' id='file-stack-css-css'  href='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/layouts/file-stack.css?ver=4.1.2' type='text/css' media='all' />
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.4/jstz.min.js?ver=4.1.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var jstzz = jstz.determine(); var FBUserTimezone = { 'tz': jstzz, 'tz_no': 'false', 'tz_no_time': false, 'saved_tz': 'America/Los_Angeles', 'saved_gmt': 1496436041, 'fire_update': true, 'user_id' : 47611 };
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/core-plugins/fb-time-zone-utility/js/fb-user-time-zone.js?ver=2.0.3'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/position.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/menu.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/autocomplete.min.js?ver=1.11.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var teamsajax = {"ajaxurl":"http:\/\/develop.fbmastery.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/Teams/js/fb-teams.js'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/plupload/plupload.full.min.js?ver=2.1.1'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/js/moment.min.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-teams/assets/js/frontend.min.js?ver=1.0.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var FBUserBookmark = {"ajaxurl":"http:\/\/develop.fbmastery.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-user-bookmarks/js/bookmark.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-wins-slider/assets/js/owl.carousel.min.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-wins-slider/assets/js/fb-wins-slider.js?ver=4.1.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var BP_DTheme = {"accepted":"Accepted","close":"Close","comments":"comments","leave_group_confirm":"Are you sure you want to leave this group?","mark_as_fav":"Favorite","my_favs":"My Favorites","rejected":"Rejected","remove_fav":"Remove Favorite","show_all":"Show all","show_all_comments":"Show all comments for this thread","show_x_comments":"Show all %d comments","unsaved_changes":"Your profile has unsaved changes. If you leave the page, the changes will be lost.","view":"View"};
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/plugins/buddypress/bp-templates/bp-legacy/js/buddypress.js?ver=2.0.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var BP_Confirm = {"are_you_sure":"Are you sure?"};
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/plugins/buddypress/bp-core/js/confirm.min.js?ver=2.0.1'></script>
<script type='text/javascript' src='http://p.jwpcdn.com/6/4/jwplayer.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-live-search/hogan-2.0.0.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-live-search/typeahead/typeahead.bundle.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-live-search/live-search.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/media.match.min.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/enquire.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/picturefill.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/matchmedia.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/scroll.js?ver=4.1.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var FBPUO = {"ajaxurl":"http:\/\/develop.fbmastery.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-persistent-user-options/js/puo.js?ver=4.1.2'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://develop.fbmastery.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://develop.fbmastery.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.1.2" />
<link rel='canonical' href='http://develop.fbmastery.com/lesson/overview-and-basic-learning-objectives/' />
<link rel='shortlink' href='http://develop.fbmastery.com/?p=267211' />

<script type="text/javascript">var fblmsURL = "http://develop.fbmastery.com/wp-content/plugins/fb-plugins/core-plugins/LMS/"</script>
<script type="text/javascript">var homeURL = "http://develop.fbmastery.com"</script>
<script type="text/javascript">var themeURL = "http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master"</script>
<script type="text/javascript">var ajaxURL = "http://develop.fbmastery.com/wp-admin/admin-ajax.php"</script>

	<script type="text/javascript">var ajaxurl = 'http://develop.fbmastery.com/wp-admin/admin-ajax.php';</script>

	<!--QUIZ_EMBEDER START-->
	<link rel="stylesheet" href="http://develop.fbmastery.com/wp-content/plugins/fb-plugins/plugins/insert-or-embed-articulate-content-into-wordpress/colorbox/colorbox.css" />
	<script type="text/javascript" src="http://develop.fbmastery.com/wp-content/plugins/fb-plugins/plugins/insert-or-embed-articulate-content-into-wordpress/colorbox/jquery.colorbox-min.js" ></script>
	<script>
		jQuery(document).ready(function($){
			//Examples of how to assign the ColorBox event to elements
			$(".colorbox_iframe").colorbox({iframe:true, width:"80%", height:"80%"});
		});
	</script>	
	<!--QUIZ_EMBEDER END-->
<script type="text/javascript">jwplayer.key='GmO+WHexZjF+7plhNagFNz7n8ba3gwxhTBS6dtgtvm0=';jwplayer.defaults = { "ph": 2 };</script><link rel="stylesheet" href="http://develop.fbmastery.com/wp-content/plugins/wp-page-numbers/classic/wp-page-numbers.css" type="text/css" media="screen" />
<!--	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
	<script>
		jQuery(document).ready(function($){
			//$('#loginform').replaceWith('<div class="maintenance">The site is currently undergoing maintenance.</div>');
			$('#login #nav').replaceWith('<div class="than-error"><img src="http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/img/than-error.jpg"></div>');
			$('#loginform').hide();
			//$('.message').hide();
			//$('#login #nav').hide();
			$('#input-search').on('keydown change paste keyup', function(e){
				var value=$.trim($("#input-search").val());
				if(value.length == 0)
				{
					$("#input-search").val("");
				 //do some stuffs. 
				}
			    if(e.keyCode == 32 && $(this).val().indexOf(' ') == 0){
			        return false;
			    }else if( e.keyCode == 32 && this.value.length == 0 ){
			        return false;
			    }
			});
		});
	</script>

</head>

<body class="single single-fblms_lesson postid-267211 logged-in  no-hero no-js">
<!-- timezone change alert -->
<div id="timezone-change-alert" class="alert alert-warning text-center">
	<p>A new timezone was detected. Would you like to update your timezone to display booking information in the <span class="new-timezone alert-success"><strong><script>document.write(FBUserTimezone.tz.name())</script></strong></span> timezone? 
	<button class="btn btn-default btn-sm" id="change-tz-yes">Yes</button>
	<button class="btn btn-default btn-sm" id="change-tz-no">Not now</button>
	<i class="msg">Updating Timezone Settings ...</i>
	<p>
</div>
<script> var title = 'Overview and Basic Learning Objectives'; </script>
<script>
    var user_id = 47611;
    var infusion_id = meta_value;
    var start_date = '2016-08-16 19:04:02';
    var level = 'FB Platinum';
    var enrollment_status = 'Alumni';
    var business_role = 'Partner';
</script>
<script>var curr_permission_names = ["All","Platinum","Full Immersion","Marketing Immersion","Nch Silver","Commercial Academy","Office Systems Academy","Ultimate Money Academy","Asset Protection And Tax Planning","Mastery Student","Rental Property Intensive","Mindset Intensive","Mastery   Status First 30 Days","Mastery   Status First 90 Days","Mastery   Status After 14 Days","Mastery   Status After 30 Days","Mastery   Status After 90 Days","Mastery   Status After 180 Days","User_47611"];</script><div id="page" class="hfeed site bg_main">
	
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding bg_siteHeader">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-3">
						<a href='http://develop.fbmastery.com/' title='FortuneBuilders' rel='home'>
							
					<div class='site-logo'>
						<img src='http://develop.fbmastery.com/wp-content/uploads/2015/05/fb-mastery-logo.png' alt='FortuneBuilders'>
					</div>						</a>
					</div>
				</div>
			</div>
		</div>

		<nav class="navbar navbar-mastery" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header container">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class='menu-cell'>
							<button type="button" class="featured-btn navbar-toggle group-btn" data-toggle="collapse" data-parent=".nav-menus" data-target=".navbar-ex1-collapse">
																	<img class='icon-logo' src='http://develop.fbmastery.com/apple-touch-icon-57x57-precomposed.png' alt='FortuneBuilders'>
															</button>
						</td>
						<td class='search-cell'>
							<button type="button" class="btn close-search btn-mastery-dark btn-block">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</button>
							<form role="search" method="get" id='site-search' class="search-form" action="http://develop.fbmastery.com/">
								<div class="input-group">

									<input type="hidden" name="search-type" value="site">

									<input type="search" id="input-search" style="text-align: left;" class="search-field form-control expandinput" placeholder="Search &hellip;" value="" name="s" title="Search for:" />

									<input type="hidden" name="isUserJS" id="isUserJS" value="" >

									<div class="input-group-btn">
										<button type="submit" class="search-submit-btn btn btn-mastery-dark"><span class="glyphicon glyphicon-search"></span><span class="sr-only">Submit Search</span></button>
										<span class="glyphicon glyphicon-search search-icon"></span>
									</div>
								</div><!-- /input-group -->
							</form>
						</td>
																		<td class='bookmarks-cell' >
							<div class="group-btn bookmarks">
								<button type="button" class="btn bookmarks-btn btn-mastery-dark btn-block" data-toggle="collapse" data-parent=".nav-menus" data-target=".navbar-ex3-collapse" rel="custom-tooltip" title="Notes &amp; Bookmarks." data-placement="bottom">
	                    			<span class="glyphicon glyphicon-bookmark bookmarks-icon"></span>
	                    		</button>
	                    		<ul class='dropdown-menu'>
	                    												<li>
					    				<a title='Notes &amp; Bookmarks' href='http://develop.fbmastery.com/my-notes/'><span class="fa fa-search"></span> Browse all your Notes &amp; Bookmarks.</a>
					    			</li>
					    		</ul>
                    		</div>
						</td>
												<td class='account-cell'>
							<div class="my-account">
								<button type="button" class="account-welcome" data-toggle="collapse" data-parent=".nav-menus" data-target=".navbar-ex4-collapse">
									<span class="sr-only">Toggle navigation</span>
									<img src="http://gravatar.com/avatar/ddc1a135cf2c1891a017b4a32e968250?d=mm&amp;s=50&amp;r=G" class="avatar user-47611-avatar avatar- photo" width="50" height="50" alt="Avatar Image" /> <span class="btn-text">My Account</span> <span class="caret"></span>
								</button>
								<ul id="menu-account-menu" class="dropdown-menu">									<li id="menu-item-7243" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7243">
										<a title="Settings" href="http://develop.fbmastery.com/members/miguel-gallegos/settings/"><span class="fa fa-gear"></span> Settings</a>
									</li>
									<li id="menu-item-7245" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7245">
										<a title="View My Profile" href="http://develop.fbmastery.com/members/miguel-gallegos"><span class="fa fa-user"></span> View My Profile</a>
									</li>
                                                                        <li id="menu-item-7246" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7246">
										<a title="Edit Profile" href="http://develop.fbmastery.com/members/miguel-gallegos/profile/edit"><span class="fa fa-pencil"></span> Edit Profile</a>
									</li>
									<li id="menu-item-7246" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7247">
										<a title="Edit Profile Picture" href="http://develop.fbmastery.com/members/miguel-gallegos/profile/change-avatar"><span class="fa fa-user-plus"></span> Edit Profile Picture</a>
									</li>
									<li id="menu-item-7247" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7248">
										<a title="Log Out" href="http://develop.fbmastery.com/logout?_wpnonce=862004d01b"><span class="fa fa-sign-out"></span> Log Out</a>
									</li>
								</ul>
							</div>
						</td>
					</tr>
				</table>

			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class='nav-menus bg_siteNav'>
				<div class="container panel primary-menu">
			    	<div class="collapse navbar-collapse navbar-ex1-collapse"><ul id="menu-main-menu" class="nav navbar-nav"><li id="menu-item-130149" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-130149"><a title="Home" href="/"><span class="glyphicon glyphicon-home"></span>Home</a></li>
<li id="menu-item-130153" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-130153 dropdown"><a title="Learning" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-lightbulb"></span>Learning <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-347267" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-347267"><a title="My Mastery Curriculum" href="http://develop.fbmastery.com/my-phases-curriculum-2/">My Mastery Curriculum</a></li>
	<li id="menu-item-430749" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-430749"><a title="Commercial Academy" href="http://develop.fbmastery.com/commercial-academy/">Commercial Academy</a></li>
	<li id="menu-item-401868" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-401868"><a title="My Action Plans" href="http://develop.fbmastery.com/my-action-plans/">My Action Plans</a></li>
	<li id="menu-item-293890" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-293890"><a title="Future FortuneBuilders Online Training Center" href="http://develop.fbmastery.com/ffb-curriculum/">Future FortuneBuilders Online Training Center</a></li>
	<li id="menu-item-130274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130274"><a title="Course Catalog" href="http://develop.fbmastery.com/course-catalog/">Course Catalog</a></li>
	<li id="menu-item-261796" class="menu-item menu-item-type-taxonomy menu-item-object-eventcategory menu-item-261796"><a title="Live Events" href="http://develop.fbmastery.com/eventcategory/mastery-live-events/">Live Events</a></li>
	<li id="menu-item-408718" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-408718"><a title="Upcoming Webinars" href="http://develop.fbmastery.com/webinars/">Upcoming Webinars</a></li>
	<li id="menu-item-130282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130282"><a title="My Notes and Bookmarks" href="http://develop.fbmastery.com/my-notes/">My Notes and Bookmarks</a></li>
	<li id="menu-item-436685" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-436685"><a title="Welcome &amp; Getting Started Videos" href="http://develop.fbmastery.com/welcome-getting-started-videos-mastery/">Welcome &#038; Getting Started Videos</a></li>
</ul>
</li>
<li id="menu-item-400785" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-400785 dropdown"><a title="Field Experts" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-parents"></span>Field Experts <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-400796" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-400796"><a title="My Calls" href="http://develop.fbmastery.com/my-coaching/">My Calls</a></li>
</li>
</ul>
</li>
<li id="menu-item-130293" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-130293 dropdown"><a title="Community" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-group"></span>Community <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-130298" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130298"><a title="Members Directory" href="http://develop.fbmastery.com/members/">Members Directory</a></li>
	<li id="menu-item-130300" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130300"><a title="Facebook Group" href="http://develop.fbmastery.com/community/mastery-facebook-group/">Facebook Group</a></li>
	<li id="menu-item-425616" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-425616"><a title="Submit a Win" href="http://develop.fbmastery.com/submit-your-deal">Submit a Win</a></li>
</ul>
</li>
<li id="menu-item-130306" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-130306 dropdown"><a title="Resources" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-settings"></span>Resources <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-130308" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-130308"><a title="Documents" href="http://develop.fbmastery.com/documents/">Documents</a></li>
	<li id="menu-item-432449" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-432449"><a title="Acceleration Services" href="http://develop.fbmastery.com/acceleration-services/">Acceleration Services</a></li>
	<li id="menu-item-433715" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-433715"><a title="Realeflow" href="http://develop.fbmastery.com/realeflow/">Realeflow</a></li>
	<li id="menu-item-432448" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-432448"><a title="FB Investor Toolbox" href="http://develop.fbmastery.com/fb-investor-toolbox/">FB Investor Toolbox</a></li>
</ul>
</li>
<li id="menu-item-130318" class="last-item menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-130318 dropdown"><a title="Support" href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-life_preserver"></span>Support <span class="caret"></span></a>
<ul role="menu" class=" dropdown-menu">
	<li id="menu-item-399403" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-399403"><a title="Student Support" href="http://develop.fbmastery.com/support/">Student Support</a></li>
</ul>
</li>
</ul></div>				</div>
				<div class="container panel search-menu">
					<div class='collapse navbar-collapse navbar-ex2-collapse'>
						<ul class='nav navbar-nav search-dropdown'>
			    		</ul>
			    	</div>
				</div>
				<div class="container panel bookmarks-menu">
					<div class='collapse navbar-collapse navbar-ex3-collapse'>
						<ul class='nav navbar-nav'>
                    											<li>
				    				<a title='Notes &amp; Bookmarks' href='http://develop.fbmastery.com/my-notes/'>Browse all your Notes &amp; Bookmarks.</a>
				    			</li>
			    		</ul>
			    	</div>
				</div>
				<div class="container panel my-account-menu">
			    	<div class="collapse navbar-collapse navbar-ex4-collapse">
			    		<ul id="menu-learning" class="nav navbar-nav">
						    			    			<li id="menu-item-7243" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7243">
								<a title="Settings" href="http://develop.fbmastery.com/members/miguel-gallegos/settings/">Settings</a>
							</li>
							<li id="menu-item-7245" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7245">
								<a title="Member Profile" href="http://develop.fbmastery.com/members/miguel-gallegos">Member Profile</a>
							</li>
							<li id="menu-item-7246" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7246">
								<a title="Log Out" href="http://develop.fbmastery.com/logout?_wpnonce=862004d01b">Log Out</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
	</header><!-- #masthead -->

	<div id="content" class="site-content">

		<script> post = {"ID":267211,"post_author":"7015","post_date":"2015-01-16 19:37:05","post_date_gmt":"2015-01-16 19:37:05","post_content":"","post_title":"Overview and Basic Learning Objectives","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"overview-and-basic-learning-objectives","to_ping":"","pinged":"","post_modified":"2015-08-04 22:00:22","post_modified_gmt":"2015-08-04 22:00:22","post_content_filtered":"","post_parent":0,"guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267211","menu_order":0,"post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","filter":"raw","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null}; </script><script> parent_course_title = "Gathering Property Information From Sellers"; </script><script> parent_course_href = "http://develop.fbmastery.com/courses/gathering-property-information-from-sellers/"; </script><script> progress = false; </script><script> course_complete = "false"; </script><script> total_lessons = 23 ; </script><script> program_name = "fortunebuildersu-default-phases-1"; </script>
<input type="hidden" id="lesson_id" value="267211">
<input type="hidden" id="lesson_length" value="298">
<input type="hidden" id="course_id" value="267210">
<article id="post-267211" class="post-267211 fblms_lesson type-fblms_lesson status-publish hentry">
	<header class="page-header-wrap">
		<div class="page-header bg_pageHeader">
	        <div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h1>
							Overview and Basic Learning Objectives <span class='glyphicon glyphicon-ok_2'></span>
						</h1>
						<div class='header-controls'>
							<a href="http://develop.fbmastery.com/courses/gathering-property-information-from-sellers/" class="btn btn-circle" title='Return To Course' id='return-course'>
								<span class="glyphicon glyphicon-message_forward"></span> <span class='btn-circle-text'>Return To Course</span>
							</a>
							<a class="btn btn-circle save-bookmark" data-obj="267211" data-type="Lesson"  title='Bookmark Lesson' id='bookmark-lesson'>
								<span class="glyphicon glyphicon-bookmark"></span> <span class='btn-circle-text'>Bookmark</span>
							</a>
							<button id="startTourBtn" class="btn btn-circle" title='Tour This Page'>
								<span class="glyphicon glyphicon-send"></span> <span class='btn-circle-text'>Tour This Page</span>
							</button>
						</div>
						<div class="progress-mastery progressdiv" style="display: block;">
							<div class="progress">
								<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div>
							</div>
							<div id="number_lessons" class="pull-right small">
								Gathering Property Information From Sellers: <span id='267210_lessons_complete_number'>0</span> of 23 Lessons Complete							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header><!-- .entry-header -->

	<div id="lesson-contain" class="container player-full-width-off">
        <div class="row">
            <div class="sidebar-right sidebar-top col-xs-12 col-md-4">
				
				<div class="button-box">
					
					<button id='enlarge-view' class='full-width-player btn btn-normal ' data-video-view="standard"><span class='glyphicon glyphicon-resize_full'></span> <span class='full-width-player-text'>Enlarge View</span></button>

                                        <button type="button" class="mark_lesson btn btn-step btn-lg mark-complete" data-mark-id='267211' data-lpt-id="0" data-parent-course-id="267210" data-lesson-completed="false" data-mark-type='lesson' data-current-lesson='true'>
                                            <span class="glyphicon glyphicon-ok_2"></span> Complete Lesson					</button>
                                                                                    <button class="autoplay-button btn btn-normal " id="autoplay" data-autoplay="false"><span class="glyphicon glyphicon-history"></span> Continuous Play</button>
                                                                                    
                                                                                    <div>Playback Rate</div>
                                                                                    <div>
                                                                                    	<button onclick="jwplayerSetPlaybackRate(1)">1</button>
                                                                                    	<button onclick="jwplayerSetPlaybackRate(2)">2</button>
                                                                                    	<button onclick="jwplayerSetPlaybackRate(3)">3</button>
                                                                                    </div>
                                        					<!-- <a id='download-button' class="btn btn-normal"><span class="glyphicon glyphicon-download_alt"></span> Download Video </a> -->
				</div>
			</div>

			<div class="lesson-content col-xs-12 col-md-8">
				<div class="panel panel-lesson-video">
					<div class="panel-heading">
						<div class='video-player-wrap media16x9'><div class='jwplayer' id='jwplayer-0'></div><script type='text/javascript'>if(typeof(jQuery)=="function"){(function($){$.fn.fitVids=function(){}})(jQuery)};jwplayer('jwplayer-0').setup({"aspectratio":"16:9","width":"100%","primary":"html5","ga":{},"sources":[{"file":"rtmp://s122hsesdou97k.cloudfront.net/cfx/st/mp4:gathering-property-information-from-sellers/Overview-Overview_basic_learning_objectives.mp4"},{"file":"http://d2pkmz7he7d1py.cloudfront.net/gathering-property-information-from-sellers/Overview-Overview_basic_learning_objectives.mp4"}]});
</script></div>					</div>
					
					<div class="panel-body btn-cols-5">
						<div class='prev-button-wrap btn-col' id='previous-button'>
							
		<button type="button" class="btn btn-nav btn-state-active prev-less-btn" disabled="disabled">
			<span class="glyphicon glyphicon-chevron-left"></span> Previous Lesson
		</button>

							</div>
						<div class='enlarge-button-wrap btn-col'>
							<button id='enlarge-view' class='full-width-player btn btn-normal ' data-video-view="standard"><span class='glyphicon glyphicon-resize_full'></span> <span class='full-width-player-text'>Enlarge View</span></button>
						</div>
						<div class='complete-button-wrap btn-col' id='mark-lesson'>
							<button type="button" class="mark_lesson btn btn-step mark-complete" data-mark-id='267211' data-lpt-id="0" data-parent-course-id="267210" data-lesson-completed="false" data-mark-type='lesson' data-current-lesson='true'>
								<span class="glyphicon glyphicon-ok_2"></span> Complete Lesson							</button>
						</div>
                                                                                                    <div class='autoplay-button-wrap btn-col'>
                                                            <button class="autoplay-button btn btn-normal " id="autoplay" data-autoplay="false"><span class="glyphicon glyphicon-history"></span> Continuous Play</button>
                                                    </div>
                                                						<div class='next-button-wrap btn-col' id='next-button'>
									<a href="http://develop.fbmastery.com/lesson/course-goals-for-experienced-investors/" class="next-less-btn">
			<button type="button" class="btn btn-nav">
				<span class="btn-next-text">Next </span>Lesson <span class="glyphicon glyphicon-chevron-right"></span>
			</button>
		</a>
							</div>
					</div>

				</div>
			</div>
<script type="text/javascript">


var jesuisVideo = null;
jwplayer().onReady(function(){
	
	theVideo = document.querySelector('video');
	console.log(theVideo);
	jesuisVideo = theVideo;
});

	
	function checkPlayer(){
		var player = jwplayer('jwplayer-0');
		console.log(player);
	}

	function jwplayerSetPlaybackRate(rate){
//		var player = jwplayer('jwplayer-0');
//		console.log(player);
//		console.log(jwplayer().getPosition());
//		player.playbackRate = rate;
//	jwplayer().seek(jwplayer().getPosition());
		jesuisVideo.playbackRate = rate;
	}

</script>
			<div class="sidebar-right sidebar-middle col-xs-12 col-md-4">
				<div class="panel panel-mastery-notes panel-mastery">
                    <div class="panel-heading" id="my-notes">My Notes</div>
                        <div class="panel-body lesson-notebox"><div class="lesson-notebox-new">
                <form id="note_form" class="form-inline" name="note_form" method="post" action="" role="form">
                    <div id="note-response"></div>
                    <textarea id="note_text" name="note_text" rows="5" placeholder="Write your notes here" style="margin-bottom: 15px;" class="form-control"></textarea>
                    <input type="hidden" name="post_id" id="post_id" value="267211" />
                </form>
                <button type="button" id="save_new_note" class="btn btn-final pull-right"><span class="glyphicon glyphicon-floppy_saved"></span> Save Note</button>
            </div><div id="results" class="clearfix">
                                    <ul id="old_notes" class="old_notes"></ul>
                                </div>
                            </div>
                        </div><div class="panel panel-mastery"><div class="panel-heading" id="resources">Lesson Resources</div>
                    <div class="panel-body">
                        <ul class="resources"><a href="https://s3.amazonaws.com/Fortunebuilders/mastery4/Documents/lms/Seller-Lead-Sheet.zip" data-type="resource" data-object-id="267257" data-title="Seller Lead Sheet" data-extension="zip" target="_blank" class="document-link" id="Seller Lead Sheet"><li class='file-icon zip'>Seller Lead Sheet</li></a><a href="http://s3.amazonaws.com/fbmastery/Scripts%20-%20Seller%20Lead%20Sheet%20Company%20Description.doc" data-type="resource" data-object-id="267259" data-title="Seller Lead Sheet Company Description" data-extension="doc" target="_blank" class="document-link" id="Seller Lead Sheet Company Description"><li class='file-icon doc'>Seller Lead Sheet Company Description</li></a><a href="http://s3.amazonaws.com/fbmastery/Scripts%20-%20Seller%20Lead%20Sheet%20Company%20Description.pdf" data-type="resource" data-object-id="267263" data-title="Seller Lead Sheet Company Description.pdf" data-extension="pdf" target="_blank" class="document-link" id="Seller Lead Sheet Company Description.pdf"><li class='file-icon pdf'>Seller Lead Sheet Company Description.pdf</li></a><a href="https://fbmastery.s3.amazonaws.com/Seller-Lead-Sheet-Questions-Verbiage.docx" data-type="resource" data-object-id="388763" data-title="Seller Lead Sheet Question Verbiage" data-extension="docx" target="_blank" class="document-link" id="Seller Lead Sheet Question Verbiage"><li class='file-icon docx'>Seller Lead Sheet Question Verbiage</li></a><a href="http://s3.amazonaws.com/Fortunebuilders/mastery4/transcripts/deal-analysis/01-Gathering-Prop-Info-Sellers-Final-Transcripts.zip" data-type="resource" data-object-id="395101" data-title="Gathering Property Information From Sellers Course Transcript" data-extension="zip" target="_blank" class="document-link" id="Gathering Property Information From Sellers Course Transcript"><li class='file-icon zip'>Gathering Property Information From Sellers Course Transcript</li></a>          </ul>
                    </div>
              </div>			</div>
			
			<div class="col-xs-12 col-md-8 clearfix">
								<div id='267210' class='course-syllabus-container'>
					
<div class='panel-contain panel-type-syllabus'>
    <div class="panel panel-mastery">
                    <a data-toggle="collapse" href="#syllabus">
                <div class="panel-heading">
                    Class Syllabus <span class="halfling halfling-chevron-down"></span>
                </div>
            </a>
                
        <div id="syllabus" class="in">

            
            <div class="panel-body">
                <h4>Overview</h4>

                <section class="lessons">
                    
                        <div class='user-row clearfix current-lesson'>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete mark-status-current' data-mark-type='lesson' data-mark-id='267211' data-lesson-completed='false' data-current-lesson='true' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/overview-and-basic-learning-objectives/">
                                Overview and Basic Learning Objectives                            </a>
                        </span>
                        <span class='row-detail'>
                            4m 58s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267212' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/course-goals-for-experienced-investors/">
                                Course Goals for Experienced Investors                            </a>
                        </span>
                        <span class='row-detail'>
                            2m 46s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267213' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/what-to-expect-from-seller-leads/">
                                What to Expect from Seller Leads                            </a>
                        </span>
                        <span class='row-detail'>
                            6m 29s                        </span>
                    </div>
                                                        </section>
            </div>
            
            <div class="panel-body">
                <h4>Communicating with Sellers</h4>

                <section class="lessons">
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267214' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/keys-to-building-trust-and-rapport/">
                                Keys to Building Trust and Rapport                            </a>
                        </span>
                        <span class='row-detail'>
                            11m 9s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267215' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/how-to-describe-your-company-to-a-seller/">
                                How to Describe Your Company to a Seller                            </a>
                        </span>
                        <span class='row-detail'>
                            6m 59s                        </span>
                    </div>
                                                        </section>
            </div>
            
            <div class="panel-body">
                <h4>Seller Lead Sheet</h4>

                <section class="lessons">
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267216' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/overview-of-the-seller-lead-sheet/">
                                Overview of the Seller Lead Sheet                            </a>
                        </span>
                        <span class='row-detail'>
                            3m 46s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267217' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/property-and-contact-information-section/">
                                Property and Contact Information Section                            </a>
                        </span>
                        <span class='row-detail'>
                            7m 6s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267218' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/repairs-and-listing-information-section/">
                                Repairs and Listing Information Section                            </a>
                        </span>
                        <span class='row-detail'>
                            15m 33s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267220' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/motivation-section/">
                                Motivation Section                            </a>
                        </span>
                        <span class='row-detail'>
                            8m 48s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267221' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/mortgage-information-section/">
                                Mortgage Information Section                            </a>
                        </span>
                        <span class='row-detail'>
                            10m 58s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267225' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/price-section/">
                                Price Section                            </a>
                        </span>
                        <span class='row-detail'>
                            8m 27s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267227' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/subject-to-section/">
                                Subject-To Section                            </a>
                        </span>
                        <span class='row-detail'>
                            2m 10s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267230' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/other-people-involved-and-decision-making-section/">
                                Other People Involved and Decision Making Section                            </a>
                        </span>
                        <span class='row-detail'>
                            4m 23s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267231' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/internal-office-information-only-section/">
                                Internal Office Information Only Section                            </a>
                        </span>
                        <span class='row-detail'>
                            1m 13s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267232' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/setting-appointments/">
                                Setting Appointments                            </a>
                        </span>
                        <span class='row-detail'>
                            5m 27s                        </span>
                    </div>
                                                        </section>
            </div>
            
            <div class="panel-body">
                <h4>Options for Taking Leads</h4>

                <section class="lessons">
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267233' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/options-for-taking-leads/">
                                Options for Taking Leads                            </a>
                        </span>
                        <span class='row-detail'>
                            3m 58s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267236' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/following-up-with-your-leads/">
                                Following Up with Your Leads                            </a>
                        </span>
                        <span class='row-detail'>
                            6m 38s                        </span>
                    </div>
                                                        </section>
            </div>
            
            <div class="panel-body">
                <h4>Realeflow and Managing Seller Leads</h4>

                <section class="lessons">
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267237' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/creating-a-property-and-seller-contact-record/">
                                Creating a Property and Seller Contact Record                            </a>
                        </span>
                        <span class='row-detail'>
                            10m 54s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267238' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/uploading-the-seller-lead-sheet/">
                                Uploading the Seller Lead Sheet                            </a>
                        </span>
                        <span class='row-detail'>
                            2m 51s                        </span>
                    </div>
                                                        </section>
            </div>
            
            <div class="panel-body">
                <h4>Next Steps</h4>

                <section class="lessons">
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267239' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/how-to-practice/">
                                How to Practice                            </a>
                        </span>
                        <span class='row-detail'>
                            2m 18s                        </span>
                    </div>
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267240' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/the-next-steps/">
                                The Next Steps                            </a>
                        </span>
                        <span class='row-detail'>
                            54s                        </span>
                    </div>
                                                        </section>
            </div>
            
            <div class="panel-body">
                <h4>Scenario Examples and Activity</h4>

                <section class="lessons">
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='267243' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/seller-lead-sheet-call-examples/">
                                Seller Lead Sheet – Call Examples                            </a>
                        </span>
                        <span class='row-detail'>
                            45m                         </span>
                    </div>
                                                        </section>
            </div>
            
            <div class="panel-body">
                <h4>Course Quiz</h4>

                <section class="lessons">
                                                                <div class='user-row'>
                            <a href="http://develop.fbmastery.com/quiz/gathering-property-information-from-sellers-quiz/">Gathering Property Information from Sellers Quiz</a>
                        </div>
                                    </section>
            </div>
            
            <div class="panel-body">
                <h4>Course Survey</h4>

                <section class="lessons">
                    
                        <div class='user-row clearfix '>
    
                        <button title='Mark Lesson Complete' class='btn-mark-circle mark-status-incomplete ' data-mark-type='lesson' data-mark-id='273862' data-lesson-completed='false' data-current-lesson='false' style="display: block;"></button>                        <span class='row-name'>
                            <a href="http://develop.fbmastery.com/lesson/new-lesson-1850/">
                                Fill Out Course Survey                            </a>
                        </span>
                        <span class='row-detail'>
                            1m                         </span>
                    </div>
                                                        </section>
            </div>
                        <div class='panel-footer' style="display: block;">
                <button type="button" id="mark_course" class="mark_course btn btn-step pull-right mark-course-complete" data-title="Gathering Property Information From Sellers" data-url="http://develop.fbmastery.com/courses/gathering-property-information-from-sellers/" data-mark-type='course' data-mark-id='267210' data-course-completed='false'>
                    <span class="glyphicon glyphicon-ok_2"></span> Complete Course                </button>
            </div>
        </div>
    </div>
</div>
<script> course = {"ID":"267210","post_author":"1","post_date":"2013-06-16 19:05:27","post_date_gmt":"2013-06-16 19:05:27","post_content":"","post_title":"Gathering Property Information From Sellers","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"gathering-property-information-from-sellers","to_ping":"","pinged":"","post_modified":"2016-11-30 14:42:11","post_modified_gmt":"2016-11-30 14:42:11","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_course&#038;p=267210","menu_order":"0","post_type":"fblms_course","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/courses\/gathering-property-information-from-sellers\/","fblms_course_description_id":"In this course you will learn how to gather property information from sellers and how to improve your communication with them. This is also the first step in the due diligence and deal analysis process. You will utilize this information that you gather to perform and after repair value appraisal on the property.\r\n","fblms_course_goals":"a:5:{i:0;a:1:{s:20:\"fblms_course_goal_id\";s:117:\"Identify key data and important information you must gather from a seller to perform an after repair value appraisal.\";}i:1;a:1:{s:20:\"fblms_course_goal_id\";s:56:\"Be able to accurately fill out the \u201cSeller Lead Sheet.\";}i:2;a:1:{s:20:\"fblms_course_goal_id\";s:74:\"Be able to recognize key phrases to build trust and rapport with a seller.\";}i:3;a:1:{s:20:\"fblms_course_goal_id\";s:98:\"Be able to better categorize your leads and start making judgments about the quality of the lead. \";}i:4;a:1:{s:20:\"fblms_course_goal_id\";s:49:\"Identify how to utilize a lead management system.\";}}","fblms_course_length":"10200","fblms_course_number_id":"MGPI 100","_edit_last":"35556","_edit_lock":"1484595617:12384","_expiration-date-status":"saved","_searchwp_indexed":"1","_searchwp_last_index":"1480516934","_thumbnail_id":"331639","content_length_seconds":"10200","content_length_full":"2:50:00","content_length_human":"2 hours 50 minutes ","content_length_human_short":"2h 50m ","chapters":{"267252":{"ID":"267252","post_author":"7015","post_date":"2015-01-16 20:04:44","post_date_gmt":"2015-01-16 20:04:44","post_content":"","post_title":"Overview","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"overview-2","to_ping":"","pinged":"","post_modified":"2015-01-16 20:04:44","post_modified_gmt":"2015-01-16 20:04:44","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_chapter&#038;p=267252","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":{"267211":{"ID":"267211","post_author":"7015","post_date":"2015-01-16 19:37:05","post_date_gmt":"2015-01-16 19:37:05","post_content":"","post_title":"Overview and Basic Learning Objectives","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"overview-and-basic-learning-objectives","to_ping":"","pinged":"","post_modified":"2015-08-04 22:00:22","post_modified_gmt":"2015-08-04 22:00:22","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267211","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/overview-and-basic-learning-objectives\/","_edit_lock":"1465234583:124","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"298","fblms_lesson_content":"gathering-property-information-from-sellers\/Overview-Overview_basic_learning_objectives.mp4","content_length_seconds":"298","content_length_full":"0:04:58","content_length_human":"4 minutes 58 seconds","content_length_human_short":"4m 58s","lesson_type":"video","resources":[],"lpt":{}},"267212":{"ID":"267212","post_author":"7015","post_date":"2015-01-16 19:37:42","post_date_gmt":"2015-01-16 19:37:42","post_content":"","post_title":"Course Goals for Experienced Investors","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"course-goals-for-experienced-investors","to_ping":"","pinged":"","post_modified":"2015-08-04 22:00:49","post_modified_gmt":"2015-08-04 22:00:49","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267212","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/course-goals-for-experienced-investors\/","_edit_lock":"1438725509:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"166","fblms_lesson_content":"gathering-property-information-from-sellers\/Overview-Course_goals_for_experienced_investors.mp4","content_length_seconds":"166","content_length_full":"0:02:46","content_length_human":"2 minutes 46 seconds","content_length_human_short":"2m 46s","lesson_type":"video","resources":[],"lpt":{}},"267213":{"ID":"267213","post_author":"7015","post_date":"2015-01-16 19:38:31","post_date_gmt":"2015-01-16 19:38:31","post_content":"","post_title":"What to Expect from Seller Leads","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"what-to-expect-from-seller-leads","to_ping":"","pinged":"","post_modified":"2015-08-04 22:01:08","post_modified_gmt":"2015-08-04 22:01:08","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267213","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/what-to-expect-from-seller-leads\/","_edit_lock":"1477428234:1","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"389","fblms_lesson_content":"gathering-property-information-from-sellers\/Overview-What_to_expect_from_seller_leads.mp4","content_length_seconds":"389","content_length_full":"0:06:29","content_length_human":"6 minutes 29 seconds","content_length_human_short":"6m 29s","lesson_type":"video","resources":[],"lpt":{}}},"order_number":0},"267251":{"ID":"267251","post_author":"7015","post_date":"2015-01-16 20:04:20","post_date_gmt":"2015-01-16 20:04:20","post_content":"","post_title":"Communicating with Sellers","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"communicating-with-sellers","to_ping":"","pinged":"","post_modified":"2015-01-16 20:04:20","post_modified_gmt":"2015-01-16 20:04:20","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_chapter&#038;p=267251","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":{"267214":{"ID":"267214","post_author":"7015","post_date":"2015-01-16 19:39:08","post_date_gmt":"2015-01-16 19:39:08","post_content":"","post_title":"Keys to Building Trust and Rapport","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"keys-to-building-trust-and-rapport","to_ping":"","pinged":"","post_modified":"2015-08-04 22:01:24","post_modified_gmt":"2015-08-04 22:01:24","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267214","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/keys-to-building-trust-and-rapport\/","_edit_lock":"1484267769:12384","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"669","fblms_lesson_content":"gathering-property-information-from-sellers\/Communicating_With_Sellers-Keys_to_building_trust_and_rapport.mp4","content_length_seconds":"669","content_length_full":"0:11:09","content_length_human":"11 minutes 9 seconds","content_length_human_short":"11m 9s","lesson_type":"video","resources":[],"lpt":{}},"267215":{"ID":"267215","post_author":"7015","post_date":"2015-01-16 19:39:50","post_date_gmt":"2015-01-16 19:39:50","post_content":"","post_title":"How to Describe Your Company to a Seller","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"how-to-describe-your-company-to-a-seller","to_ping":"","pinged":"","post_modified":"2015-08-04 22:01:42","post_modified_gmt":"2015-08-04 22:01:42","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267215","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/how-to-describe-your-company-to-a-seller\/","_edit_lock":"1438725562:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"419","fblms_lesson_content":"gathering-property-information-from-sellers\/Communicating_with_Sellers-How_to_describe_your_company_to_a_seller.mp4","content_length_seconds":"419","content_length_full":"0:06:59","content_length_human":"6 minutes 59 seconds","content_length_human_short":"6m 59s","lesson_type":"video","resources":[],"lpt":{}}},"order_number":1},"267249":{"ID":"267249","post_author":"7015","post_date":"2015-01-16 20:03:48","post_date_gmt":"2015-01-16 20:03:48","post_content":"","post_title":"Seller Lead Sheet","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"seller-lead-sheet","to_ping":"","pinged":"","post_modified":"2015-01-16 20:03:48","post_modified_gmt":"2015-01-16 20:03:48","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_chapter&#038;p=267249","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":{"267216":{"ID":"267216","post_author":"7015","post_date":"2015-01-16 19:40:25","post_date_gmt":"2015-01-16 19:40:25","post_content":"","post_title":"Overview of the Seller Lead Sheet","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"overview-of-the-seller-lead-sheet","to_ping":"","pinged":"","post_modified":"2015-08-04 22:01:57","post_modified_gmt":"2015-08-04 22:01:57","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267216","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/overview-of-the-seller-lead-sheet\/","_edit_lock":"1438725578:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"226","fblms_lesson_content":"gathering-property-information-from-sellers\/01_Seller-Lead-Sheet_Overview_of_the_Seller-Lead_Sheet.mp4","content_length_seconds":"226","content_length_full":"0:03:46","content_length_human":"3 minutes 46 seconds","content_length_human_short":"3m 46s","lesson_type":"video","resources":[],"lpt":{}},"267217":{"ID":"267217","post_author":"7015","post_date":"2015-01-16 19:41:10","post_date_gmt":"2015-01-16 19:41:10","post_content":"","post_title":"Property and Contact Information Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"property-and-contact-information-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:02:45","post_modified_gmt":"2015-08-04 22:02:45","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267217","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/property-and-contact-information-section\/","_edit_lock":"1438725625:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"426","fblms_lesson_content":"gathering-property-information-from-sellers\/02-Seller_Lead_Sheet_Property_and_Contact_Section.mp4","content_length_seconds":"426","content_length_full":"0:07:06","content_length_human":"7 minutes 6 seconds","content_length_human_short":"7m 6s","lesson_type":"video","resources":[],"lpt":{}},"267218":{"ID":"267218","post_author":"7015","post_date":"2015-01-16 19:44:12","post_date_gmt":"2015-01-16 19:44:12","post_content":"","post_title":"Repairs and Listing Information Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"repairs-and-listing-information-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:00","post_modified_gmt":"2015-08-04 22:03:00","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267218","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/repairs-and-listing-information-section\/","_edit_lock":"1438725641:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"933","fblms_lesson_content":"gathering-property-information-from-sellers\/03-Seller_Lead_Sheet_Repairs_and_Listing_Info.mp4","content_length_seconds":"933","content_length_full":"0:15:33","content_length_human":"15 minutes 33 seconds","content_length_human_short":"15m 33s","lesson_type":"video","resources":[],"lpt":{}},"267220":{"ID":"267220","post_author":"7015","post_date":"2015-01-16 19:44:53","post_date_gmt":"2015-01-16 19:44:53","post_content":"","post_title":"Motivation Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"motivation-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:18","post_modified_gmt":"2015-08-04 22:03:18","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267220","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/motivation-section\/","_edit_lock":"1438725705:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"528","fblms_lesson_content":"gathering-property-information-from-sellers\/04_Seller-Lead-Sheet_Motivation-Section.mp4","content_length_seconds":"528","content_length_full":"0:08:48","content_length_human":"8 minutes 48 seconds","content_length_human_short":"8m 48s","lesson_type":"video","resources":[],"lpt":{}},"267221":{"ID":"267221","post_author":"7015","post_date":"2015-01-16 19:50:20","post_date_gmt":"2015-01-16 19:50:20","post_content":"","post_title":"Mortgage Information Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"mortgage-information-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:24","post_modified_gmt":"2015-08-04 22:03:24","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267221","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/mortgage-information-section\/","_edit_lock":"1438725705:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"658","fblms_lesson_content":"gathering-property-information-from-sellers\/05_Seller-Lead-Sheet_Mortgage_Info.mp4","content_length_seconds":"658","content_length_full":"0:10:58","content_length_human":"10 minutes 58 seconds","content_length_human_short":"10m 58s","lesson_type":"video","resources":[],"lpt":{}},"267225":{"ID":"267225","post_author":"7015","post_date":"2015-01-16 19:51:05","post_date_gmt":"2015-01-16 19:51:05","post_content":"","post_title":"Price Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"price-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:28","post_modified_gmt":"2015-08-04 22:03:28","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267225","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/price-section\/","_edit_lock":"1438725708:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"507","fblms_lesson_content":"gathering-property-information-from-sellers\/06_Seller-Lead-Sheet_Price-Section.mp4","content_length_seconds":"507","content_length_full":"0:08:27","content_length_human":"8 minutes 27 seconds","content_length_human_short":"8m 27s","lesson_type":"video","resources":[],"lpt":{}},"267227":{"ID":"267227","post_author":"7015","post_date":"2015-01-16 19:51:43","post_date_gmt":"2015-01-16 19:51:43","post_content":"","post_title":"Subject-To Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"subject-to-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:32","post_modified_gmt":"2015-08-04 22:03:32","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267227","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/subject-to-section\/","_edit_lock":"1438725710:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"130","fblms_lesson_content":"gathering-property-information-from-sellers\/07_Seller-Lead-Sheet_Subject-to_Section.mp4","content_length_seconds":"130","content_length_full":"0:02:10","content_length_human":"2 minutes 10 seconds","content_length_human_short":"2m 10s","lesson_type":"video","resources":[],"lpt":{}},"267230":{"ID":"267230","post_author":"7015","post_date":"2015-01-16 19:52:32","post_date_gmt":"2015-01-16 19:52:32","post_content":"","post_title":"Other People Involved and Decision Making Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"other-people-involved-and-decision-making-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:36","post_modified_gmt":"2015-08-04 22:03:36","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267230","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/other-people-involved-and-decision-making-section\/","_edit_lock":"1438725712:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"263","fblms_lesson_content":"gathering-property-information-from-sellers\/08_Seller-Lead-Sheet_Other-People-Invovled-in-Decision-Making.mp4","content_length_seconds":"263","content_length_full":"0:04:23","content_length_human":"4 minutes 23 seconds","content_length_human_short":"4m 23s","lesson_type":"video","resources":[],"lpt":{}},"267231":{"ID":"267231","post_author":"7015","post_date":"2015-01-16 19:53:16","post_date_gmt":"2015-01-16 19:53:16","post_content":"","post_title":"Internal Office Information Only Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"internal-office-information-only-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:39","post_modified_gmt":"2015-08-04 22:03:39","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267231","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/internal-office-information-only-section\/","_edit_lock":"1438725715:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"73","fblms_lesson_content":"gathering-property-information-from-sellers\/09_Seller-Lead-Sheet_Internal_Office_Info_Only_Section.mp4","content_length_seconds":"73","content_length_full":"0:01:13","content_length_human":"1 minute 13 seconds","content_length_human_short":"1m 13s","lesson_type":"video","resources":[],"lpt":{}},"267232":{"ID":"267232","post_author":"7015","post_date":"2015-01-16 19:53:57","post_date_gmt":"2015-01-16 19:53:57","post_content":"","post_title":"Setting Appointments","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"setting-appointments","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:42","post_modified_gmt":"2015-08-04 22:03:42","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267232","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/setting-appointments\/","_edit_lock":"1438725717:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"327","fblms_lesson_content":"gathering-property-information-from-sellers\/10_Seller-Lead-Sheet_Setting-Appointments.mp4","content_length_seconds":"327","content_length_full":"0:05:27","content_length_human":"5 minutes 27 seconds","content_length_human_short":"5m 27s","lesson_type":"video","resources":[],"lpt":{}}},"order_number":2},"267248":{"ID":"267248","post_author":"7015","post_date":"2015-01-16 20:03:05","post_date_gmt":"2015-01-16 20:03:05","post_content":"","post_title":"Options for Taking Leads","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"options-for-taking-leads","to_ping":"","pinged":"","post_modified":"2015-01-16 20:03:05","post_modified_gmt":"2015-01-16 20:03:05","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_chapter&#038;p=267248","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":{"267233":{"ID":"267233","post_author":"7015","post_date":"2015-01-16 19:54:32","post_date_gmt":"2015-01-16 19:54:32","post_content":"","post_title":"Options for Taking Leads","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"options-for-taking-leads","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:46","post_modified_gmt":"2015-08-04 22:03:46","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267233","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/options-for-taking-leads\/","_edit_lock":"1438725719:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"238","fblms_lesson_content":"gathering-property-information-from-sellers\/Options_for_Taking_Leads-Options_for_taking_leads.mp4","content_length_seconds":"238","content_length_full":"0:03:58","content_length_human":"3 minutes 58 seconds","content_length_human_short":"3m 58s","lesson_type":"video","resources":[],"lpt":{}},"267236":{"ID":"267236","post_author":"7015","post_date":"2015-01-16 19:55:10","post_date_gmt":"2015-01-16 19:55:10","post_content":"","post_title":"Following Up with Your Leads","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"following-up-with-your-leads","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:50","post_modified_gmt":"2015-08-04 22:03:50","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267236","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/following-up-with-your-leads\/","_edit_lock":"1438725721:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"398","fblms_lesson_content":"gathering-property-information-from-sellers\/Options_for_Taking_Leads-Following_up_with_your_leads.mp4","content_length_seconds":"398","content_length_full":"0:06:38","content_length_human":"6 minutes 38 seconds","content_length_human_short":"6m 38s","lesson_type":"video","resources":[],"lpt":{}}},"order_number":3},"267247":{"ID":"267247","post_author":"7015","post_date":"2015-01-16 20:02:26","post_date_gmt":"2015-01-16 20:02:26","post_content":"","post_title":"Realeflow and Managing Seller Leads","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"realeflow-and-managing-seller-leads","to_ping":"","pinged":"","post_modified":"2015-01-16 20:02:26","post_modified_gmt":"2015-01-16 20:02:26","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_chapter&#038;p=267247","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":{"267237":{"ID":"267237","post_author":"7015","post_date":"2015-01-16 19:56:40","post_date_gmt":"2015-01-16 19:56:40","post_content":"","post_title":"Creating a Property and Seller Contact Record","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"creating-a-property-and-seller-contact-record","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:53","post_modified_gmt":"2015-08-04 22:03:53","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267237","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/creating-a-property-and-seller-contact-record\/","_edit_lock":"1438725723:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"654","fblms_lesson_content":"gathering-property-information-from-sellers\/01_Realeflow_and_Managing_Seller_Leads_Creating_a_Property_and_Contact_Record.mp4","content_length_seconds":"654","content_length_full":"0:10:54","content_length_human":"10 minutes 54 seconds","content_length_human_short":"10m 54s","lesson_type":"video","resources":[],"lpt":{}},"267238":{"ID":"267238","post_author":"7015","post_date":"2015-01-16 19:57:13","post_date_gmt":"2015-01-16 19:57:13","post_content":"","post_title":"Uploading the Seller Lead Sheet","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"uploading-the-seller-lead-sheet","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:57","post_modified_gmt":"2015-08-04 22:03:57","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267238","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/uploading-the-seller-lead-sheet\/","_edit_lock":"1453147754:12384","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"171","fblms_lesson_content":"gathering-property-information-from-sellers\/02_Realeflow_and_Managing_Seller_Leads-Uploading_the_Seller_Lead_Sheet.mp4","content_length_seconds":"171","content_length_full":"0:02:51","content_length_human":"2 minutes 51 seconds","content_length_human_short":"2m 51s","lesson_type":"video","resources":[],"lpt":{}}},"order_number":4},"267245":{"ID":"267245","post_author":"7015","post_date":"2015-01-16 20:02:01","post_date_gmt":"2015-01-16 20:02:01","post_content":"","post_title":"Next Steps","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"next-steps","to_ping":"","pinged":"","post_modified":"2015-01-16 20:02:01","post_modified_gmt":"2015-01-16 20:02:01","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_chapter&#038;p=267245","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":{"267239":{"ID":"267239","post_author":"7015","post_date":"2015-01-16 19:57:44","post_date_gmt":"2015-01-16 19:57:44","post_content":"","post_title":"How to Practice","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"how-to-practice","to_ping":"","pinged":"","post_modified":"2015-08-04 22:04:01","post_modified_gmt":"2015-08-04 22:04:01","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267239","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/how-to-practice\/","_edit_lock":"1438725729:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"138","fblms_lesson_content":"gathering-property-information-from-sellers\/Next_Steps-How_to_practice.mp4","content_length_seconds":"138","content_length_full":"0:02:18","content_length_human":"2 minutes 18 seconds","content_length_human_short":"2m 18s","lesson_type":"video","resources":[],"lpt":{}},"267240":{"ID":"267240","post_author":"7015","post_date":"2015-01-16 19:58:23","post_date_gmt":"2015-01-16 19:58:23","post_content":"","post_title":"The Next Steps","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"the-next-steps","to_ping":"","pinged":"","post_modified":"2015-08-04 22:07:10","post_modified_gmt":"2015-08-04 22:07:10","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267240","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/the-next-steps\/","_edit_lock":"1438726032:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"54","fblms_lesson_content":"gathering-property-information-from-sellers\/Next_steps-Whats_the_next_stage.mp4","content_length_seconds":"54","content_length_full":"0:00:54","content_length_human":"54 seconds","content_length_human_short":"54s","lesson_type":"video","resources":[],"lpt":{}}},"order_number":5},"267244":{"ID":"267244","post_author":"7015","post_date":"2015-01-16 20:01:32","post_date_gmt":"2015-01-16 20:01:32","post_content":"","post_title":"Scenario Examples and Activity","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"scenario-examples-and-activity","to_ping":"","pinged":"","post_modified":"2015-07-15 20:19:18","post_modified_gmt":"2015-07-15 20:19:18","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_chapter&#038;p=267244","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":{"267243":{"ID":"267243","post_author":"7015","post_date":"2015-01-16 19:59:48","post_date_gmt":"2015-01-16 19:59:48","post_content":"","post_title":"Seller Lead Sheet \u2013 Call Examples","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"seller-lead-sheet-call-examples","to_ping":"","pinged":"","post_modified":"2015-07-15 20:19:15","post_modified_gmt":"2015-07-15 20:19:15","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267243","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/seller-lead-sheet-call-examples\/","_edit_lock":"1484289961:12384","_edit_last":"7015","_expiration-date-status":"saved","fblms_lesson_type":"articulate","fblms_lesson_content_length":"2700","fblms_lesson_content":"http:\/\/s3.amazonaws.com\/Fortunebuilders\/designteam\/Articulate\/TakingLeads\/SellerLeadSheetActivity\/story.html","_searchwp_indexed":"1","content_length_seconds":"2700","content_length_full":"0:45:00","content_length_human":"45 minutes ","content_length_human_short":"45m ","lesson_type":"articulate","resources":[],"lpt":{}}},"order_number":6},"278080":{"ID":"278080","post_author":"7015","post_date":"2015-02-05 21:28:47","post_date_gmt":"2015-02-05 21:28:47","post_content":"","post_title":"Course Quiz","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"course-quiz","to_ping":"","pinged":"","post_modified":"2015-04-14 20:30:31","post_modified_gmt":"2015-04-14 20:30:31","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_chapter&#038;p=278080","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":[],"order_number":7},"273861":{"ID":"273861","post_author":"1","post_date":"2015-01-28 19:33:59","post_date_gmt":"2015-01-28 19:33:59","post_content":"","post_title":"Course Survey","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"new-chapter-272","to_ping":"","pinged":"","post_modified":"2015-02-02 07:21:46","post_modified_gmt":"2015-02-02 07:21:46","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/chapter\/new-chapter-272\/","menu_order":"0","post_type":"fblms_chapter","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"lessons":{"273862":{"ID":"273862","post_author":"1","post_date":"2015-01-28 19:34:05","post_date_gmt":"2015-01-28 19:34:05","post_content":"","post_title":"Fill Out Course Survey","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"new-lesson-1850","to_ping":"","pinged":"","post_modified":"2015-02-02 07:21:42","post_modified_gmt":"2015-02-02 07:21:42","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/lesson\/new-lesson-1850\/","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/new-lesson-1850\/","fblms_lesson_type":"web","fblms_lesson_content":"https:\/\/www.surveymonkey.com\/s\/GPIFS","fblms_lesson_content_length":"60","_searchwp_indexed":"1","_edit_lock":"1454513936:35556","content_length_seconds":"60","content_length_full":"0:01:00","content_length_human":"1 minute ","content_length_human_short":"1m ","lesson_type":"web","resources":[],"lpt":{}}},"order_number":8}},"lessons":{"267211":{"ID":"267211","post_author":"7015","post_date":"2015-01-16 19:37:05","post_date_gmt":"2015-01-16 19:37:05","post_content":"","post_title":"Overview and Basic Learning Objectives","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"overview-and-basic-learning-objectives","to_ping":"","pinged":"","post_modified":"2015-08-04 22:00:22","post_modified_gmt":"2015-08-04 22:00:22","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267211","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/overview-and-basic-learning-objectives\/","_edit_lock":"1465234583:124","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"298","fblms_lesson_content":"gathering-property-information-from-sellers\/Overview-Overview_basic_learning_objectives.mp4","content_length_seconds":"298","content_length_full":"0:04:58","content_length_human":"4 minutes 58 seconds","content_length_human_short":"4m 58s","lesson_type":"video","resources":[],"lpt":{}},"267212":{"ID":"267212","post_author":"7015","post_date":"2015-01-16 19:37:42","post_date_gmt":"2015-01-16 19:37:42","post_content":"","post_title":"Course Goals for Experienced Investors","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"course-goals-for-experienced-investors","to_ping":"","pinged":"","post_modified":"2015-08-04 22:00:49","post_modified_gmt":"2015-08-04 22:00:49","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267212","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/course-goals-for-experienced-investors\/","_edit_lock":"1438725509:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"166","fblms_lesson_content":"gathering-property-information-from-sellers\/Overview-Course_goals_for_experienced_investors.mp4","content_length_seconds":"166","content_length_full":"0:02:46","content_length_human":"2 minutes 46 seconds","content_length_human_short":"2m 46s","lesson_type":"video","resources":[],"lpt":{}},"267213":{"ID":"267213","post_author":"7015","post_date":"2015-01-16 19:38:31","post_date_gmt":"2015-01-16 19:38:31","post_content":"","post_title":"What to Expect from Seller Leads","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"what-to-expect-from-seller-leads","to_ping":"","pinged":"","post_modified":"2015-08-04 22:01:08","post_modified_gmt":"2015-08-04 22:01:08","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267213","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/what-to-expect-from-seller-leads\/","_edit_lock":"1477428234:1","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"389","fblms_lesson_content":"gathering-property-information-from-sellers\/Overview-What_to_expect_from_seller_leads.mp4","content_length_seconds":"389","content_length_full":"0:06:29","content_length_human":"6 minutes 29 seconds","content_length_human_short":"6m 29s","lesson_type":"video","resources":[],"lpt":{}},"267214":{"ID":"267214","post_author":"7015","post_date":"2015-01-16 19:39:08","post_date_gmt":"2015-01-16 19:39:08","post_content":"","post_title":"Keys to Building Trust and Rapport","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"keys-to-building-trust-and-rapport","to_ping":"","pinged":"","post_modified":"2015-08-04 22:01:24","post_modified_gmt":"2015-08-04 22:01:24","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267214","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/keys-to-building-trust-and-rapport\/","_edit_lock":"1484267769:12384","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"669","fblms_lesson_content":"gathering-property-information-from-sellers\/Communicating_With_Sellers-Keys_to_building_trust_and_rapport.mp4","content_length_seconds":"669","content_length_full":"0:11:09","content_length_human":"11 minutes 9 seconds","content_length_human_short":"11m 9s","lesson_type":"video","resources":[],"lpt":{}},"267215":{"ID":"267215","post_author":"7015","post_date":"2015-01-16 19:39:50","post_date_gmt":"2015-01-16 19:39:50","post_content":"","post_title":"How to Describe Your Company to a Seller","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"how-to-describe-your-company-to-a-seller","to_ping":"","pinged":"","post_modified":"2015-08-04 22:01:42","post_modified_gmt":"2015-08-04 22:01:42","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267215","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/how-to-describe-your-company-to-a-seller\/","_edit_lock":"1438725562:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"419","fblms_lesson_content":"gathering-property-information-from-sellers\/Communicating_with_Sellers-How_to_describe_your_company_to_a_seller.mp4","content_length_seconds":"419","content_length_full":"0:06:59","content_length_human":"6 minutes 59 seconds","content_length_human_short":"6m 59s","lesson_type":"video","resources":[],"lpt":{}},"267216":{"ID":"267216","post_author":"7015","post_date":"2015-01-16 19:40:25","post_date_gmt":"2015-01-16 19:40:25","post_content":"","post_title":"Overview of the Seller Lead Sheet","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"overview-of-the-seller-lead-sheet","to_ping":"","pinged":"","post_modified":"2015-08-04 22:01:57","post_modified_gmt":"2015-08-04 22:01:57","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267216","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/overview-of-the-seller-lead-sheet\/","_edit_lock":"1438725578:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"226","fblms_lesson_content":"gathering-property-information-from-sellers\/01_Seller-Lead-Sheet_Overview_of_the_Seller-Lead_Sheet.mp4","content_length_seconds":"226","content_length_full":"0:03:46","content_length_human":"3 minutes 46 seconds","content_length_human_short":"3m 46s","lesson_type":"video","resources":[],"lpt":{}},"267217":{"ID":"267217","post_author":"7015","post_date":"2015-01-16 19:41:10","post_date_gmt":"2015-01-16 19:41:10","post_content":"","post_title":"Property and Contact Information Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"property-and-contact-information-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:02:45","post_modified_gmt":"2015-08-04 22:02:45","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267217","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/property-and-contact-information-section\/","_edit_lock":"1438725625:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"426","fblms_lesson_content":"gathering-property-information-from-sellers\/02-Seller_Lead_Sheet_Property_and_Contact_Section.mp4","content_length_seconds":"426","content_length_full":"0:07:06","content_length_human":"7 minutes 6 seconds","content_length_human_short":"7m 6s","lesson_type":"video","resources":[],"lpt":{}},"267218":{"ID":"267218","post_author":"7015","post_date":"2015-01-16 19:44:12","post_date_gmt":"2015-01-16 19:44:12","post_content":"","post_title":"Repairs and Listing Information Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"repairs-and-listing-information-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:00","post_modified_gmt":"2015-08-04 22:03:00","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267218","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/repairs-and-listing-information-section\/","_edit_lock":"1438725641:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"933","fblms_lesson_content":"gathering-property-information-from-sellers\/03-Seller_Lead_Sheet_Repairs_and_Listing_Info.mp4","content_length_seconds":"933","content_length_full":"0:15:33","content_length_human":"15 minutes 33 seconds","content_length_human_short":"15m 33s","lesson_type":"video","resources":[],"lpt":{}},"267220":{"ID":"267220","post_author":"7015","post_date":"2015-01-16 19:44:53","post_date_gmt":"2015-01-16 19:44:53","post_content":"","post_title":"Motivation Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"motivation-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:18","post_modified_gmt":"2015-08-04 22:03:18","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267220","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/motivation-section\/","_edit_lock":"1438725705:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"528","fblms_lesson_content":"gathering-property-information-from-sellers\/04_Seller-Lead-Sheet_Motivation-Section.mp4","content_length_seconds":"528","content_length_full":"0:08:48","content_length_human":"8 minutes 48 seconds","content_length_human_short":"8m 48s","lesson_type":"video","resources":[],"lpt":{}},"267221":{"ID":"267221","post_author":"7015","post_date":"2015-01-16 19:50:20","post_date_gmt":"2015-01-16 19:50:20","post_content":"","post_title":"Mortgage Information Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"mortgage-information-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:24","post_modified_gmt":"2015-08-04 22:03:24","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267221","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/mortgage-information-section\/","_edit_lock":"1438725705:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"658","fblms_lesson_content":"gathering-property-information-from-sellers\/05_Seller-Lead-Sheet_Mortgage_Info.mp4","content_length_seconds":"658","content_length_full":"0:10:58","content_length_human":"10 minutes 58 seconds","content_length_human_short":"10m 58s","lesson_type":"video","resources":[],"lpt":{}},"267225":{"ID":"267225","post_author":"7015","post_date":"2015-01-16 19:51:05","post_date_gmt":"2015-01-16 19:51:05","post_content":"","post_title":"Price Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"price-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:28","post_modified_gmt":"2015-08-04 22:03:28","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267225","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/price-section\/","_edit_lock":"1438725708:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"507","fblms_lesson_content":"gathering-property-information-from-sellers\/06_Seller-Lead-Sheet_Price-Section.mp4","content_length_seconds":"507","content_length_full":"0:08:27","content_length_human":"8 minutes 27 seconds","content_length_human_short":"8m 27s","lesson_type":"video","resources":[],"lpt":{}},"267227":{"ID":"267227","post_author":"7015","post_date":"2015-01-16 19:51:43","post_date_gmt":"2015-01-16 19:51:43","post_content":"","post_title":"Subject-To Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"subject-to-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:32","post_modified_gmt":"2015-08-04 22:03:32","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267227","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/subject-to-section\/","_edit_lock":"1438725710:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"130","fblms_lesson_content":"gathering-property-information-from-sellers\/07_Seller-Lead-Sheet_Subject-to_Section.mp4","content_length_seconds":"130","content_length_full":"0:02:10","content_length_human":"2 minutes 10 seconds","content_length_human_short":"2m 10s","lesson_type":"video","resources":[],"lpt":{}},"267230":{"ID":"267230","post_author":"7015","post_date":"2015-01-16 19:52:32","post_date_gmt":"2015-01-16 19:52:32","post_content":"","post_title":"Other People Involved and Decision Making Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"other-people-involved-and-decision-making-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:36","post_modified_gmt":"2015-08-04 22:03:36","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267230","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/other-people-involved-and-decision-making-section\/","_edit_lock":"1438725712:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"263","fblms_lesson_content":"gathering-property-information-from-sellers\/08_Seller-Lead-Sheet_Other-People-Invovled-in-Decision-Making.mp4","content_length_seconds":"263","content_length_full":"0:04:23","content_length_human":"4 minutes 23 seconds","content_length_human_short":"4m 23s","lesson_type":"video","resources":[],"lpt":{}},"267231":{"ID":"267231","post_author":"7015","post_date":"2015-01-16 19:53:16","post_date_gmt":"2015-01-16 19:53:16","post_content":"","post_title":"Internal Office Information Only Section","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"internal-office-information-only-section","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:39","post_modified_gmt":"2015-08-04 22:03:39","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267231","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/internal-office-information-only-section\/","_edit_lock":"1438725715:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"73","fblms_lesson_content":"gathering-property-information-from-sellers\/09_Seller-Lead-Sheet_Internal_Office_Info_Only_Section.mp4","content_length_seconds":"73","content_length_full":"0:01:13","content_length_human":"1 minute 13 seconds","content_length_human_short":"1m 13s","lesson_type":"video","resources":[],"lpt":{}},"267232":{"ID":"267232","post_author":"7015","post_date":"2015-01-16 19:53:57","post_date_gmt":"2015-01-16 19:53:57","post_content":"","post_title":"Setting Appointments","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"setting-appointments","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:42","post_modified_gmt":"2015-08-04 22:03:42","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267232","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/setting-appointments\/","_edit_lock":"1438725717:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"327","fblms_lesson_content":"gathering-property-information-from-sellers\/10_Seller-Lead-Sheet_Setting-Appointments.mp4","content_length_seconds":"327","content_length_full":"0:05:27","content_length_human":"5 minutes 27 seconds","content_length_human_short":"5m 27s","lesson_type":"video","resources":[],"lpt":{}},"267233":{"ID":"267233","post_author":"7015","post_date":"2015-01-16 19:54:32","post_date_gmt":"2015-01-16 19:54:32","post_content":"","post_title":"Options for Taking Leads","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"options-for-taking-leads","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:46","post_modified_gmt":"2015-08-04 22:03:46","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267233","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/options-for-taking-leads\/","_edit_lock":"1438725719:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"238","fblms_lesson_content":"gathering-property-information-from-sellers\/Options_for_Taking_Leads-Options_for_taking_leads.mp4","content_length_seconds":"238","content_length_full":"0:03:58","content_length_human":"3 minutes 58 seconds","content_length_human_short":"3m 58s","lesson_type":"video","resources":[],"lpt":{}},"267236":{"ID":"267236","post_author":"7015","post_date":"2015-01-16 19:55:10","post_date_gmt":"2015-01-16 19:55:10","post_content":"","post_title":"Following Up with Your Leads","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"following-up-with-your-leads","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:50","post_modified_gmt":"2015-08-04 22:03:50","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267236","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/following-up-with-your-leads\/","_edit_lock":"1438725721:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"398","fblms_lesson_content":"gathering-property-information-from-sellers\/Options_for_Taking_Leads-Following_up_with_your_leads.mp4","content_length_seconds":"398","content_length_full":"0:06:38","content_length_human":"6 minutes 38 seconds","content_length_human_short":"6m 38s","lesson_type":"video","resources":[],"lpt":{}},"267237":{"ID":"267237","post_author":"7015","post_date":"2015-01-16 19:56:40","post_date_gmt":"2015-01-16 19:56:40","post_content":"","post_title":"Creating a Property and Seller Contact Record","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"creating-a-property-and-seller-contact-record","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:53","post_modified_gmt":"2015-08-04 22:03:53","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267237","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/creating-a-property-and-seller-contact-record\/","_edit_lock":"1438725723:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"654","fblms_lesson_content":"gathering-property-information-from-sellers\/01_Realeflow_and_Managing_Seller_Leads_Creating_a_Property_and_Contact_Record.mp4","content_length_seconds":"654","content_length_full":"0:10:54","content_length_human":"10 minutes 54 seconds","content_length_human_short":"10m 54s","lesson_type":"video","resources":[],"lpt":{}},"267238":{"ID":"267238","post_author":"7015","post_date":"2015-01-16 19:57:13","post_date_gmt":"2015-01-16 19:57:13","post_content":"","post_title":"Uploading the Seller Lead Sheet","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"uploading-the-seller-lead-sheet","to_ping":"","pinged":"","post_modified":"2015-08-04 22:03:57","post_modified_gmt":"2015-08-04 22:03:57","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267238","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/uploading-the-seller-lead-sheet\/","_edit_lock":"1453147754:12384","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"171","fblms_lesson_content":"gathering-property-information-from-sellers\/02_Realeflow_and_Managing_Seller_Leads-Uploading_the_Seller_Lead_Sheet.mp4","content_length_seconds":"171","content_length_full":"0:02:51","content_length_human":"2 minutes 51 seconds","content_length_human_short":"2m 51s","lesson_type":"video","resources":[],"lpt":{}},"267239":{"ID":"267239","post_author":"7015","post_date":"2015-01-16 19:57:44","post_date_gmt":"2015-01-16 19:57:44","post_content":"","post_title":"How to Practice","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"how-to-practice","to_ping":"","pinged":"","post_modified":"2015-08-04 22:04:01","post_modified_gmt":"2015-08-04 22:04:01","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267239","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/how-to-practice\/","_edit_lock":"1438725729:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"138","fblms_lesson_content":"gathering-property-information-from-sellers\/Next_Steps-How_to_practice.mp4","content_length_seconds":"138","content_length_full":"0:02:18","content_length_human":"2 minutes 18 seconds","content_length_human_short":"2m 18s","lesson_type":"video","resources":[],"lpt":{}},"267240":{"ID":"267240","post_author":"7015","post_date":"2015-01-16 19:58:23","post_date_gmt":"2015-01-16 19:58:23","post_content":"","post_title":"The Next Steps","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"the-next-steps","to_ping":"","pinged":"","post_modified":"2015-08-04 22:07:10","post_modified_gmt":"2015-08-04 22:07:10","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267240","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/the-next-steps\/","_edit_lock":"1438726032:7015","_edit_last":"7015","_expiration-date-status":"saved","_searchwp_indexed":"1","fblms_lesson_type":"video","fblms_lesson_content_length":"54","fblms_lesson_content":"gathering-property-information-from-sellers\/Next_steps-Whats_the_next_stage.mp4","content_length_seconds":"54","content_length_full":"0:00:54","content_length_human":"54 seconds","content_length_human_short":"54s","lesson_type":"video","resources":[],"lpt":{}},"267243":{"ID":"267243","post_author":"7015","post_date":"2015-01-16 19:59:48","post_date_gmt":"2015-01-16 19:59:48","post_content":"","post_title":"Seller Lead Sheet \u2013 Call Examples","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"seller-lead-sheet-call-examples","to_ping":"","pinged":"","post_modified":"2015-07-15 20:19:15","post_modified_gmt":"2015-07-15 20:19:15","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_lesson&#038;p=267243","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/seller-lead-sheet-call-examples\/","_edit_lock":"1484289961:12384","_edit_last":"7015","_expiration-date-status":"saved","fblms_lesson_type":"articulate","fblms_lesson_content_length":"2700","fblms_lesson_content":"http:\/\/s3.amazonaws.com\/Fortunebuilders\/designteam\/Articulate\/TakingLeads\/SellerLeadSheetActivity\/story.html","_searchwp_indexed":"1","content_length_seconds":"2700","content_length_full":"0:45:00","content_length_human":"45 minutes ","content_length_human_short":"45m ","lesson_type":"articulate","resources":[],"lpt":{}},"273862":{"ID":"273862","post_author":"1","post_date":"2015-01-28 19:34:05","post_date_gmt":"2015-01-28 19:34:05","post_content":"","post_title":"Fill Out Course Survey","post_excerpt":"","post_status":"publish","comment_status":"open","ping_status":"open","post_password":"","post_name":"new-lesson-1850","to_ping":"","pinged":"","post_modified":"2015-02-02 07:21:42","post_modified_gmt":"2015-02-02 07:21:42","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/lesson\/new-lesson-1850\/","menu_order":"0","post_type":"fblms_lesson","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/lesson\/new-lesson-1850\/","fblms_lesson_type":"web","fblms_lesson_content":"https:\/\/www.surveymonkey.com\/s\/GPIFS","fblms_lesson_content_length":"60","_searchwp_indexed":"1","_edit_lock":"1454513936:35556","content_length_seconds":"60","content_length_full":"0:01:00","content_length_human":"1 minute ","content_length_human_short":"1m ","lesson_type":"web","resources":[],"lpt":{}}},"lms_objects_ids":[267210,267211,267212,267213,267214,267215,267216,267217,267218,267220,267221,267225,267227,267230,267231,267232,267233,267236,267237,267238,267239,267240,267243,273862],"number_of_lessons":23,"resources":[{"ID":"267257","post_author":"7015","post_date":"2015-01-16 20:06:31","post_date_gmt":"2015-01-16 20:06:31","post_content":"","post_title":"Seller Lead Sheet","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"seller-lead-sheet","to_ping":"","pinged":"","post_modified":"2016-04-15 20:15:25","post_modified_gmt":"2016-04-15 20:15:25","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_resource&#038;p=267257","menu_order":"0","post_type":"fblms_resource","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/resource\/seller-lead-sheet\/","fblms_resource_url":"https:\/\/s3.amazonaws.com\/Fortunebuilders\/mastery4\/Documents\/lms\/Seller-Lead-Sheet.zip","_edit_last":"35556","_edit_lock":"1460751183:35556","_expiration-date-status":"saved"},{"ID":"267259","post_author":"7015","post_date":"2015-01-16 20:07:38","post_date_gmt":"2015-01-16 20:07:38","post_content":"","post_title":"Seller Lead Sheet Company Description","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"seller-lead-sheet-company-description","to_ping":"","pinged":"","post_modified":"2015-01-16 20:07:38","post_modified_gmt":"2015-01-16 20:07:38","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_resource&#038;p=267259","menu_order":"0","post_type":"fblms_resource","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/resource\/seller-lead-sheet-company-description\/","fblms_resource_url":"http:\/\/s3.amazonaws.com\/fbmastery\/Scripts%20-%20Seller%20Lead%20Sheet%20Company%20Description.doc","_edit_last":"7015","_edit_lock":"1421438723:7015","_expiration-date-status":"saved"},{"ID":"267263","post_author":"7015","post_date":"2015-01-16 20:08:42","post_date_gmt":"2015-01-16 20:08:42","post_content":"","post_title":"Seller Lead Sheet Company Description.pdf","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"seller-lead-sheet-company-description-pdf","to_ping":"","pinged":"","post_modified":"2015-01-16 20:08:42","post_modified_gmt":"2015-01-16 20:08:42","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_resource&#038;p=267263","menu_order":"0","post_type":"fblms_resource","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/resource\/seller-lead-sheet-company-description-pdf\/","fblms_resource_url":"http:\/\/s3.amazonaws.com\/fbmastery\/Scripts%20-%20Seller%20Lead%20Sheet%20Company%20Description.pdf","_edit_last":"7015","_edit_lock":"1447796903:35556","_expiration-date-status":"saved"},{"ID":"388763","post_author":"35556","post_date":"2015-11-17 21:51:10","post_date_gmt":"2015-11-17 21:51:10","post_content":"","post_title":"Seller Lead Sheet Question Verbiage","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"seller-lead-sheet-question-verbiage","to_ping":"","pinged":"","post_modified":"2015-11-17 21:51:10","post_modified_gmt":"2015-11-17 21:51:10","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_resource&#038;p=388763","menu_order":"0","post_type":"fblms_resource","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/resource\/seller-lead-sheet-question-verbiage\/","fblms_resource_url":"https:\/\/fbmastery.s3.amazonaws.com\/Seller-Lead-Sheet-Questions-Verbiage.docx","_edit_last":"35556","_edit_lock":"1447802818:35556","_expiration-date-status":"saved"},{"ID":"395101","post_author":"7015","post_date":"2015-12-16 18:44:55","post_date_gmt":"2015-12-16 18:44:55","post_content":"","post_title":"Gathering Property Information From Sellers Course Transcript","post_excerpt":"","post_status":"publish","comment_status":"closed","ping_status":"closed","post_password":"","post_name":"gathering-property-information-from-sellers-course-transcript","to_ping":"","pinged":"","post_modified":"2015-12-16 18:44:55","post_modified_gmt":"2015-12-16 18:44:55","post_content_filtered":"","post_parent":"0","guid":"http:\/\/develop.fbmastery.com\/?post_type=fblms_resource&#038;p=395101","menu_order":"0","post_type":"fblms_resource","post_mime_type":"","comment_count":"0","group_access":null,"public_only":"0","release_cycle":null,"hide_menu":"0","more":"0","start_prot":"0","stop_prot":"0","contact_list":null,"_mgt_status":null,"_mgt_sort":null,"permalink":"http:\/\/develop.fbmastery.com\/resource\/gathering-property-information-from-sellers-course-transcript\/","fblms_resource_url":"http:\/\/s3.amazonaws.com\/Fortunebuilders\/mastery4\/transcripts\/deal-analysis\/01-Gathering-Prop-Info-Sellers-Final-Transcripts.zip","_edit_last":"7015","_edit_lock":"1484596238:12384","_expiration-date-status":"saved"}],"order_number":0,"is_core":true,"course_type":"core","user":{"ID":"47611","user_login":"mgallegos@fortunebuilders.com","user_nicename":"miguel-gallegos","user_email":"mgallegos@fortunebuilders.com","user_url":"http:\/\/mgallegostest.com","user_registered":"2016-08-16 19:04:02","user_activation_key":"","user_status":"0","display_name":"!Miguel !Gallegos","old_user_login":"","nickname":"mgallegos@fortunebuilders.com","first_name":"!Miguel","last_name":"!Gallegos","description":"","rich_editing":"true","comment_shortcuts":"false","admin_color":"fresh","use_ssl":"0","show_admin_bar_front":"true","wp_capabilities":"a:1:{s:11:\"fb_platinum\";b:1;}","wp_user_level":"0","last_activity":"2017-06-02 20:39:45","Birthday":"meta_value","phone":"999","street":"2910 Brando Dr","city":"San Diego","state":"CA","zipcode":"92154","country":"United States","Groups":"2623,4087,4999,5527,5873,6182,6322,6352,6354,6356,6360,6364,6700,7854,11505,12153,14217,15091,15849,17006,17012,17570,19131,19681,19683,19685,19835,20209,21784,21934,21952,22872,24702,24738,24744,24746,25214,25218,26540,27518,27520,29436,29494,31567,31980,32164,32168,32322,32330,33040,33538,33662,35252,35298,39093,40339,41167,41205,41225,42317,42711,42963,44242,44436,44562,45444,45446,45508,45698,45700,45914","Website":"mgallegostest.com","LastUpdated":"meta_value","City":"meta_value","State":"meta_value","Country":"meta_value","Id":"meta_value","PostalCode":"meta_value","StreetAddress1":"meta_value","Phone1":"meta_value","pubemail":"","facebook":"","twitter":"","googleplus":"","linkedin":"","youtube":"","_MasteryLastLogin":"2017-06-02 20:39:44","_call_booked_time":"1488847718","_second_call_notified":"1475686801","facebook_id":"1318524048167341","use_fb_img":"0","facebook_connected":"0","ta_assigned":"false","Email":"mgallegos@fortunebuilders.com","_MasteryPassword":"o0i9u8y7t6","session_tokens":"a:2:{s:64:\"d4f63757f1df40d1a5e1a0978cc096649a59e5f70eb68e439a01ca93fac94614\";a:4:{s:10:\"expiration\";i:1496512232;s:2:\"ip\";s:9:\"10.0.1.75\";s:2:\"ua\";s:106:\"Mozilla\/5.0 (Windows NT 6.2; WOW64) AppleWebKit\/534.57.2 (KHTML, like Gecko) Version\/5.1.7 Safari\/534.57.2\";s:5:\"login\";i:1496339432;}s:64:\"c4f2244f704af329ea5d757af1fa478ab3aaa6912bc0ad3dead11226c54ef9e9\";a:4:{s:10:\"expiration\";i:1496608784;s:2:\"ip\";s:9:\"10.0.0.55\";s:2:\"ua\";s:115:\"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/58.0.3029.110 Safari\/537.36\";s:5:\"login\";i:1496435984;}}"},"lpt":{},"join_lpt_id":0,"complete_lpt_id":0,"current_lesson_id":"267211"};</script>				</div>
			</div>

			<aside class="lesson-aside col-xs-12 col-md-8">

				
			</aside>

			<div class="sidebar-right sidebar-lesson col-xs-12 col-md-4">
							</div>
		</div>

	</div>
</article>


	<!-- #content -->

	<div class='footer-controls container'>
		<a href="#page" class='scrollto back-to-top'><span class='glyphicon glyphicon-circle_arrow_top'></span> Top</a>
			</div>

	<div class='footer-controls-admin container'>
			</div>

</div><!-- #page -->

<footer id="colophon" class="site-footer footer-margin" role="contentinfo">

	<div class='footer-nav bg_siteFooter clearfix'>
		<div class='footer-nav-wrap'>
			<nav class="tabs-left container">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li class='active'><a href="#footer-nav-information" data-toggle="tab">Information</a></li>
					<li><a href="#footer-nav-connect" data-toggle="tab">Connect</a></li>
					<li><a href="#footer-nav-my-account" data-toggle="tab">My Account</a></li>
									</ul>

				<!-- Tab panes -->
				<div class="tab-content ">
					<div class='footer-content container'>
						<div class='footer-copy'>
														<a href='http://develop.fbmastery.com/' title='FortuneBuilders' rel='home'>
								
					<div class='site-logo'>
						<img src='http://develop.fbmastery.com/wp-content/uploads/2015/05/fb-mastery-logo.png' alt='FortuneBuilders'>
					</div>							</a>
							<p>Real Estate Education created by FortuneBuilders INC in San Diego, CA.</p> <p class="copyright">Copyright © 2017 All rights reserved.</p>
						</div>
					</div>
					<div class='active' id="footer-nav-information"><aside id="nav_menu-3" class="widget widget_nav_menu"><h4 class="widget-title">Information</h4><div class="menu-information-container"><ul id="menu-information" class="menu"><li id="menu-item-399390" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-399390"><a href="http://develop.fbmastery.com/legal-information/">Legal Information</a></li>
<li id="menu-item-399391" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-399391"><a href="http://develop.fbmastery.com/documents-disclaimer/">Documents Disclaimer</a></li>
<li id="menu-item-399392" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-399392"><a target="_blank" href="https://www.fortunebuilders.com/privacy-policy/">Privacy Policy</a></li>
<li id="menu-item-399393" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-399393"><a target="_blank" href="https://www.fortunebuilders.com/terms-of-use/">Terms of Use</a></li>
<li id="menu-item-399394" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-399394"><a target="_blank" href="https://www.fortunebuilders.com/earnings-income-disclaimers/">Earnings &#038; Income Disclosures</a></li>
</ul></div></aside></div>
					<div id="footer-nav-connect">
						<aside id="nav_menu-4" class="widget widget_nav_menu">
							<h4 class="widget-title">Connect</h4>
							<div class="menu-connect-container">
								<ul id="menu-connect" class="menu">
									<div class="footer-social">
										<a href="http://www.facebook.com/flipthishouse/" target="_blank">
											<span class="social social-facebook"></span>
										</a>
										<a href="http://www.followfortunebuilders.com/" target="_blank">
											<span class="social social-twitter"></span>
										</a>
										<a href="http://www.youtube.com/fortunebuilders" target="_blank">
											<span class="social social-youtube"></span>
										</a>
										<a href="http://www.linkedin.com/company/fortunebuilders" target="_blank">
											<span class="social social-linked_in"></span>
										</a>
										<a href="https://plus.google.com/116306573100995520611/posts" style="text-decoration:none;" target="_blank">
											<span class="social social-google_plus"></span>
										</a>
									</div>
									<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="mailto:support@fortunebuildersmastery.com">Email Support</a></li>
								</ul>
							</div>
						</aside>
					</div>
<!--					<div id="footer-nav-community">--><!--</div>-->
<!--					<div id="footer-nav-resources">--><!--</div>-->
<!--					<div id="footer-nav-support">--><!--</div>-->
					<div id="footer-nav-my-account">
						<aside id="nav_menu-9" class="widget widget_nav_menu">
							<h4 class="widget-title">My Account</h4>
							<div class="menu-my-account-container">
								<ul id="menu-my-account" class="menu">
									<li class="menu-item menu-item-type-custom menu-item-object-custom">
										<a title="Settings" href="http://develop.fbmastery.com/members/miguel-gallegos/settings/">Settings</a>
									</li>
									<li class="menu-item menu-item-type-custom menu-item-object-custom">
										<a title="Member Profile" href="http://develop.fbmastery.com/members/miguel-gallegos">Member Profile</a>
									</li>
									<li class="menu-item menu-item-type-post_type menu-item-object-page">
										<a title="Log Out" href="http://develop.fbmastery.com/logout?_wpnonce=862004d01b">Log Out</a>
									</li>
								</ul>
							</div>
						</aside>
					</div>

					
				</div>
			</nav>
<!--			FOOTER-->
		</div>
	</div>

</footer><!-- #colophon -->


<!-- Generated in 0.652 seconds. (123 q) -->

	<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/sortable.min.js?ver=1.11.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var lptajax = {"ajaxurl":"http:\/\/develop.fbmastery.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/core-plugins/LearningProgressTracker/lpt.js?ver=2.3.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var noteajax = {"ajaxurl":"http:\/\/develop.fbmastery.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/UserNotes/js/note.js?ver=1.1.0'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/resizable.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/draggable.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/button.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/dialog.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/js/jquery-ui-timepicker-addon.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/js/daterangepicker.js?ver=2.1.13'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var FBCoachingCalls = {"ajaxurl":"http:\/\/develop.fbmastery.com\/wp-admin\/admin-ajax.php","pluploadflash":"http:\/\/develop.fbmastery.com\/wp-includes\/js\/plupload\/plupload.flash.swf","pluploadsilver":"http:\/\/develop.fbmastery.com\/wp-includes\/js\/plupload\/plupload.silverlight.xap"};
/* ]]> */
</script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/js/fb-coaching-calls.js?ver=2.9.19'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/redactor/redactor.js?ver=2.1'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/redactor/fullscreen.js?ver=2.0'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-coaching-calls/js/jquery.base64.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-expiration-dates/js/expiration_dates.js?ver=2.3.8'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-includes/js/comment-reply.min.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/plugins/fb-plugins/custom-plugins/fb-articulate-progress/articulate-xd-message.js?ver=1.0.0'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/hopscotch-0.1.2.js?ver=4.1.2'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/bootstrap.min.js?ver=3.0'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/mastery.js?ver=4.1.6'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/chart.js?ver=2.1.0'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/inc/tinymce/tinymce.min.js?ver=4.3.10'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/jquery.easypiechart.min.js?ver=2.0.3'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/inc/slidenav/slidenav.min.js?ver=2.0'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/skip-link-focus-fix.js?ver=20130115'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/inc/modal/assets/js/modal.min.js'></script>
<script type='text/javascript' src='http://develop.fbmastery.com/wp-content/themes/MasteryBootstrap-master/js/tour-single-lesson.js?ver=4.1.2'></script>

<div id="noscript" class="alert alert-danger" style="position: fixed; top:0; z-index: 999999;">

	<h3 style="text-transform: uppercase; color: #B94A48;"><span class="glyphicon glyphicon-circle_exclamation_mark"></span> This Site Requires Javascript</h3>

	<p>We're sorry, you must have Javascript enabled in order for the Mastery Site to function correctly. You currently have Javascript disabled by your browser or there is something preventing Javascript from running correctly.</p>

	<p>Please enable Javascript and reload this page. If you would like instructions on how to enable Javascript for your browser <a href="http://www.enable-javascript.com/" title="Visit http://www.enable-javascript.com/">Click Here</a></p>

	<p>If you continue to have issues with Javascript please contact <a href="mailto: support@fortunebuildersmastery.com" title="Email support@fortunebuildersmastery.com">FB Mastery Support</a>.</p>

</div>
<script type="text/javascript">
	jQuery('#noscript').hide();
</script>
</body>
</html>
