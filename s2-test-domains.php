<?php

include '../wp-load.php';

/**
 * Test to read server scheme [http/https] and site name referenced by a few entities and services that performs lookups to WP site
 * in most cases.
 *
 **/

$um = fb_srv('ekino.wordpress.manager.user');

$uid = $_GET['uid'];

$u = $um->findOneById($uid);

echo "User: ";
echo $u->getDomain();

//-------

$em = fb_srv('fb_coaching.email_manager');

echo "<br/>";
echo "Email Manager: ";
echo $em->getDomain();

$cm = fb_srv('fb_coaching.coaching_manager');

echo "<br/>";
echo "Coaching Manager: ";
echo $cm->getDomain();
echo "<br/>";
echo "  --> TMP server var: " . $cm->getRequestSchemeTMP() . '://' . $cm->getQuickServerNameTMP();

die();