<?php 

include '../wp-load.php';



// $programs = fb_get_user_programs_by_exp ( get_current_user_id () );
$programs = fb_get_user_programs_by_exp ( 47611 );

// echo "<pre>" . print_r($programsLMS, true ) . "</pre>";
// die();

// // $p1 = new stdClass();
// // $p1->ID = 461425;
// // $p1->post_title;
// //course next: 461427
// $p1 = get_post(461425);

// $programs = [
// 	$p1,
// ];

$siteurl = site_url();
$output = '';
if($programs != 'None'){
    foreach ( $programs as $program ) {

    	//GET NEXT COURSE -- swap out by our next -- NEEDS FIX!!!
        $course_continue_id = LPT::get_course_continue_id ( $program->ID ); 
    	// $course_continue_id = 461427;

        $post = get_post ( $course_continue_id );
        $course_thumb = get_the_post_thumbnail( $post->ID, "thumb-cropped-w300-h300" );
        if ( empty($course_thumb) ) { $course_thumb = '<img src="'.get_bloginfo( 'stylesheet_directory' ).'/img/fb-thumb-grey.jpg" class="attachment-thumb-cropped-w300-h300 wp-post-image" alt="FB Placeholder" data-cid="$course_continue_id">'; }
        $program_percentage = LPT::get_program_percentage($program->ID);
        // $course_percentage = LPT::get_course_percentage($post->ID);
        // $total_lessons = LPT::get_number_of_lessons($post->ID);
        $completed_lessons = LPT::get_number_of_completed_lessons($post->ID);

        if($completed_lessons > 0){
            $btn_content = "Continue Course";
        } else {
            $btn_content = "Start Course";
        }
        $commercial_btn = "";

        if(($program->post_name) == 'commercial-academy-program'){

            $commercial_btn = '<a href="' . $siteurl . '/documents?category=Commercial" class="btn btn-nav btn-coursePanel">View Commercial Documents <span class="glyphicon glyphicon-chevron-right"></span></a>';
        }

        if ($program_percentage < 100) {
            if (strpos($program->post_title, 'Curriculum') !== false) {
                $postHeader = '                        <header class="panel-heading">
                <span class="fa fa-graduation-cap"></span> My ' . $program->post_title . '
            </header>';
            } else {
                $postHeader = '                        <header class="panel-heading">
                <span class="fa fa-graduation-cap"></span> My ' . $program->post_title . ' Curriculum
            </header>';
            }
            $template = '
            <article class="panel panel-mastery part-currentPanel">
                
            ' . $postHeader . '

                <div class="panel-body">
                    <div class="col-sm-12">
                        <div class="innerPanel-body">
                            <a href="resources/curriculum/'. $program->post_name . 'class="panel-body-link" title="'. $post->post_title .'">
                                <div class="coursePanel-img">
                                '. $course_thumb .'
                                </div>
                            </a>
                            <div class="course-info">
                                <a href="resources/curriculum/'. $program->post_name . '" class="panel-body-link" title="'. $post->post_title .'">
                                    <p class="course-title">Current Course: '. $post->post_title . '</p>
                                </a>
                                <a href="resources/curriculum/'. $program->post_name . '" class="btn btn-nav btn-coursePanel">'. $btn_content .' <span class="glyphicon glyphicon-chevron-right"></span></a>
                                '. $commercial_btn .'
                            </div>
                        </div>
                    </div>
                </div>

            </article>
            ';
        } else {
            $template = '
            <article class="panel panel-mastery part-currentPanel">

                    <header class="panel-heading">
                    <span class="fa fa-graduation-cap"></span> My '. $program->post_title . ' Curriculum
                </header>

                <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="innerPanel-body">
                                <div class="coursePanel-img">
                                    <img src="'.get_bloginfo( 'stylesheet_directory' ).'/img/fb-placeholder.jpg" class="attachment-thumb-cropped-w300-h300 wp-post-image" alt="FB Placeholder">
                                </div>
                                <div class="course-info">
                                    <p class="course-title">Congratulations, you have completed all core courses!</p>
                                    <a href="course-catalog" class="btn btn-nav btn-coursePanel">View Course Catalog <span class="glyphicon glyphicon-chevron-right"></span></a>
                                </div>
                            </div>
                        </div>
                </div>
            </article>
            ';
        }
        $output .= '
        <div class="row">
            <div class="current-course-col col-sm-12">
                <div class="panel-contain">
                    '. $template .'
                </div>
            </div>
        </div>
        ';

    }
}

echo $output;