<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';


$coachIds = [17615,
33479 //jherrera
];

$avgCallsByDay = 10;

$totalDays = 15;

$dformat = 'Y-m-d H:i:s';
$hformat = 'H:i:s';
$today = new \DateTime('now');

$endDate = new \DateTime('now');
$endDate->modify('+'.$totalDays.' days');
$endDate->setTime(23,59,59);

echo $today->format('Y-m-d H:i:s');
echo "<br/>";
echo $endDate->format($dformat);

$todayPivot = clone $today;
echo "<br/>";
echo $todayPivot->format($dformat);

$hoursInDay = range(0, 23);
if(!isset($_GET['go'])){
	echo " --- TEST MODE ---";
}

echo "<h2>Generating Calls</h2>";
while($todayPivot < $endDate){


	echo "Creating calls for " . $todayPivot->format('Y-m-d');
	echo "<br/>";

	$hoursWithCall = array_rand($hoursInDay, rand(1, 5));
	$callDay = clone $todayPivot;
	foreach ($hoursWithCall as $h) {
		$callDay->setTime($h, 0, 0);
		echo " **** HOUR: ". $callDay->format($hformat);
		echo "<br/>";

		$insParams = [
			'coach_id' => $coachIds[array_rand($coachIds)],
			'start_time' => $callDay->format($dformat),
			'end_time' => $callDay->modify('+45 minutes')->format($dformat),
			'status' => 0,
			'type' => 'ss_calls'
		];

		if(isset($_GET['go'])){
			$ins = $wpdb->insert('wp_fb_coaching_calls', $insParams);

			echo $wpdb->insert_id . ' (' . $ins . ' new row)';
			echo "<br/>";			
		}

		//echo "<pre>". print_r($insParams, true) . "</pre>";
	}

	//echo "<pre>". print_r($ar, true) . "</pre>";
	// $totalCallsForToday = rand(1, $avgCallsByDay + ( rand(-1,1) * rand(0, 5)));
	// foreach ($hoursInDay as $hour) {

	// 	$totalCallsForToday = rand(1, $avgCallsByDay + ( rand(-1,1) * rand(0, 5)));
	// 	echo ' +++Hour: '. $hour .' calls #' . $totalCallsForToday;
	// 	echo "<br/>";

	// 	// for($i = 0; $i < $totalCallsForToday; $i ++){
	// 	// 	//$sql
	// 	// }
	// }

	$todayPivot->modify('+1 day');


}

