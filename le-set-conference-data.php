<?php 


include '../wp-load.php';

//$uid = $_GET['uid'];

ob_start();
set_conference_data();

$dat = ob_get_contents();

ob_end_clean();

$dat = json_decode($dat, true);

foreach ($dat['confEvents'] as $key => $confEvent) {
    foreach ($confEvent as $key1 => $value1) {
        echo "[$key] " . $value1['post_title'];
        echo "<br/>";
    }
}

//OK with the above, integrate expiration dates only for commercial academy types -- 
//THEN go to Live Events section and filter out there too

//expDate support flow:
//currently, sys doesn't support CA exp dates, and as of today (1/22):
    //CA 101 => 36 mo.
    //CA 202 => 24 mo.
    //CA 303 => 24 mo.
//create a file with slugs and tags:
//tag represents product 17570 = CA 101
//this class will be injected as a filter to the ED file and return the date or calculate
//class receives start date or user, is primary etc
//table (file first):
//product_duration

//PRODUCT NAME              PRODUCT CODE  DURATION          CREATED_AT  EXPIRED_AT
//commercial academy 101    ca-101-v1     36 mo / millis    
//commercial academy 202    ca-202-v1     24 mo

//PRODUCT NAME is really a catgory or an end entity like PRODUCT


/*

    ExpDate
        ExpDateUser

    Definitions:
        ExpDate:
            name << human-read name << Commercial Academy 101
            common_name  <<< commercial-academy-101
            slug << unique <<< commercial-academy-101-wefmoix091
            duration << in months
            created_at
            deleted_at  << hmm can't delete easily

        ExpDateUser:
            exp_date_id
            user_id
            created_at
            expired_at

    ExpirationDateManager -->
        if user has expiration date -- return -- (check primary)
        if user has NOT -- return default -- get newest ExpDate -- get start date of user and bang!

        requs: 
            no unlimited so far



php app/console generate:doctrine:entity --no-interaction --entity=FBAppBundle:ExpirationDate --fields="name:string(length=255) common_name:string(length=255) slug:string(length=255) duration:integer created_at:datetime" --format=annotation

php app/console generate:doctrine:entity --no-interaction --entity=FBAppBundle:ExpirationDateUser --fields="created_at:datetime expired_at:datetime(nullable=true)" --format=annotation



*/

//----- DO TONY click's 1/23! --- DONE 1/23 will go live 1/24
//----- CREATE a ExpirationDateService class to play around with 