<?php 

include '../wp-load.php';

#http://members1.fortunebuilders.com/wp-lms-test-scripts/test-live-events.php?uid=47611&pid=
// on a conference

$uid = $_GET['uid'];

//2810 LE integration
$legacyName = 'live_events_exp';

$eds = FB_EXP_DATES::getInstance()->getAPIExpirationDateByCategoryId($uid, $legacyName);

$edsByWPPermission = [];

foreach ($eds as $ed) {
    if (isset($ed['wppermissions']))
        foreach ($ed['wppermissions'] as $catSlug) {
            $edsByWPPermission[$catSlug] = $ed;
        }
}

$postId = $_GET['pid'];

$confEvents = new WP_Query( array(
	'connected_type' => 'events_to_conferences',
	'connected_items' => $postId,
    'connected_order' => 'asc',
	'nopaging' => true,
	// 'fb_permissions' => true
) );


// echo "<pre>" . print_r($confEvents, true ) . "</pre>";die();

// $confEventPosts = [
// 	'bootcamps' => [],
// 	'events' => []
// ];


// $confRegEvents = [
// 	'registered' => [],
//     'waiting' => [],
//     'waitingList' => []
// ];


function print_user($userId) {
	$u = get_user_by('id', $userId);

	echo sprintf("<strong>%d) %s (%s)</strong>", $u->ID, $u->display_name, $u->user_login);
	echo "<br/>";
}

function print_expiration_dates($expirationDates) {
	foreach ($expirationDates as $key => $value) {
		// echo "<pre>" . print_r($value, true ) . "</pre>";
		$isExpired = $value['is_expired'];

		$color = 'b8ffe1';


		if ($isExpired) {
			$color = 'e1e1e1';
		}

		echo sprintf("<div style='background-color: $color;'> %d | <strong>%s (%d)</strong> | %s | %s | <br/> -- Cat: [%d %s] <br/> -- Slugs >> <i>%s</i> %s</div>", $value['id'], $value['code'], $value['permission_id'], $value['name'], $value['end_date'], $value['category']['id'], $value['category']['slug'], implode(', ', $value['wppermissions']), $value['is_expired'] ? " # EXP!" : '');
		echo "<br/>";
		echo "<br/>";
	}
}

print_user($uid);
print_expiration_dates($eds);

echo "<strong>EVENTS: </strong>";
echo "<br/>";

$availableLEs = [];

if ( !empty($confEvents->posts) ) {
	foreach ( $confEvents->posts as $post_key => &$ev_post ) {
        //2018

        $lookupWPCategories = ['eventcategory', 'fb-permissions'];

        // $goAhead = false;
        $goAhead = user_can($uid, 'use_admin_access') || FB_LiveEvents::getInstance()->isUserRegistered($uid, $ev_post->ID);

        $myExpirablePermission = null;

        foreach ($lookupWPCategories as $tax) {
            $terms = get_the_terms($ev_post->ID, $tax);
            foreach ($terms as $term) {
                if (array_key_exists($term->slug, $edsByWPPermission)) {
                    $myExpirablePermission = $edsByWPPermission[$term->slug];
                    if ( !$myExpirablePermission['is_expired'] ) {
                        $goAhead = true;

                        // display it
                        break;
                    }
                }
            }
        }

        // if (!$goAhead && !(current_user_can('use_admin_access') || FB_LiveEvents::getInstance()->isUserRegistered($uid, $ev_post->ID))) {
        // 	continue;
        // }

        //OK!!! where is our fix??? include the hotfix we did for fb-user-exp-validator
        // if (!current_user_can('use_admin_access') && !$goAhead && !FB_LiveEvents::getInstance()->isUserRegistered($uid, $ev_post->ID)) continue;
        //2018 --- /\

        //display it here!!! add ED info -- 

        if (!$goAhead) {
        	continue;
        }

	    echo "GOES TO: " . $ev_post->ID . ' ' . $ev_post->post_name ;
	    echo "<br/>";
	    echo sprintf(" [admin: %s, reg: %s, goAhead: %s]", user_can($uid, 'use_admin_access'), FB_LiveEvents::getInstance()->isUserRegistered($uid, $ev_post->ID), $goAhead);
	    echo "<br/>";
	}

}
