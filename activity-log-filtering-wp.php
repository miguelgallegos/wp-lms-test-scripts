<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

require_once '../wp-load.php';

if(!isset($_GET['uid'])){
	die('UID not found');
}
$user_id = $_GET['uid'];

$filters = 'task,action_plan,course';
if(isset($_GET['f'])){
	$filters = $_GET['f'];
}

//$filters = '';

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/4.0.2/bootstrap-material-design.css">

<script type="text/javascript">
var myurl = '/wp-admin/admin-ajax.php';

var _templateDiv = '<div>__OBJ__</div>';
(function($){
	$(document).ready(function(){

		var resultsContainer = $('.results');

		$('.loadData').click(function(e){
			e.preventDefault();
			var params = {
				action:'get_fb_activity_log',
				limit:50,
				offset:0,
				user_id: <?php echo $user_id ?>,
				filters: <?php echo $filters ?>
			};
			$.post(myurl, params, function(data){
				//console.log(data);
				$(data.activities).each(function(i, e){
					resultsContainer.append(_templateDiv.replace('__OBJ__', "<pre>" + JSON.stringify(e) + "</pre>"));
				});
			});

		});

	});
})(jQuery);
</script>
<button class="loadData">LOAD DATA</button>

<div class="results"></div>
<?php 

$a = new FB_Activity();
$args = [
	'limit' => 50,
	'offset' => 0,
	'user_id' => $user_id,
	'filters' => $filters
];

//note,call
//task,action_plan,course
//webinar
//event,masteryevents
//survey
//win
//tag_note
//website,nss_step,resource

$acts = $a->getActivities($args);
echo "<pre>". print_r(TimeMeasurer::$times, true) . "</pre>";
?>

<div>
<h2>PHP Results:</h2>

Total records: 
<?php
echo count($acts);
?>
<div>
<h4>Times:</h4>
             <?php echo (TimeMeasurer::$times['t2'] - TimeMeasurer::$times['t1']) ?> seconds to retrieve from WP<br/>
</div>
</div>