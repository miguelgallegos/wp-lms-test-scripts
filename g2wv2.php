<?php 

class Go2Webinar {

	private $url = 'https://api.getgo.com/oauth/v2/';

	private $consumerKey;


    /**
     * Returns Authorization URL
     *
     * @param string $redirect_url if empty it will add current url as redirect url
     *
     * @return string Authorization URL
     */
    function auth_url($redirect_url = '')
    {
        if($redirect_url == '')
        {
            $redirect_url = $this->get_current_url();
        }
        if($this->consumerKey == '')
        {
            throw new Exception('Please set consumerKey.');
        }
        return $this->url . 'authorize?response_type=code&client_id=' . $this->consumerKey . '&redirect_uri='. urlencode($redirect_url);
    }

    public function setConsumerKey( $key ) {

    	$this->consumerKey = $key;
    }

    /**
     * Returns the Current URL
     *
     * @return string The current URL
     */
    protected function get_current_url()
    {
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https://';
        }
        else
        {
            $protocol = 'http://';
        }

        $currentUrl = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $parts = parse_url($currentUrl);

        // use port if non default
        $port =
            isset($parts['port']) &&
            (($protocol === 'http://' && $parts['port'] !== 80) ||
                ($protocol === 'https://' && $parts['port'] !== 443))
                ? ':' . $parts['port'] : '';

        // rebuild
        return $protocol . $parts['host'] . $port . $parts['path'];
    }    

}