<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

$user = get_user_by('email', 'mgallegos@fortunebuilders.com');

$h = "<div class='{$user->first_name}'></div>";

echo $h;
echo "<br/>";
$hClean = "<div class='".preg_replace('/[^a-zA-Z0-9-]/', '-', $user->first_name) ."'></div>";
echo $hClean;