<?php 
$ins['lms-access'] = 'LMS Access';

$ins['live_events'] = "
Office System Academy
Sales and Negotiations
Construction Academy
Mindset Academy
Leadership Academy
Tax & Asset Academy
Money 
REO/SS 
Future FB
Full Immersion
Marketing and Wholesaling
Marketing Immersion
Wholesaling Bootcamp
Rehabbing
Rental Property
Commercial 101
Commercial 202
Commercial 303
Internet Intensive
";

$ins['replays'] = "
Office System Academy
Sales and Negotiations
Construction Academy
Mindset Academy
Leadership Academy
Full Immersion
Tax & Asset Academy
Money 
REO/SS 
Marketing and Wholesaling
Marketing Immersion
Wholesaling Bootcamp
Rehabbing
Rental Property
Commercial 101
Commercial 202
Commercial 303
Internet Intensive
";

$ins['learning-content'] = "
Mastery 1
Mastery 2
Mastery 3
Jumpstart
Passive Investor 
Sales and Negotiation
Commercial
Internet Quickstart
Leadership
Future Fortunebuilders
Tax Lien
Deal Review
Community
Facebook Group
";

$ins['group-coaching'] = "
Commercial
Leadership Academy
Mastery 
Jumpstart
Inner Circle
Starter 1
Starter 2
Starter 3
Starter 4";

$ins['coaching-calls'] = "
Inner Circle
Startup Specialist
Field Expert
Mind Protein Kick-off
Mind Protein Classic
Jumpsstart
Passive Income Intro
Passive Income 1st
Passive income 2nd
Passive Income Hand-Off
Passive income Follow-Up
Deal Review Consultation
Mind Protein Unlimited
Business 
Real Estate
Tech Challenged
";

$ins['pre-requisites'] = "Launch Group Coaching";

$idx = 1;
foreach ($ins as $key => $value) {    
    //echo 'Index: ' . $idx . ' - ' .$key . ' ';
    $vals = explode(PHP_EOL, $value);
    $vals = array_filter($vals);

    $tpl = "INSERT INTO `fb_permission` (`code`, `type_id`, `name`) VALUES ('__VAL__', __CAT_ID__, '__OVAL__');";

    foreach ($vals as $vo) {
        
        $vo = trim($vo);
        $v = $vo;        
        $v = str_replace(' ', '-', $v);
        $v = str_replace('/', '-', $v);
        $v = str_replace('&', 'and', $v);

        $v = strtolower($v);

        $line = str_replace('__VAL__', $v, $tpl);
        $line = str_replace('__OVAL__', $vo, $line);
        $line = str_replace('__CAT_ID__', $idx, $line);

        echo $line;

        echo "\n";
    }

    $idx ++;
}
