<?php

$host = 'http://127.0.0.1:8000/';
$url = $host . 'api/learning_objects';
// $ts = date('Y-m-d H:i:s');

// //SF 4:
// $dateObj = '2018-01-11T15:03:01.012345Z';

// $fields = array(
//     'title' => 'MG Title POST (create) ' . $ts,
//     'content' => 'Content ' . $ts,
//     'isActive' => true,
//     'createdAt' => $dateObj,
//     'createdById' => 1,
//     'type' => 1
// );

//$fields_string = json_encode($fields);

//open connection
$ch = curl_init();

//set the url, number of POST vars, POST data
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_POST, false);
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_HEADER, false);

//curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
    'Content-Type: application/json',                                                                                
    //'Content-Length: ' . strlen($fields_string),
    'X-AUTH-TOKEN: 1324'
    )
);  

//execute post
$result = curl_exec($ch);

// Check for errors and display the error message
if($errno = curl_errno($ch)) {
    $error_message = curl_strerror($errno);
    echo "!!! cURL error ({$errno}):\n {$error_message}";
    die();
}


$result = json_decode($result);

echo "<pre>". print_r($result, true) . "</pre>";

//close connection
curl_close($ch);