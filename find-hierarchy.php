<?php 

include '../wp-load.php';


$itemId = $_GET['item_id'];

$h = new FB_CurriculumHierarchy();

$parent = $h->findParent($itemId);

$children = $h->findChildren($itemId);

if ($parent) {

	echo "+Parent:";
	echo "<br/>";
	echo $parent;
	echo "<br/>";

} else {

	echo "No Parent, it's program.";
	echo "<br/>";
}

if ($children) {

	echo "+Children:";
	echo "<br/>";

	foreach ($children as $value) {

		echo "$value";
		echo "<br/>";
	}
} else {

	echo "No Children, it's lesson.";
	echo "<br/>";	
}


// 429872 program

/* courses

429873: ch: 429875, p:429872
429893
430000
430019
430033
430044
430052
430560

chapter
429875

lessons: p: 429875
429876
429877
429878
429879
429880
429881
429882 
*/
