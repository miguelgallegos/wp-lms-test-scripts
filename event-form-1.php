<?php
?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
<script type='text/javascript' src='http://mastery.com/wp-includes/js/jquery/jquery.js?ver=1.11.1'></script>
<script type="text/javascript">

jQuery(document).ready(function(){

	jQuery('.do').click(function(e){
		e.preventDefault();
		_do();
	});

	function _do(){
		jQuery.post('http://mastery.com/wp-content/plugins/fb-plugins/custom-plugins/fbbuild-events/masteryevent_proc.php', jQuery('#ConfirmForm').serialize())

		.error(function() {

		    alert('We\'re sorry, there was an error connecting to site. The server may be busy. Please try again in a few minutes.');
		    jQuery('#event_submit').val('Click Here To Register!');
		    jQuery('#event_submit').removeAttr('disabled');
		    return false;
		}).success(function() {
		    //document.ConfirmForm.setAttribute('action', 'http://mastery.com/event-registration-thank-you-for-registering/');
		    //document.ConfirmForm.submit();
		    jQuery('.do').after('<h2>DONE!</h2>');
		});
	}
});
</script>


<form action="https://cthomes.infusionsoft.com/AddForms/processFormSecure.jsp" name="ConfirmForm" id="ConfirmForm" method="POST" accept-charset="UTF-8" class="guest-form">
		<input id="theurl" name="theurl" type="hidden" value="https://cthomes.infusionsoft.com/AddForms/processFormSecure.jsp">
		<input id="addtag" name="addtag" type="hidden" value="34734">
		<input id="removetag" name="removetag" type="hidden" value="34738">
		<input id="addtag_mp" name="addtag_mp" type="hidden" value="">
		<input id="removetag_mp" name="removetag_mp" type="hidden" value="">
		<input id="user_id" name="user_id" type="hidden" value="1">
		<input type="hidden" name="infusion_xid" id="infusion_xid" value="4bd35073893d0e275d4bd3a64c37c5dc">
		<input type="hidden" name="infusion_type" id="infusion_type" value="CustomFormWeb">
		<input type="hidden" name="infusion_name" id="infusion_name" value="Mastery Event_Office Systems_1">
		<input type="hidden" name="post_id" id="post_id" value="426498"><div class="alert alert-warning"><strong><p><em>Completing this form only reserves a seat for yourself, Your Mastery Partner(s) must register separately through their own Mastery Account(s).  If you need to add a Mastery Partner, please contact <a href="mailto:support@fortunebuildersmastery.com" title="support@fortunebuildersmastery.com" target="_blank">Mastery Support</a></em></p>
</strong></div>
        <div class="formitems row">
            <div class="col-md-6">
        		<div class="formitem">
            		<label for="Contact0FirstName">First Name:</label>
            		<input size="15" name="Contact0FirstName" type="text" class="name required field form-control" id="Contact0FirstName" value="Mastery" readonly="readonly">
        		</div>
                <div class="formitem">
            		<label for="Contact0LastName">Last Name:</label>
            		<input size="15" name="Contact0LastName" type="text" class="name required field form-control" id="Contact0LastName" value="Admin" readonly="readonly">
        		</div>
                <div class="formitem">
            		<label for="Contact0Email">Email Address:</label>
            		<input size="15" name="Contact0Email" type="text" class="email required field form-control" id="Contact0Email" value="mgallegos@fortunebuilders.com" readonly="readonly">
        		</div>
                <div class="formitem">
                    <label for="Contact0Phone1">Phone Number:</label>
                    <input size="15" name="Contact0Phone1" type="text" class="phone required field form-control" id="Contact0Phone1" value="(763) 807-9116" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
                </div>
            </div>
            <div class="col-md-6">
            <i class="fa fa-trash-o pull-right delete-spot- delete-spot"></i>            
                <div class="formitem">
            		<label for="Contact0StreetAddress1">Street Address:</label>
            		<input size="15" name="Contact0StreetAddress1" type="text" class="address required field form-control" id="Contact0StreetAddress1" value="910 Grand Ave">
        		</div>
                <div class="formitem">
            		<label for="Contact0City">City:</label>
            		<input size="15" name="Contact0City" type="text" class="city required field form-control" id="Contact0City" value="San Diego">
        		</div>
                <div class="formitem">
            		<label for="Contact0State">State:</label>
            		<input size="15" name="Contact0State" type="text" class="state required field form-control" id="Contact0State" value="CA">
        		</div>
                <div class="formitem">
            		<label for="Contact0PostalCode">Postal Code:</label>
            		<input size="15" name="Contact0PostalCode" type="text" class="zip required field form-control" id="Contact0PostalCode" value="92109">
        		</div>
                <div class="formitem">
                    <input type="hidden" id="event-thankyou" name="event-thankyou" value="<h2 style='text-align: center;'>Thank You For Registering!</h2>
						    <div class='alert alert-warning'>
								<h3 class='eventthank_head'>REMINDER:</h3><p><strong>You have only reserved a seat for yourself. If your partner(s) or spouse have not yet registered for this event please have them log into the mastery site and register themselves before the event sells out. You have been sent a registration confirmation email with all the event details.  This email has a recommendation of hotels near the FB Training Center. </strong></p>
</div><a href='http://mastery.com/masteryevents/construction-management-academy-september-24th-25th-san-diego-ca/' class='button'>Return to the Event Page</a> <a href='http://mastery.com/eventcategory/mastery-live-events/' class='button'>Return to the Live Events Section</a>">
        		</div>
            </div>
        </div>
        <div style="clear:both;">
            <em style="display: inline-block; margin-bottom: 15px;">*All Fields Are Required. If you need to update your information visit your <a title="Member Profile" href="http://mastery.com/members/admin">Member Profile</a>.</em>
            <input name="event_submit" id="event_submit" type="submit" class="btn btn-final" value="Click Here To Register!">
        </div>
		</form>

<button class="do">DO</button>