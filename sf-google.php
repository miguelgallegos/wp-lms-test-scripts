<?php

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

// $cs = fb_srv('fb_coaching.google_calendar_service');

// $events = $cs->listEvents(['maxResults' => 10, 'timeMin' => date('c', strtotime('-3 weeks'))]);

// echo "<pre>". print_r($events->getItems(), true) . "</pre>";

// die();

// $gid = 'e3clovdujda9ntp8jvrirqhtvs';

// $slot = $cs->getSlotById($gid);

// /*
// $coachingCall = $params['coachingCall'];
// $coach = $coachingCall->getCoach();
// $summary = $params['summary'];
// $description = $params['description'];

// $details = [
//     'userEmail' => 'miguel.gallegos@gmail.com',
//     'summary' => $summary,//AJAX 
//     'description' => $description,//AJAX
//     'call_id' => $coachingCall->getId(),
//     'startTime' => $coachingCall->getStartTime()->format('c'),
//     'endTime' => $coachingCall->getEndTime()->format('c'),
//     'colorId' => 1
// ];

// $tz = $coach->getDefaultTimezone()->getSelected();
// if($coach->getTimezone())
//     $tz = $coach->getTimezone()->getSelected();

// $details['timezone'] = $tz;        
// */

// $templateData = [];

// $supportedGoogleVars = [
// 	'STUDENT_NAME',
// 	'STUDENT_EMAIL',
// 	'STUDENT_PHONE',
// 	'STUDENT_LMS_LINK',
// 	'STUDENT_PROGRAM_LEVEL',
// 	'SCHEDULED_BY_NAME',
// 	'SCHEDULED_BY_EMAIL',
// 	'CALL_TYPE',
// 	'CALL_SEQ',
// ];

// $templateData = [
// 		'STUDENT_NAME' => 'Miguel G.',
// 		'STUDENT_EMAIL' => 'mgallegos@fortunebuilders.com',
// 		'STUDENT_PHONE' => '619-123-1234',
// 		'STUDENT_LMS_LINK' => '<a href="click.com">Student | Phone</a>',
// 		'STUDENT_PROGRAM_LEVEL' => 'Platinum',
// 		'SCHEDULED_BY_INITIALS' => 'MG',
// 		'SCHEDULED_BY_NAME' => 'Mike Galet',
// 		'SCHEDULED_BY_EMAIL' => 'mlopez@fb.com',
// 		'SCHEDULED_AT' => '2017-06-20',
// 		'CALL_TYPE' => 'M',
// 		'CALL_SEQ' => 4,
// 	'DUDE' => 'ON'
// ];

// $cs->setGoogleTemplateData($templateData);

// $pTemplates = $cs->prepareSummaryDescriptionForSlot($slot);

// die(' -- ');

$g = fb_srv('fb_coaching.google_calendar_service');

//working 
if($_GET['op'] == 'pushtest')
	$g->testBookAndPushToGoogle();
if($_GET['op'] == 'cancel')
	$g->testCancelSlot();
if($_GET['op'] == 'colors')
	$g->getColors();
if($_GET['op'] == 'list')
	$g->testListEvents();
if($_GET['op'] == 'store')
	$g->testStoreEvent();

if($_GET['op'] == 'push')
	$g->pushToGoogle();
