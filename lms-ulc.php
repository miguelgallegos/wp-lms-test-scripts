<?php 

include_once('../wp-load.php');

function execInBackground($cmd) {
    if (substr(php_uname(), 0, 7) == "Windows"){
        pclose(popen("start /B ". $cmd, "r"));
    }
    else {//echo $cmd;
        shell_exec($cmd . " > /dev/null &");
    }
}

$opt = $_GET['opt'];
$keyword = 'update-lms-cache.php';
$path = '/var/www/mastery/scripts/';
if ($opt == 'update') {
    execInBackground('nohup php ' . $path . $keyword);
    echo 'Cache update started at ' . date('H:i m/d/Y');
}

if ($opt == 'check') {
    $cmd = 'ps ax | grep ' . $keyword;
    $r = shell_exec($cmd);
    if (strpos($r, 'php '.$path . $keyword) !== false) {
        echo 'Cache update in progress';
    } else {
        echo "No cache update process found.";
    }
}
