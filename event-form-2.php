<form id="confForm" novalidate="" class="ng-pristine ng-valid ng-submitted" _lpchecked="1" style="">

                                <!-- Conference Events -->
                                <article ng-controller="confEvents" class="ng-scope">
                                    <!-- ngIf: !confEvents.eventsEmpty --><div class="panel panel-mastery ng-scope" ng-if="!confEvents.eventsEmpty" style="opacity: 1;">
                                        <header class="panel-heading ng-binding" ng-bind="confForm.confGD.confPost.meta.ev_reconevents_title">Bootcamp Summit Pre Events (Select One)</header>
                                        <div class="panel-body">

                                            <!-- Event Rows -->
                                            <div class="conf-event-rows">
                                                <!-- ngRepeat: event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate' --><div ng-repeat="event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate'" class="conf-event-row clearfix ng-scope" fb-scroll-if="confForm.toggles[event.ID] === 'closed', confForm.toggles[event.ID]" ng-show="!event.meta.mwbcRequired || ( event.meta.mwbcRequired &amp;&amp; confForm.formData.user.attendedMWBC )">

                                                    <!-- Event Row -->
                                                    <div class="triangle-right"></div>
                                                    <div class="conf-event-listing clearfix">
                                                        <div class="conf-event-data">
                                                            <span ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-time reg-conflict" style="">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                                <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/17 9:00AM</span> - <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/19 6:00PM</span>
                                                            </span>
                                                            <span class="event-label event-label-status">
                                                                <button ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="conf-event-txt btn-link reg-conflict" ng-click="confForm.toggle(event.ID)" style="">

                                                                    <span ng-bind="event.post_title" class="ng-binding">Internet Intensive: October 17th - 19th, 2016 - San Diego, CA (Day 1 - Morning Session)</span>
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><span class="halfling halfling-chevron-down ng-scope" ng-if="confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined"></span><!-- end ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined -->
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'open' -->
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                            <!-- ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                            <span class="event-label event-label-select">
                                                                <button class="btn btn-normal btn-sm conf-event-btn btn-state-disabled" title="View Event Information" ng-class="{ 'btn-state-toggled': confForm.toggles[event.ID] === 'open', 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-click="confForm.toggle(event.ID)" style=""><span class="glyphicon glyphicon-circle_info"></span>
                                                                </button>
                                                                <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Row -->

                                                    <!-- Event Details -->
                                                    <div class="conf-event-details ng-hide" ng-show="confForm.toggles[event.ID] === 'open'">
                                                        <!-- ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection1content_hide &amp;&amp; event.meta.evsection1content">
                                                            <h4 ng-bind="event.meta.ev_section_1_title" class="ng-binding">Date, Time &amp; Location</h4>
                                                            <span ng-bind-html="event.meta.evsection1content" class="ng-binding"><p><strong>Date:</strong> October 17th - 19th, 2016<br>
<strong>Location:</strong> Sheraton San Diego Hotel &amp; Marina<br>
<strong>Address:</strong> 1380 Harbor Island Drive - San Diego, CA 92101<br>
<strong>Phone:</strong> (619) 291-2900 <br>
<strong>Classroom Times:</strong> Day 1 is a half day session - Monday 9am - 12:30pm, Tues.-Wed. are full days - 9am to 5pm each day</p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content -->
                                                        <!-- ngIf: event.post_content --><div class="conf-event-details-item ng-scope" ng-if="event.post_content">
                                                            <h4 ng-bind="event.meta.ev_section_0_title" class="ng-binding">Event Description</h4>

                                                            <!-- ngIf: event.post_thumbnail --><div ng-if="event.post_thumbnail" class="thumb event-thumb ng-binding ng-scope" ng-bind-html="event.post_thumbnail"><img width="300" height="184" src="http://mastery.com/wp-content/uploads/2016/08/San-Diego-1-300x184.jpg" class="attachment-medium wp-post-image" alt="San Diego 1"></div><!-- end ngIf: event.post_thumbnail -->
                                                            <span ng-bind-html="event.post_content" class="ng-binding"><p><strong><span style="font-weight: 400;">The Internet Intensive is a two and a half day training designed to supercharge your Internet presence for today’s fast paced world of business. During the event you will learn how to build an effective online presence and how to use social media to broadcast your business and build your audience. We will show you how to generate leads online by ranking in Google, and you’ll learn to identify and capitalize on additional streams of revenue in your existing business. After this event, you’ll walk away ready to automate and outsource your online presence so that you can focus on growing your business!</span></strong></p>
</span>
                                                        </div><!-- end ngIf: event.post_content -->
                                                        <!-- ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection4content_hide &amp;&amp; event.meta.evsection4content">
                                                            <h4 ng-bind-html="event.meta.ev_section_4_title" class="ng-binding">Hotel Information</h4>

                                                            <div class="panel-body">
                                                                <div class="alert alert-success ng-binding" ng-bind-html="event.meta.evsection4content"><p>We have a LIMITED block of guest rooms locked in at a discounted rate of $189 per night under the group name, FortuneBuilders. Once the room block is full, standard rates apply so PLEASE book your accommodations at the Sheraton San Diego Hotel &amp; Marina right away to take advantage of this discounted rate as it is first come, first serve! Make your reservation by calling 1-877-734-2726 or by clicking this link <a href="https://www.starwoodmeeting.com/Book/FortuneBuildersBootcampSummit" target="_blank">Book Your Hotel</a>.</p>
</div>
                                                            </div>
                                                        </div><!-- end ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content -->
                                                        <!-- ngIf: !event.meta.evsection5content_hide && event.meta.evsection5content -->
                                                        <!-- ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content -->
                                                        <div class="conf-event-details-footer">
                                                            <button class="btn btn-normal btn-sm conf-event-btn" ng-click="confForm.toggle(event.ID)">Close
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Details -->
                                                </div><!-- end ngRepeat: event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate' --><div ng-repeat="event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate'" class="conf-event-row clearfix ng-scope" fb-scroll-if="confForm.toggles[event.ID] === 'closed', confForm.toggles[event.ID]" ng-show="!event.meta.mwbcRequired || ( event.meta.mwbcRequired &amp;&amp; confForm.formData.user.attendedMWBC )">

                                                    <!-- Event Row -->
                                                    <div class="triangle-right"></div>
                                                    <div class="conf-event-listing clearfix">
                                                        <div class="conf-event-data">
                                                            <span ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-time reg-conflict" style="">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                                <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/17 12:30PM</span> - <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/19 5:00PM</span>
                                                            </span>
                                                            <span class="event-label event-label-status">
                                                                <button ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="conf-event-txt btn-link reg-conflict" ng-click="confForm.toggle(event.ID)" style="">

                                                                    <span ng-bind="event.post_title" class="ng-binding">Internet Intensive - October 17th - 19th, 2016 - San Diego, CA (Day 1 - Afternoon Session)</span>
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><span class="halfling halfling-chevron-down ng-scope" ng-if="confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined"></span><!-- end ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined -->
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'open' -->
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                            <!-- ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                            <span class="event-label event-label-select">
                                                                <button class="btn btn-normal btn-sm conf-event-btn btn-state-disabled" title="View Event Information" ng-class="{ 'btn-state-toggled': confForm.toggles[event.ID] === 'open', 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-click="confForm.toggle(event.ID)" style=""><span class="glyphicon glyphicon-circle_info"></span>
                                                                </button>
                                                                <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Row -->

                                                    <!-- Event Details -->
                                                    <div class="conf-event-details ng-hide" ng-show="confForm.toggles[event.ID] === 'open'">
                                                        <!-- ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection1content_hide &amp;&amp; event.meta.evsection1content">
                                                            <h4 ng-bind="event.meta.ev_section_1_title" class="ng-binding">Date, Time &amp; Location</h4>
                                                            <span ng-bind-html="event.meta.evsection1content" class="ng-binding"><p><strong>Date:</strong> October 17th - 19th, 2016<br>
<strong>Location:</strong> Sheraton San Diego Hotel &amp; Marina<br>
<strong>Address:</strong> 1380 Harbor Island Drive - San Diego, CA 92101<br>
<strong>Classroom Times:</strong> Day 1 is a half day session - Monday 1:30pm - 6:00pm, Tues.-Wed. are full days - 9am to 5pm each day</p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content -->
                                                        <!-- ngIf: event.post_content --><div class="conf-event-details-item ng-scope" ng-if="event.post_content">
                                                            <h4 ng-bind="event.meta.ev_section_0_title" class="ng-binding">Event Description</h4>

                                                            <!-- ngIf: event.post_thumbnail --><div ng-if="event.post_thumbnail" class="thumb event-thumb ng-binding ng-scope" ng-bind-html="event.post_thumbnail"><img width="300" height="184" src="http://mastery.com/wp-content/uploads/2016/08/San-Diego-1-300x184.jpg" class="attachment-medium wp-post-image" alt="San Diego 1"></div><!-- end ngIf: event.post_thumbnail -->
                                                            <span ng-bind-html="event.post_content" class="ng-binding"><p><strong><span style="font-weight: 400;">The Internet Intensive is a two and a half day training designed to supercharge your Internet presence for today’s fast paced world of business. During the event you will learn how to build an effective online presence and how to use social media to broadcast your business and build your audience. We will show you how to generate leads online by ranking in Google, and you’ll learn to identify and capitalize on additional streams of revenue in your existing business. After this event, you’ll walk away ready to automate and outsource your online presence so that you can focus on growing your business!</span></strong></p>
</span>
                                                        </div><!-- end ngIf: event.post_content -->
                                                        <!-- ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection4content_hide &amp;&amp; event.meta.evsection4content">
                                                            <h4 ng-bind-html="event.meta.ev_section_4_title" class="ng-binding">Hotel Information</h4>

                                                            <div class="panel-body">
                                                                <div class="alert alert-success ng-binding" ng-bind-html="event.meta.evsection4content"><p>We have a LIMITED block of guest rooms locked in at a discounted rate of $189 per night under the group name, FortuneBuilders. Once the room block is full, standard rates apply so PLEASE book your accommodations at the Sheraton San Diego Hotel &amp; Marina right away to take advantage of this discounted rate as it is first come, first serve! Make your reservation by calling 1-877-734-2726 or by clicking this link <a href="https://www.starwoodmeeting.com/Book/FortuneBuildersBootcampSummit" target="_blank">Book Your Hotel</a>.</p>
</div>
                                                            </div>
                                                        </div><!-- end ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content -->
                                                        <!-- ngIf: !event.meta.evsection5content_hide && event.meta.evsection5content -->
                                                        <!-- ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content -->
                                                        <div class="conf-event-details-footer">
                                                            <button class="btn btn-normal btn-sm conf-event-btn" ng-click="confForm.toggle(event.ID)">Close
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Details -->
                                                </div><!-- end ngRepeat: event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate' --><div ng-repeat="event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate'" class="conf-event-row clearfix ng-scope" fb-scroll-if="confForm.toggles[event.ID] === 'closed', confForm.toggles[event.ID]" ng-show="!event.meta.mwbcRequired || ( event.meta.mwbcRequired &amp;&amp; confForm.formData.user.attendedMWBC )">

                                                    <!-- Event Row -->
                                                    <div class="triangle-right"></div>
                                                    <div class="conf-event-listing clearfix">
                                                        <div class="conf-event-data">
                                                            <span ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-time reg-conflict" style="">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                                <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/19 9:00AM</span> - <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/20 6:00PM</span>
                                                            </span>
                                                            <span class="event-label event-label-status">
                                                                <button ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="conf-event-txt btn-link reg-conflict" ng-click="confForm.toggle(event.ID)" style="">

                                                                    <span ng-bind="event.post_title" class="ng-binding">Asset Protection &amp; Tax Planning Academy - October 19th - 20th - San Diego, CA</span>
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><span class="halfling halfling-chevron-down ng-scope" ng-if="confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined"></span><!-- end ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined -->
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'open' -->
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                            <!-- ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                            <span class="event-label event-label-select">
                                                                <button class="btn btn-normal btn-sm conf-event-btn btn-state-disabled" title="View Event Information" ng-class="{ 'btn-state-toggled': confForm.toggles[event.ID] === 'open', 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-click="confForm.toggle(event.ID)" style=""><span class="glyphicon glyphicon-circle_info"></span>
                                                                </button>
                                                                <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Row -->

                                                    <!-- Event Details -->
                                                    <div class="conf-event-details ng-hide" ng-show="confForm.toggles[event.ID] === 'open'">
                                                        <!-- ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection1content_hide &amp;&amp; event.meta.evsection1content">
                                                            <h4 ng-bind="event.meta.ev_section_1_title" class="ng-binding">Date, Time &amp; Location</h4>
                                                            <span ng-bind-html="event.meta.evsection1content" class="ng-binding"><p><strong>Date:</strong> October 19th - 20th, 2016<br>
<strong>Location:</strong> Sheraton San Diego Hotel &amp; Marina<br>
<strong>Address:</strong> 1380 Harbor Island Drive - San Diego, CA 92101<br>
<strong>Phone:</strong> (619) 291-2900 <br>
<strong>Classroom Times:</strong> 9am to 6pm (times subject to change)</p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content -->
                                                        <!-- ngIf: event.post_content --><div class="conf-event-details-item ng-scope" ng-if="event.post_content">
                                                            <h4 ng-bind="event.meta.ev_section_0_title" class="ng-binding">Event Description</h4>

                                                            <!-- ngIf: event.post_thumbnail --><div ng-if="event.post_thumbnail" class="thumb event-thumb ng-binding ng-scope" ng-bind-html="event.post_thumbnail"><img width="300" height="184" src="http://mastery.com/wp-content/uploads/2016/08/San-Diego-1-300x184.jpg" class="attachment-medium wp-post-image" alt="San Diego 1"></div><!-- end ngIf: event.post_thumbnail -->
                                                            <span ng-bind-html="event.post_content" class="ng-binding"><p><span style="font-weight: 400;">Knowing how to protect assets and preserve wealth is one of the most valuable areas to learn as a real estate investor. Taking the proper steps today helps insulate against personal and professional risks. For many people, real estate investing is a vehicle to not just make money, but to build generational wealth for one’s family and future. Having a comprehensive Wealth Protection plan in place has provided us with confidence that our assets are secure and our legacy is protected. During the academy, you’ll learn from seasoned experts who will share effective strategies and techniques on how to proactively protect assets. Other useful topics include reducing exposure to potential lawsuits and predators, understanding the protections offered by the various business structures, legacy planning and more.</span></p>
<p><span style="font-weight: 400;">Taxes are one aspect of real estate investing that can be both confusing and intimidating – not to mention time consuming and expensive. So, the more you know, the better prepared you’ll be to deal with them. While the old saying, “the only two sure things in life are death and taxes” may be true, proper planning can help ensure the rules are used to your advantage, and you have the proper documentation in the event of an audit. At the event, we’ll discuss advanced strategies on how to minimize overall tax obligations, avoid triggering a taxable event, understand the tax implications of different types of income (passive vs. active), increase the bottom line and some additional creative and innovative year-end tax strategies that may have a big impact on the overall financial picture.</span></p>
<p><i><span style="font-weight: 400;">FortuneBuilders and its affiliates do not provide individual tax, legal or accounting advice. These strategies are shared for informational purposes only. They are neither intended to provide, nor should they be relied on as tax, legal or accounting advice. Your situation is unique, so always consult your own tax, legal and accounting advisors before engaging in any transaction.</span></i></p>
<p>&nbsp;</p>
</span>
                                                        </div><!-- end ngIf: event.post_content -->
                                                        <!-- ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection4content_hide &amp;&amp; event.meta.evsection4content">
                                                            <h4 ng-bind-html="event.meta.ev_section_4_title" class="ng-binding">Hotel Information</h4>

                                                            <div class="panel-body">
                                                                <div class="alert alert-success ng-binding" ng-bind-html="event.meta.evsection4content"><p>We have a LIMITED block of guest rooms locked in at a discounted rate of $189 per night under the group name, FortuneBuilders. Once the room block is full, standard rates apply so PLEASE book your accommodations at the Sheraton San Diego Hotel &amp; Marina right away to take advantage of this discounted rate as it is first come, first serve! Make your reservation by calling 1-877-734-2726 or by clicking this link <a href="https://www.starwoodmeeting.com/Book/FortuneBuildersBootcampSummit" target="_blank">Book Your Hotel</a>.</p>
</div>
                                                            </div>
                                                        </div><!-- end ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content -->
                                                        <!-- ngIf: !event.meta.evsection5content_hide && event.meta.evsection5content -->
                                                        <!-- ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content -->
                                                        <div class="conf-event-details-footer">
                                                            <button class="btn btn-normal btn-sm conf-event-btn" ng-click="confForm.toggle(event.ID)">Close
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Details -->
                                                </div><!-- end ngRepeat: event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate' --><div ng-repeat="event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate'" class="conf-event-row clearfix ng-scope" fb-scroll-if="confForm.toggles[event.ID] === 'closed', confForm.toggles[event.ID]" ng-show="!event.meta.mwbcRequired || ( event.meta.mwbcRequired &amp;&amp; confForm.formData.user.attendedMWBC )">

                                                    <!-- Event Row -->
                                                    <div class="triangle-right"></div>
                                                    <div class="conf-event-listing clearfix">
                                                        <div class="conf-event-data">
                                                            <span ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-time" style="">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                                <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/19 9:00AM</span> - <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/19 6:00PM</span>
                                                            </span>
                                                            <span class="event-label event-label-status">
                                                                <button ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="conf-event-txt btn-link" ng-click="confForm.toggle(event.ID)" style="">

                                                                    <span ng-bind="event.post_title" class="ng-binding">Rental Property Intensive: October 19th, 2016 - San Diego, CA</span>
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><span class="halfling halfling-chevron-down ng-scope" ng-if="confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined"></span><!-- end ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined -->
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'open' -->
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                            <!-- ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                            <span class="event-label event-label-select">
                                                                <button class="btn btn-normal btn-sm conf-event-btn" title="View Event Information" ng-class="{ 'btn-state-toggled': confForm.toggles[event.ID] === 'open', 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-click="confForm.toggle(event.ID)" style=""><span class="glyphicon glyphicon-circle_info"></span>
                                                                </button>
                                                                <button class="btn btn-normal btn-sm conf-event-btn ng-binding" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" style="">Deselect</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Row -->

                                                    <!-- Event Details -->
                                                    <div class="conf-event-details ng-hide" ng-show="confForm.toggles[event.ID] === 'open'">
                                                        <!-- ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection1content_hide &amp;&amp; event.meta.evsection1content">
                                                            <h4 ng-bind="event.meta.ev_section_1_title" class="ng-binding">Date, Time &amp; Location</h4>
                                                            <span ng-bind-html="event.meta.evsection1content" class="ng-binding"><p><strong>Date:</strong> October 19th, 2016<br>
<strong>Location:</strong> Sheraton San Diego Hotel &amp; Marina<br>
<strong>Address:</strong> 1380 Harbor Island Drive - San Diego, CA 92101 <br>
<strong>Phone:</strong> (619) 291-2900 <br>
<strong>Classroom Time:</strong> 9am to 6pm (times subject to change)</p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content -->
                                                        <!-- ngIf: event.post_content --><div class="conf-event-details-item ng-scope" ng-if="event.post_content">
                                                            <h4 ng-bind="event.meta.ev_section_0_title" class="ng-binding">Event Description</h4>

                                                            <!-- ngIf: event.post_thumbnail --><div ng-if="event.post_thumbnail" class="thumb event-thumb ng-binding ng-scope" ng-bind-html="event.post_thumbnail"><img width="300" height="184" src="http://mastery.com/wp-content/uploads/2016/08/San-Diego-1-300x184.jpg" class="attachment-medium wp-post-image" alt="San Diego 1"></div><!-- end ngIf: event.post_thumbnail -->
                                                            <span ng-bind-html="event.post_content" class="ng-binding"><p><strong><span style="font-weight: 400;">Our Rental Property Intensive is a revealing, one-day event taught by Paul Shively, head of the Passive Income Club, and rental property experts from his team. Here, you’ll learn how to implement the same successful systems that Than, Paul, and JD use to purchase their Passive Income Properties, build their own personal rental property portfolios, set benchmarks for their future investment opportunities, and find the best properties to produce high earning, monthly passive income. Paul will also walk you through how he and his team evaluate target markets for rental properties, as well as how they identify the absolute best property managers to make the experience as hands-off as possible. Learning how to evaluate specific deals, as well as how to use FortuneBuilders’ own set of purchase criteria, will ensure your rental properties meet the same A+ standard they subscribe to and, as such, can greatly improve the likelihood of you enjoying similar success.</span></strong></p>
</span>
                                                        </div><!-- end ngIf: event.post_content -->
                                                        <!-- ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection4content_hide &amp;&amp; event.meta.evsection4content">
                                                            <h4 ng-bind-html="event.meta.ev_section_4_title" class="ng-binding">Hotel Information</h4>

                                                            <div class="panel-body">
                                                                <div class="alert alert-success ng-binding" ng-bind-html="event.meta.evsection4content"><p>We have a LIMITED block of guest rooms locked in at a discounted rate of $189 per night under the group name, FortuneBuilders. Once the room block is full, standard rates apply so PLEASE book your accommodations at the Sheraton San Diego Hotel &amp; Marina right away to take advantage of this discounted rate as it is first come, first serve! Make your reservation by calling 1-877-734-2726 or by clicking this link <a href="https://www.starwoodmeeting.com/Book/FortuneBuildersBootcampSummit" target="_blank">Book Your Hotel</a>.</p>
</div>
                                                            </div>
                                                        </div><!-- end ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content -->
                                                        <!-- ngIf: !event.meta.evsection5content_hide && event.meta.evsection5content -->
                                                        <!-- ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content -->
                                                        <div class="conf-event-details-footer">
                                                            <button class="btn btn-normal btn-sm conf-event-btn" ng-click="confForm.toggle(event.ID)">Close
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" style="">Deselect</button>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Details -->
                                                </div><!-- end ngRepeat: event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate' --><div ng-repeat="event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate'" class="conf-event-row clearfix ng-scope" fb-scroll-if="confForm.toggles[event.ID] === 'closed', confForm.toggles[event.ID]" ng-show="!event.meta.mwbcRequired || ( event.meta.mwbcRequired &amp;&amp; confForm.formData.user.attendedMWBC )">

                                                    <!-- Event Row -->
                                                    <div class="triangle-right"></div>
                                                    <div class="conf-event-listing clearfix">
                                                        <div class="conf-event-data">
                                                            <span ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-time reg-conflict" style="">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                                <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/19 9:00AM</span> - <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/19 6:00PM</span>
                                                            </span>
                                                            <span class="event-label event-label-status">
                                                                <button ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="conf-event-txt btn-link reg-conflict" ng-click="confForm.toggle(event.ID)" style="">

                                                                    <span ng-bind="event.post_title" class="ng-binding">Mindset Intensive: October 19th, 2016 - San Diego, CA</span>
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><span class="halfling halfling-chevron-down ng-scope" ng-if="confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined"></span><!-- end ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined -->
                                                                    <!-- ngIf: confForm.toggles[event.ID] === 'open' -->
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                            <!-- ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                            <span class="event-label event-label-select">
                                                                <button class="btn btn-normal btn-sm conf-event-btn btn-state-disabled" title="View Event Information" ng-class="{ 'btn-state-toggled': confForm.toggles[event.ID] === 'open', 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-click="confForm.toggle(event.ID)" style=""><span class="glyphicon glyphicon-circle_info"></span>
                                                                </button>
                                                                <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Row -->

                                                    <!-- Event Details -->
                                                    <div class="conf-event-details ng-hide" ng-show="confForm.toggles[event.ID] === 'open'">
                                                        <!-- ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection1content_hide &amp;&amp; event.meta.evsection1content">
                                                            <h4 ng-bind="event.meta.ev_section_1_title" class="ng-binding">Date, Time &amp; Location</h4>
                                                            <span ng-bind-html="event.meta.evsection1content" class="ng-binding"><p><strong>Date:</strong> October 19th, 2016<br>
<strong>Location:</strong> Sheraton San Diego Hotel &amp; Marina<br>
<strong>Address:</strong> 1380 Harbor Island Drive - San Diego, CA 92101 <br>
<strong>Phone:</strong> (619) 291-2900 <br>
<strong>Classroom Time:</strong> 9am to 6pm (times subject to change)</p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content -->
                                                        <!-- ngIf: event.post_content --><div class="conf-event-details-item ng-scope" ng-if="event.post_content">
                                                            <h4 ng-bind="event.meta.ev_section_0_title" class="ng-binding">Event Description</h4>

                                                            <!-- ngIf: event.post_thumbnail --><div ng-if="event.post_thumbnail" class="thumb event-thumb ng-binding ng-scope" ng-bind-html="event.post_thumbnail"><img width="300" height="184" src="http://mastery.com/wp-content/uploads/2016/08/San-Diego-1-300x184.jpg" class="attachment-medium wp-post-image" alt="San Diego 1"></div><!-- end ngIf: event.post_thumbnail -->
                                                            <span ng-bind-html="event.post_content" class="ng-binding"><p><strong><span style="font-weight: 400;">One simple truth you will hear spoken over and over again from Mastery coaches, mentors and FortuneBuilders alumni alike is this: reaching your maximum potential as a successful real estate investor begins and ends with your mindset. As Than says all the time, “Your success as an entrepreneur is 70% mindset and 30% technical.” You’ll see exactly what Than means when you attend this enlightening one day event co-taught by Konrad Sopielnikow, owner and co-founder of FortuneBuilders, and Jeff Rutkowski, FB alumni and coach. During this program, you’ll learn how to break through any lingering fears you might have that are negatively impacting your success. You’ll learn how to rise above those fears and use them as the motivation you need to take actions in your daily life so you can grow and evolve your business. You’ll also learn how managing your time effectively, understanding how failure can help you succeed, and how setting business goals not only increases your speed of implementation, but also helps you achieve greater results. With the benefits of this invaluable knowledge, you’ll come to realize just how important a role having the right mindset can play in taking the success of your real estate investing business to a whole new level.</span></strong></p>
</span>
                                                        </div><!-- end ngIf: event.post_content -->
                                                        <!-- ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection4content_hide &amp;&amp; event.meta.evsection4content">
                                                            <h4 ng-bind-html="event.meta.ev_section_4_title" class="ng-binding">Hotel Information</h4>

                                                            <div class="panel-body">
                                                                <div class="alert alert-success ng-binding" ng-bind-html="event.meta.evsection4content"><p>We have a LIMITED block of guest rooms locked in at a discounted rate of $189 per night under the group name, FortuneBuilders. Once the room block is full, standard rates apply so PLEASE book your accommodations at the Sheraton San Diego Hotel &amp; Marina right away to take advantage of this discounted rate as it is first come, first serve! Make your reservation by calling 1-877-734-2726 or by clicking this link <a href="https://www.starwoodmeeting.com/Book/FortuneBuildersBootcampSummit" target="_blank">Book Your Hotel</a>.</p>
</div>
                                                            </div>
                                                        </div><!-- end ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content -->
                                                        <!-- ngIf: !event.meta.evsection5content_hide && event.meta.evsection5content -->
                                                        <!-- ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content -->
                                                        <div class="conf-event-details-footer">
                                                            <button class="btn btn-normal btn-sm conf-event-btn" ng-click="confForm.toggle(event.ID)">Close
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'event')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                        </div>
                                                    </div>
                                                    <!-- / Event Details -->
                                                </div><!-- end ngRepeat: event in confEvents.events | orderObjectsBy: 'meta.ev_section_0_startdate' -->
                                            </div>
                                            <!-- Event Rows -->

                                        </div>
                                    </div><!-- end ngIf: !confEvents.eventsEmpty -->
                                </article>
                                <!-- / Conference Events -->

                                <!-- Conference Bootcamps -->
                                <article ng-controller="confBootcamps" class="ng-scope">
                                    <!-- ngIf: !confBootcamps.bootcampsEmpty --><div class="panel panel-mastery ng-scope" ng-if="!confBootcamps.bootcampsEmpty" style="opacity: 1;">
                                        <header class="panel-heading ng-binding" ng-bind="confForm.confGD.confPost.meta.ev_reconbootcamps_title">Bootcamp Summit Events (Select One)</header>
                                        <div class="panel-body">

                                            <!-- Bootcamp Rows -->
                                            <div class="conf-event-rows">
                                                <!-- ngRepeat: event in confBootcamps.events | orderObjectsBy: 'p2p_order_from' --><div ng-repeat="event in confBootcamps.events | orderObjectsBy: 'p2p_order_from'" class="conf-event-row ng-scope" fb-scroll-if="confForm.toggles[event.ID] === 'closed', confForm.toggles[event.ID]" ng-show="( event.meta.mwbc &amp;&amp; !confForm.formData.user.attendedMWBC ) || confForm.formData.user.attendedMWBC || !confBootcamps.mwbc">

                                                    <!-- Bootcamp Row -->
                                                    <div class="triangle-right"></div>
                                                    <div class="conf-event-listing clearfix">
                                                        <div class="conf-event-data">
                                                        <span ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-time">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/20 9:00AM</span> -
                                                            <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/23 6:00PM</span>
                                                        </span>
                                                        <span class="event-label event-label-status">
                                                            <button ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="conf-event-txt btn-link" ng-click="confForm.toggle(event.ID)">
                                                                <!-- ngIf: event.meta.mwbc && !confForm.formData.user.attendedMWBC && !(event.meta.can_view_MWBC_BLOCKED) -->
                                                                <span ng-bind="event.post_title" class="ng-binding">The Wholesaling Bootcamp: October 20th – 23rd, 2016 – San Diego, CA</span> <!-- ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><!-- ngIf: confForm.toggles[event.ID] === 'open' --><span class="halfling halfling-chevron-up ng-scope" ng-if="confForm.toggles[event.ID] === 'open'"></span><!-- end ngIf: confForm.toggles[event.ID] === 'open' -->
                                                            </button>
                                                        </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                        <!-- ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                        <span class="event-label event-label-select">
                                                            <button class="btn btn-normal btn-sm conf-event-btn btn-state-toggled" title="View Event Information" ng-class="{ 'btn-state-toggled': confForm.toggles[event.ID] === 'open', 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-click="confForm.toggle(event.ID)" style=""><span class="glyphicon glyphicon-circle_info"></span>
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding" ng-click="confForm.select(event.ID, 'bootcamp')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]">Deselect</button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <!-- Bootcamp Row -->

                                                    <!-- Bootcamp Details -->
                                                    <div class="conf-event-details" ng-show="confForm.toggles[event.ID] === 'open'" style="">
                                                        <!-- ngIf: event.meta.mwbc && !confForm.formData.user.attendedMWBC && !(event.meta.can_view_MWBC_BLOCKED) -->
                                                        <!-- ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection1content_hide &amp;&amp; event.meta.evsection1content">
                                                            <h4 ng-bind="event.meta.ev_section_1_title" class="ng-binding">Date, Time &amp; Location</h4>
                                                            <span ng-bind-html="event.meta.evsection1content" class="ng-binding"><p><strong>Date:</strong> October 20th - 23rd, 2016<br>
<strong>Location:</strong> Sheraton San Diego Hotel &amp; Marina<br>
<strong>Address:</strong> 1380 Harbor Island Drive - San Diego, CA 92101 <br>
<strong>Phone:</strong> (619) 291-2900 <br>
<strong>Classroom Times:</strong> 9am to 6pm each day (times subject to change)</p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content -->
                                                        <!-- ngIf: event.post_content --><div class="conf-event-details-item ng-scope" ng-if="event.post_content">
                                                            <h4 ng-bind="event.meta.ev_section_0_title" class="ng-binding">Event Description</h4>

                                                            <!-- ngIf: event.post_thumbnail --><div ng-if="event.post_thumbnail" class="thumb event-thumb ng-binding ng-scope" ng-bind-html="event.post_thumbnail"><img width="300" height="184" src="http://mastery.com/wp-content/uploads/2016/08/San-Diego-1-300x184.jpg" class="attachment-medium wp-post-image" alt="San Diego 1"></div><!-- end ngIf: event.post_thumbnail -->
                                                            <span ng-bind-html="event.post_content" class="ng-binding"><p><span style="font-weight: 400;">This intensive training is taught by Than Merrill and Jeff Rutkowski. Many of our students have found this bootcamp to be especially valuable, as wholesaling is one the fastest ways to make consistent money in real estate.&nbsp;</span>In this bootcamp, you’ll gain a very firm understanding of how to wholesale all types of real estate deals. Likewise, you will leave the training with an exact blueprint detailing which systems you should leverage in order to grow the wholesaling side of your business.&nbsp;We take a very systematic approach in this training as we break down all the systems required to wholesale multiple deals per month. The bootcamp is also “case study” driven from wholesale deals CT Homes has recently closed and are actively working on.</p>
<p><strong><span style="font-weight: 400;">*Prerequisite to all other bootcamps.</span></strong></p>
</span>
                                                        </div><!-- end ngIf: event.post_content -->
                                                        <!-- ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection4content_hide &amp;&amp; event.meta.evsection4content">
                                                            <h4 ng-bind-html="event.meta.ev_section_4_title" class="ng-binding">Hotel Information</h4>

                                                            <div class="panel-body">
                                                                <div class="alert alert-success ng-binding" ng-bind-html="event.meta.evsection4content"><p>We have a LIMITED block of guest rooms locked in at a discounted rate of $189 per night under the group name, FortuneBuilders. Once the room block is full, standard rates apply so PLEASE book your accommodations at the Sheraton San Diego Hotel &amp; Marina right away to take advantage of this discounted rate as it is first come, first serve! Make your reservation by calling 1-877-734-2726 or by clicking this link <a href="https://www.starwoodmeeting.com/Book/FortuneBuildersBootcampSummit" target="_blank">Book Your Hotel</a>.</p>
</div>
                                                            </div>
                                                        </div><!-- end ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content -->
                                                        <!-- ngIf: !event.meta.evsection5content_hide && event.meta.evsection5content -->
                                                        <!-- ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection6content_hide &amp;&amp; event.meta.evsection6content">
                                                            <h4 ng-bind="event.meta.ev_section_6_title" class="ng-binding">Event Agenda</h4>
                                                            <span ng-bind-html="event.meta.evsection6content" class="ng-binding"><p></p><div class="well"><p></p>
<h3>DAY 1: Setting Up Your Company &amp; Creating a Marketing Plan to Find Deals</h3>
<ul>
<li>Learn how to set up your corporate and office systems correctly</li>
<li>Learn how to integrate technology from day one including setting up multiple websites, your phone systems, and database</li>
<li>How to implement and automate our brand new offline marketing strategies (including a phenomenal fulfillment house we’ve recently teamed up with that now handles ALL of our direct mail)</li>
<li>FREE online marketing techniques that we are using to drive in seller leads!</li>
<li>How to create an online presence in 7 days or less to dominate your local market</li>
<li>Custom design a step by step marketing plan you can put into effect immediately incorporating the most effective online AND offline strategies to generate a TON of buyer and seller leads</li>
<li>How to build a stellar marketing team so you can beat the pants off your competition</li>
<li>How to outsource your marketing to other people so you can focus on what’s important — BUYING PROPERTIES</li>
<li>Marketing round table so you can pick the brains of the top marketing experts who are literally buying and selling hundreds of houses each year collectively!</li>
</ul>
<p></p></div><div class="well"><p></p>
<h3>DAY 2: Evaluating Deals, Creating Buying Systems, &amp; Virtual Investing Tools</h3>
<ul>
<li>How to evaluate deals quickly and easily without leaving your house</li>
<li>Learn how to appraise the value of any property so you can have the confidence to make offers every time you visit a seller</li>
<li>Learn how we are using the power of the Internet to evaluate properties that are outside your area</li>
<li>Practice sessions on meeting with sellers and handling objections so you will never lose a deal to a competitor ever again</li>
<li>Learn how to structure creative offers so you can buy houses without any of your own cash</li>
<li>How to write contracts the right way so you eliminate all of the risk</li>
<li>Setting up and leveraging your Lead Management Systems</li>
<li>Hiring Virtual Assistants to evaluate your deals and streamline your entire lead generation process</li>
<li>Virtual Investing Tools that will help you make WAY MORE money in WAY LESS time</li>
<li>BRING YOUR ACTIVE DEALS! We will be doing live Wholesaling Case Studies where we will analyze them, structure them, and more importantly ensure they get accepted!!</li>
</ul>
<p></p></div><div class="well"><p></p>
<h3>DAY 3: New Rehabbing Strategies, Building Your Buyers List, &amp; Implementing The 9 Step Selling System</h3>
<ul>
<li>Learn the different approaches we are taking in today’s changing market to automate our Rehabbing projects, decrease our overhead, and increase our profits</li>
<li>Top marketing strategies (including using video, squeeze pages, landing pages and more) to build a massive builders list. These are the strategies we’ve used to add over 30,000 people to our buyers list in the last year alone</li>
<li>Advanced Online Tools we’ve implemented to channel leads and get our properties SOLD using the 9 Step Selling System</li>
<li>Practice sessions on meeting with sellers and handling objections so you will never lose a deal to a competitor ever again</li>
<li>Learn my top online and offline property marketing strategies to get your properties SOLD at lightning speed</li>
<li>Learn how to turn bad credit buyers into homeowners with 5 simple steps</li>
<li>How to get your properties sold without having to meet with buyers or show properties yourself</li>
<li>How to get investor buyers literally fawning all over you hoping you will sell them a property</li>
<li>How to step-by-step implement the 9 Stage Selling System to completely automate every phase of the selling process!</li>
</ul>
<p></p></div><div class="well"><p></p>
<h3>DAY 4: Financing Properties, Business Planning, Virtual Outsourcing, &amp; Bulk REO Property Opportunities</h3>
<ul>
<li>Learn 10 killer negotiating tips you can use to get any bank to discount</li>
<li>How to raise over $1,000,000 dollars in private funds so you will never have to worry about having enough money to fund your deals again</li>
<li>Learn how to set up Transactional Funding is a MUST in today’s wholesaling market</li>
<li>The changes in Creative Financing Strategies and what’s WORKING right now</li>
<li>Learn how to do deals in your IRA so you will never have to pay taxes again</li>
<li>Set goals for the upcoming year so you have an action plan when you leave</li>
<li>How to find and buy deeply discounted bulk REO properties, which is one of today’s HUGEST money making opportunities!</li>
<li>How to automate your biz by having people who can work around the clock for you!</li>
<li>What type of work you can and should be outsourcing virtually, and more importantly what you should not outsource virtually</li>
<li>The best companies to hire virtual assistants at dirt cheap prices</li>
<li>The best websites to post projects on and get others bidding</li>
<li>How to pre-screen and hire the best virtual assistants and service providers</li>
<li>How to set up a training system to get your VA’s trained quickly and correctly from day one!</li>
<li>Learn how to manage and pay your Virtual Assistant’s</li>
<li>Set goals for the upcoming year so you have an action plan when you leave!</li>
</ul>
<p></p></div><p></p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content -->
                                                        <div class="conf-event-details-footer">
                                                            <button class="btn btn-normal btn-sm conf-event-btn" ng-click="confForm.toggle(event.ID)">Close
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding" ng-click="confForm.select(event.ID, 'bootcamp')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]">Deselect</button>
                                                        </div>
                                                    </div>
                                                    <!-- / Bootcamp Details -->
                                                </div><!-- end ngRepeat: event in confBootcamps.events | orderObjectsBy: 'p2p_order_from' --><div ng-repeat="event in confBootcamps.events | orderObjectsBy: 'p2p_order_from'" class="conf-event-row ng-scope" fb-scroll-if="confForm.toggles[event.ID] === 'closed', confForm.toggles[event.ID]" ng-show="( event.meta.mwbc &amp;&amp; !confForm.formData.user.attendedMWBC ) || confForm.formData.user.attendedMWBC || !confBootcamps.mwbc">

                                                    <!-- Bootcamp Row -->
                                                    <div class="triangle-right"></div>
                                                    <div class="conf-event-listing clearfix">
                                                        <div class="conf-event-data">
                                                        <span ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-time reg-conflict" style="">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/20 9:00AM</span> -
                                                            <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/23 6:00PM</span>
                                                        </span>
                                                        <span class="event-label event-label-status">
                                                            <button ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="conf-event-txt btn-link reg-conflict" ng-click="confForm.toggle(event.ID)" style="">
                                                                <!-- ngIf: event.meta.mwbc && !confForm.formData.user.attendedMWBC && !(event.meta.can_view_MWBC_BLOCKED) -->
                                                                <span ng-bind="event.post_title" class="ng-binding">The Rehab Bootcamp: October 20th – 23rd, 2016 – San Diego, CA</span> <!-- ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><span class="halfling halfling-chevron-down ng-scope" ng-if="confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined"></span><!-- end ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><!-- ngIf: confForm.toggles[event.ID] === 'open' -->
                                                            </button>
                                                        </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                        <!-- ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                        <span class="event-label event-label-select">
                                                            <button class="btn btn-normal btn-sm conf-event-btn btn-state-disabled" title="View Event Information" ng-class="{ 'btn-state-toggled': confForm.toggles[event.ID] === 'open', 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-click="confForm.toggle(event.ID)" style=""><span class="glyphicon glyphicon-circle_info"></span>
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'bootcamp')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <!-- Bootcamp Row -->

                                                    <!-- Bootcamp Details -->
                                                    <div class="conf-event-details ng-hide" ng-show="confForm.toggles[event.ID] === 'open'">
                                                        <!-- ngIf: event.meta.mwbc && !confForm.formData.user.attendedMWBC && !(event.meta.can_view_MWBC_BLOCKED) -->
                                                        <!-- ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection1content_hide &amp;&amp; event.meta.evsection1content">
                                                            <h4 ng-bind="event.meta.ev_section_1_title" class="ng-binding">Date, Time &amp; Location</h4>
                                                            <span ng-bind-html="event.meta.evsection1content" class="ng-binding"><p><strong>Date:</strong> October 20th - 23rd, 2016<br>
<strong>Location:</strong> Sheraton San Diego Hotel &amp; Marina<br>
<strong>Address:</strong> 1380 Harbor Island Drive - San Diego, CA 92101 <br>
<strong>Phone:</strong> (619) 291-2900 <br>
<strong>Classroom Times:</strong> 9am to 6pm each day (times subject to change)</p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content -->
                                                        <!-- ngIf: event.post_content --><div class="conf-event-details-item ng-scope" ng-if="event.post_content">
                                                            <h4 ng-bind="event.meta.ev_section_0_title" class="ng-binding">Event Description</h4>

                                                            <!-- ngIf: event.post_thumbnail --><div ng-if="event.post_thumbnail" class="thumb event-thumb ng-binding ng-scope" ng-bind-html="event.post_thumbnail"><img width="300" height="184" src="http://mastery.com/wp-content/uploads/2016/08/San-Diego-1-300x184.jpg" class="attachment-medium wp-post-image" alt="San Diego 1"></div><!-- end ngIf: event.post_thumbnail -->
                                                            <span ng-bind-html="event.post_content" class="ng-binding"><p><span style="font-weight: 400;">After you attend the Wholesaling Bootcamp, like many Mastery students, you’ll most likely move on to bigger and more involved real estate transactions. Understanding all the inner workings of rehabbing is essential to your long-term success – which is precisely why we created a 3-day “Rehabbing Bootcamp.”&nbsp;</span>This intensive bootcamp is taught by JD Esajian who has rehabbed and sold hundreds of homes across the country.</p>
<p>This bootcamp is our most hands-on training by far, as we devote lots of class time at actual job sites that are in different phases of construction. Clearly there is simply no substitute for learning in the field and actually seeing firsthand which systems you’ll use to manage your rehab projects. This is invaluable information to know, because the profitability of your projects often depends on your construction knowledge and systems for creating a detailed scope of work, material and labor cost management, and the efficiencies of how you manage the construction process itself.</p>
<p><strong><span style="font-weight: 400;">You will leave this bootcamp with a comprehensive understanding of everything you need to know about managing rehab projects of all sizes and the systems you need to leverage to be successful. These strategies are effective in any area of the country and you can run your business from the comfort of your desk at home. It’s no wonder our Rehabbing Bootcamp is considered by many Mastery students to be the most beneficial training we offer.</span></strong></p>
</span>
                                                        </div><!-- end ngIf: event.post_content -->
                                                        <!-- ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection4content_hide &amp;&amp; event.meta.evsection4content">
                                                            <h4 ng-bind-html="event.meta.ev_section_4_title" class="ng-binding">Hotel Information</h4>

                                                            <div class="panel-body">
                                                                <div class="alert alert-success ng-binding" ng-bind-html="event.meta.evsection4content"><p>We have a LIMITED block of guest rooms locked in at a discounted rate of $189 per night under the group name, FortuneBuilders. Once the room block is full, standard rates apply so PLEASE book your accommodations at the Sheraton San Diego Hotel &amp; Marina right away to take advantage of this discounted rate as it is first come, first serve! Make your reservation by calling 1-877-734-2726 or by clicking this link <a href="https://www.starwoodmeeting.com/Book/FortuneBuildersBootcampSummit" target="_blank">Book Your Hotel</a>.</p>
</div>
                                                            </div>
                                                        </div><!-- end ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content -->
                                                        <!-- ngIf: !event.meta.evsection5content_hide && event.meta.evsection5content -->
                                                        <!-- ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection6content_hide &amp;&amp; event.meta.evsection6content">
                                                            <h4 ng-bind="event.meta.ev_section_6_title" class="ng-binding">Event Agenda</h4>
                                                            <span ng-bind-html="event.meta.evsection6content" class="ng-binding"><p></p><div class="well"><p></p>
<h3>DAY 1: The Top Techniques to find Houses, Contractors, and the system to follow to get the Best Rehab Team</h3>
<ul>
<li>Learn the latest Marketing techniques and online strategies to get all the deals you will ever need to Rehab or Wholesale</li>
<li>Learn How to Prescreen Contractors with A System (Live Role Playing with Contractors)</li>
<li>Learn How to Market For, Locate and Identify the top 5% of quality contractor to make any Rehab a success</li>
<li>Step by Step breakdown of the Rehabbing Process</li>
<li>Learn the order of work on every house, starting with the paperwork needed for any project and all contractors</li>
<li> How to Formulate A Scope of Work</li>
<li>How to Formulate A Payment Schedule to keep every job on track</li>
<li>Current Material and Labor costs breakdown so you can save on your next Rehab and estimate repairs for your next Wholesale</li>
<li>Learn the Rehab techniques that will make you the most successful Wholesaler in your State!!</li>
<li>Learn the techniques to estimate renovation costs accurately in 15 minutes or less for your Rehab</li>
</ul>
<p></p></div><br>
<div class="well"><p></p>
<h3>DAY 2: On Site Live Bus Tour</h3>
<ul>
<li>Q&amp;A with our Contractors on site at the projects themselves</li>
<li>A live Bus Tour of our existing projects to learn all the Rehab ins &amp; outs ON SITE</li>
<li>Job Site Rehab and Contractor Evaluations – ON SITE!!!</li>
<li>Understand the principals of Redesigning Spatial Layouts and Optimize Floor Plans at actual projects on Site</li>
<li>Learn How to Introduce, Sign, and Implement the 6 Essential Documents to sign with every contractor on every job!</li>
<li>  Learn How to Manage the exclusive FortuneBuilder’s Rehab System to Stay on Schedule and keep Under Budget</li>
<li> Identify the problem houses that never make any investor money and the reasons houses don’t sell</li>
<li>  Understand how to handle violations &amp; citations from building department, including Lead &amp; Mold and how you can make a fortune with these deals!!!</li>
<li>  Identify the most common mistakes &amp; projects that new investors under estimate and NEED TO STAY AWAY FROM, these will end your career before it even gets started</li>
</ul>
<p></p></div><br>
<div class="well"><p></p>
<h3>DAY 3: Construction Techniques To Sell All Your Houses &amp; The Insurance Needed To Protect Your Real Estate Fortune</h3>
<ul>
<li>Sizzle features to implement and install in every Rehab to sell your house fast</li>
<li> Learn the 5 top kitchens’ that can sell any house!!!</li>
<li>  Make every buyer write an offer once they see the Bathrooms you have installed form our Scope of Work</li>
<li> How to check buyer’s credit, mortgage, and financing to only take you Deal off the market with a qualified buyer</li>
<li> Learn what legal structure and entity is best to make the most money and protect your assets</li>
<li> The top 5 things to do before listing any house to insure it is ready to sell</li>
<li>How to take advantage of the Excess Surplus Market to Insure ALL your burned out, bombed, uninsurable houses!!</li>
</ul>
<p></p></div><br>
<div class="well"><p></p>
<h3>DAY 4: Office Set Up &amp; Accounting Software and Procedures to count every dollar you earn, &amp; how to take the Residential Redevelopment Business to the Next Level on any residential or Commercial deal!!</h3>
<ul>
<li>Learn how Build and grow relationships with Contractors so you can grow your Rehab Team</li>
<li>   Setting up your Rehab Office from the paperwork filing down to the accounting</li>
<li> Understanding the best system for Account Payables and Account Receivables</li>
<li>Learn the available accounting systems and the best one for you to organize, and run your whole Real Estate business from Rehabbing, Wholesaling, and Landlording</li>
<li> How to use Quickbooks and what are the most important reports for every investor.</li>
<li>Learn how to set the Rules, create a Mission Statement, and Motivate you and your Team</li>
<li> The most important steps to identify and examine to partner with anyone to make money</li>
<li>How to hire your first Employee</li>
<li>Finally what we are doing in today’s market to stay ahead of the curve on building a buyers list and selling houses before we even own them!!</li>
</ul>
<p></p></div><p></p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content -->
                                                        <div class="conf-event-details-footer">
                                                            <button class="btn btn-normal btn-sm conf-event-btn" ng-click="confForm.toggle(event.ID)">Close
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'bootcamp')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled" style="">Select</button>
                                                        </div>
                                                    </div>
                                                    <!-- / Bootcamp Details -->
                                                </div><!-- end ngRepeat: event in confBootcamps.events | orderObjectsBy: 'p2p_order_from' --><div ng-repeat="event in confBootcamps.events | orderObjectsBy: 'p2p_order_from'" class="conf-event-row ng-scope" fb-scroll-if="confForm.toggles[event.ID] === 'closed', confForm.toggles[event.ID]" ng-show="( event.meta.mwbc &amp;&amp; !confForm.formData.user.attendedMWBC ) || confForm.formData.user.attendedMWBC || !confBootcamps.mwbc">

                                                    <!-- Bootcamp Row -->
                                                    <div class="triangle-right"></div>
                                                    <div class="conf-event-listing clearfix">
                                                        <div class="conf-event-data">
                                                        <span ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-time reg-conflict" style="">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/21 9:00AM</span> -
                                                            <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/23 6:00PM</span>
                                                        </span>
                                                        <span class="event-label event-label-status">
                                                            <button ng-class="{'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="conf-event-txt btn-link reg-conflict" ng-click="confForm.toggle(event.ID)" style="">
                                                                <!-- ngIf: event.meta.mwbc && !confForm.formData.user.attendedMWBC && !(event.meta.can_view_MWBC_BLOCKED) -->
                                                                <span ng-bind="event.post_title" class="ng-binding">The Money Academy: October 21st - 23rd, 2016 - San Diego, CA</span> <!-- ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><span class="halfling halfling-chevron-down ng-scope" ng-if="confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined"></span><!-- end ngIf: confForm.toggles[event.ID] === 'closed' || confForm.toggles[event.ID] === undefined --><!-- ngIf: confForm.toggles[event.ID] === 'open' -->
                                                            </button>
                                                        </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                        <!-- ngIf: event.meta.ev_section_2_status !== 'active' --><span ng-if="event.meta.ev_section_2_status !== 'active'" ng-class="{ 'reg-conflict': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) }" class="event-label event-label-status ng-scope reg-conflict" style="">
                                                            <span class="glyphicon glyphicon-warning_sign" style="font-size:1.2em; margin-right:.2em;"></span>
<!--                                                            <span ng-if="event.meta.ev_section_2_status === 'active'">Open</span>-->
                                                            <!-- ngIf: event.meta.ev_section_2_status !== 'active' --><span ng-if="event.meta.ev_section_2_status !== 'active'" class="ng-scope">
                                                                <!-- ngIf: event.meta.ev_section_2_status === 'closed' --><span ng-if="event.meta.ev_section_2_status === 'closed'" class="ng-scope">At Capacity</span><!-- end ngIf: event.meta.ev_section_2_status === 'closed' -->
                                                                <!-- ngIf: event.meta.ev_section_2_status === 'waiting' -->
                                                                <!-- ngIf: event.meta.ev_section_2_status !== 'closed' && event.meta.ev_section_2_status !== 'waiting' -->
                                                            </span><!-- end ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                        </span><!-- end ngIf: event.meta.ev_section_2_status !== 'active' -->
                                                        <span class="event-label event-label-select">
                                                            <button class="btn btn-normal btn-sm conf-event-btn btn-state-disabled" title="View Event Information" ng-class="{ 'btn-state-toggled': confForm.toggles[event.ID] === 'open', 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-click="confForm.toggle(event.ID)"><span class="glyphicon glyphicon-circle_info"></span>
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'bootcamp')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled">Select</button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <!-- Bootcamp Row -->

                                                    <!-- Bootcamp Details -->
                                                    <div class="conf-event-details ng-hide" ng-show="confForm.toggles[event.ID] === 'open'">
                                                        <!-- ngIf: event.meta.mwbc && !confForm.formData.user.attendedMWBC && !(event.meta.can_view_MWBC_BLOCKED) -->
                                                        <!-- ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection1content_hide &amp;&amp; event.meta.evsection1content">
                                                            <h4 ng-bind="event.meta.ev_section_1_title" class="ng-binding">Date, Time &amp; Location</h4>
                                                            <span ng-bind-html="event.meta.evsection1content" class="ng-binding"><p><strong>Date:</strong> October 21st - 23rd, 2016<br>
<strong>Location:</strong> Sheraton San Diego Hotel &amp; Marina<br>
<strong>Address:</strong> 1380 Harbor Island Drive - San Diego, CA 92101<br>
<strong>Phone:</strong> (619) 291-2900 <br>
<strong>Classroom Times:</strong> 9am to 6pm each day (times subject to change)</p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection1content_hide && event.meta.evsection1content -->
                                                        <!-- ngIf: event.post_content --><div class="conf-event-details-item ng-scope" ng-if="event.post_content">
                                                            <h4 ng-bind="event.meta.ev_section_0_title" class="ng-binding">Event Descripton</h4>

                                                            <!-- ngIf: event.post_thumbnail --><div ng-if="event.post_thumbnail" class="thumb event-thumb ng-binding ng-scope" ng-bind-html="event.post_thumbnail"><img width="300" height="184" src="http://mastery.com/wp-content/uploads/2016/08/San-Diego-1-300x184.jpg" class="attachment-medium wp-post-image" alt="San Diego 1"></div><!-- end ngIf: event.post_thumbnail -->
                                                            <span ng-bind-html="event.post_content" class="ng-binding"><p><strong><span style="font-weight: 400;">At this revealing, eye-opening training, Paul Esajian, Co-Founder of FortuneBuilders, and Jeff Carter from Grand Coast Capital, teach you all the ins and outs of securing private and hard money to finance your real estate investing deals. You’ll leave this event well-informed and confident in your ability to find lenders who are ready and willing to work with you. You’ll learn how to do what it takes to make a great first impression when you meet with lenders. You’ll learn what you need to know about how to conduct yourself during the interviewing and qualification process to create successful, long lasting relationships with lenders. You’ll also understand the logistics of executing a loan – which will prepare you to scale your business, dramatically increase the number of real estate transactions you complete, and most importantly, make more money!</span></strong></p>
</span>
                                                        </div><!-- end ngIf: event.post_content -->
                                                        <!-- ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection4content_hide &amp;&amp; event.meta.evsection4content">
                                                            <h4 ng-bind-html="event.meta.ev_section_4_title" class="ng-binding">Hotel Information</h4>

                                                            <div class="panel-body">
                                                                <div class="alert alert-success ng-binding" ng-bind-html="event.meta.evsection4content"><p>We have a LIMITED block of guest rooms locked in at a discounted rate of $189 per night under the group name, FortuneBuilders. Once the room block is full, standard rates apply so PLEASE book your accommodations at the Sheraton San Diego Hotel &amp; Marina right away to take advantage of this discounted rate as it is first come, first serve! Make your reservation by calling 1-877-734-2726 or by clicking this link <a href="https://www.starwoodmeeting.com/Book/FortuneBuildersBootcampSummit" target="_blank">Book Your Hotel</a>.</p>
</div>
                                                            </div>
                                                        </div><!-- end ngIf: !event.meta.evsection4content_hide && event.meta.evsection4content -->
                                                        <!-- ngIf: !event.meta.evsection5content_hide && event.meta.evsection5content -->
                                                        <!-- ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content --><div class="conf-event-details-item ng-scope" ng-if="!event.meta.evsection6content_hide &amp;&amp; event.meta.evsection6content">
                                                            <h4 ng-bind="event.meta.ev_section_6_title" class="ng-binding">Event Agenda</h4>
                                                            <span ng-bind-html="event.meta.evsection6content" class="ng-binding"><p></p><div class="well"><p></p>
<h3>Topics Covered at this Event</h3>
<ul>
<li>How to prepare before raising money</li>
<li>How to find lenders</li>
<li>How to interview and qualify lenders</li>
<li>How to present to lenders</li>
<li>Terms to offers</li>
<li>Documents needed</li>
<li>Self-Directed Retirement Accounts</li>
</ul>
<p></p></div><p></p>
</span>
                                                        </div><!-- end ngIf: !event.meta.evsection6content_hide && event.meta.evsection6content -->
                                                        <div class="conf-event-details-footer">
                                                            <button class="btn btn-normal btn-sm conf-event-btn" ng-click="confForm.toggle(event.ID)">Close
                                                            </button>
                                                            <button class="btn btn-normal btn-sm conf-event-btn ng-binding btn-state-disabled" ng-click="confForm.select(event.ID, 'bootcamp')" ng-class="{ 'btn-state-disabled': confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID]) }" ng-disabled="confForm.isTimeOverlap(event.meta.ev_section_0_startdate,event.meta.ev_section_0_enddate,event.ID) || confForm.isStatusDisabled(event.meta.ev_section_2_status, confForm.selects[event.ID])" ng-bind="confForm.selects[event.ID]" disabled="disabled">Select</button>
                                                        </div>
                                                    </div>
                                                    <!-- / Bootcamp Details -->
                                                </div><!-- end ngRepeat: event in confBootcamps.events | orderObjectsBy: 'p2p_order_from' -->
                                            </div>
                                            <!-- / Bootcamp Rows -->

                                        </div>
                                    </div><!-- end ngIf: !confBootcamps.bootcampsEmpty -->
                                </article>
                                <!-- / Conference Bootcamps -->

                                <!-- Registration Form -->
                                <article class="panel panel-mastery conf-form" style="opacity: 1;">
                                    <header class="panel-heading ng-binding" ng-bind="confForm.formTitle">Registration Submission</header>
                                    <div class="panel-body" style="padding-bottom: 0;">
                                        <div class="alert alert-warning"><strong><p><em>Completing this form only
                                                        reserves a seat for yourself, Your Mastery Partner(s) must
                                                        register separately through their own Mastery Account(s). If you
                                                        need to add a Mastery Partner, please contact <a href="mailto:support@fortunebuildersmastery.com" title="support@fortunebuildersmastery.com" target="_blank">Mastery Support</a></em></p></strong>
                                        </div>

                                        <!-- Form Fields -->
                                        <div class="formitems row">
                                            <div class="col-md-6">
                                                <div class="formitem">
                                                    <label for="Contact0FirstName">First Name:</label>
                                                    <input size="15" type="text" class="name required field form-control ng-pristine ng-untouched ng-valid ng-not-empty" id="Contact0FirstName" ng-model="confForm.formData.user.Contact0FirstName" ng-readonly="true" readonly="readonly">
                                                </div>
                                                <div class="formitem">
                                                    <label for="Contact0LastName">Last Name:</label>
                                                    <input size="15" type="text" class="name required field form-control ng-pristine ng-valid ng-not-empty ng-touched" id="Contact0LastName" ng-model="confForm.formData.user.Contact0LastName" ng-readonly="true" readonly="readonly" style="">
                                                </div>
                                                <div class="formitem">
                                                    <label for="Contact0Email">Email Address:</label>
                                                    <input size="15" type="text" class="email required field form-control ng-pristine ng-untouched ng-valid ng-not-empty" id="Contact0Email" ng-model="confForm.formData.user.Contact0Email" ng-readonly="true" readonly="readonly">
                                                </div>
                                                <div class="formitem">
                                                    <label for="Contact0Phone1">Phone Number:</label>
                                                    <input size="15" type="text" class="phone required field form-control ng-pristine ng-untouched ng-valid ng-not-empty" id="Contact0Phone1" ng-model="confForm.formData.user.Contact0Phone1" ng-readonly="true" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="formitem">
                                                    <label for="Contact0StreetAddress1">Street Address:</label>
                                                    <input size="15" type="text" class="address required field form-control ng-pristine ng-untouched ng-valid ng-not-empty" id="Contact0StreetAddress1" ng-model="confForm.formData.user.Contact0StreetAddress1" ng-readonly="true" readonly="readonly">
                                                </div>
                                                <div class="formitem">
                                                    <label for="Contact0City">City:</label>
                                                    <input size="15" type="text" class="city required field form-control ng-pristine ng-untouched ng-valid ng-not-empty" id="Contact0City" ng-model="confForm.formData.user.Contact0City" ng-readonly="true" readonly="readonly">
                                                </div>
                                                <div class="formitem">
                                                    <label for="Contact0State">State:</label>
                                                    <input size="15" type="text" class="state required field form-control ng-pristine ng-untouched ng-valid ng-not-empty" id="Contact0State" ng-model="confForm.formData.user.Contact0State" ng-readonly="true" readonly="readonly">
                                                </div>
                                                <div class="formitem">
                                                    <label for="Contact0PostalCode">Postal Code:</label>
                                                    <input size="15" type="text" class="zip required field form-control ng-pristine ng-untouched ng-valid ng-not-empty" id="Contact0PostalCode" ng-model="confForm.formData.user.Contact0PostalCode" ng-readonly="true" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="padding: 15px; clear: both;">
                                                <em>If you need to update your information visit your <a title="Member Profile" href="http://mastery.com/members/admin">Member
                                                        Profile</a>.</em>
                                            </div>
                                        </div>
                                        <!-- / Form Fields -->

                                        <!-- Registration Summary -->
                                        <!-- ngIf: !confForm.regEventsEmpty || !confForm.addEventsEmpty || !confForm.waitEventsEmpty || !confForm.removeEventsEmpty --><div class="reg-summary ng-scope" style="clear: both;" ng-if="!confForm.regEventsEmpty || !confForm.addEventsEmpty || !confForm.waitEventsEmpty || !confForm.removeEventsEmpty">

                                            <!-- Registered Events -->
                                            <!-- ngIf: !confForm.regRegisteredEventsEmpty -->
                                            <!-- / Registered Events -->

                                            <!-- Registered Wait Events -->
                                            <!-- ngIf: !confForm.regWaitingEventsEmpty -->
                                            <!-- / Registered Wait Events -->

                                            <!-- Added Events -->
                                            <!-- ngIf: !confForm.addEventsEmpty --><div class="reg-add ng-scope" ng-if="!confForm.addEventsEmpty">
                                                <div class="reg-heading">You Have Selected To Register For:</div>
                                                <div class="reg-events">
                                                    <!-- ngRepeat: event in confForm.formData.addEvents | orderObjectsBy: 'meta.ev_section_0_startdate' --><div class="conf-event-listing clearfix ng-scope" ng-repeat="event in confForm.formData.addEvents | orderObjectsBy: 'meta.ev_section_0_startdate'" style="">
                                                        <div class="triangle-right"></div>
                                                        <div class="conf-event-data">
                                                        <span class="event-label event-label-time">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/19 9:00AM</span> - <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/19 6:00PM</span>
                                                        </span>
                                                        <span class="event-label event-label-status" style="text-transform: none;">
                                                            <span class="conf-event-txt ng-binding" ng-bind="event.post_title">Rental Property Intensive: October 19th, 2016 - San Diego, CA</span>
                                                        </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                            <span class="event-label event-label-select">
                                                                <button class="btn btn-normal btn-sm conf-event-btn ng-binding" ng-click="confForm.select(event.ID, 'event')" ng-bind="confForm.selects[event.ID]">Deselect</button>
                                                            </span>
                                                        </div>
                                                    </div><!-- end ngRepeat: event in confForm.formData.addEvents | orderObjectsBy: 'meta.ev_section_0_startdate' --><div class="conf-event-listing clearfix ng-scope" ng-repeat="event in confForm.formData.addEvents | orderObjectsBy: 'meta.ev_section_0_startdate'">
                                                        <div class="triangle-right"></div>
                                                        <div class="conf-event-data">
                                                        <span class="event-label event-label-time">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                            <span class="event-label-start-time ng-binding" ng-bind="event.meta.event_human_start_time">10/20 9:00AM</span> - <span class="event-label-end-time ng-binding" ng-bind="event.meta.event_human_end_time">10/23 6:00PM</span>
                                                        </span>
                                                        <span class="event-label event-label-status" style="text-transform: none;">
                                                            <span class="conf-event-txt ng-binding" ng-bind="event.post_title">The Wholesaling Bootcamp: October 20th – 23rd, 2016 – San Diego, CA</span>
                                                        </span>
                                                        </div>
                                                        <div class="conf-event-options">
                                                            <span class="event-label event-label-select">
                                                                <button class="btn btn-normal btn-sm conf-event-btn ng-binding" ng-click="confForm.select(event.ID, 'event')" ng-bind="confForm.selects[event.ID]">Deselect</button>
                                                            </span>
                                                        </div>
                                                    </div><!-- end ngRepeat: event in confForm.formData.addEvents | orderObjectsBy: 'meta.ev_section_0_startdate' -->
                                                </div>
                                            </div><!-- end ngIf: !confForm.addEventsEmpty -->
                                            <!-- / Added Events -->

                                            <!-- WaitList Events -->
                                            <!-- ngIf: !confForm.waitEventsEmpty -->
                                            <!-- / Added Events -->

                                            <!-- Removed Events -->
                                            <!-- ngIf: !confForm.removeEventsEmpty -->
                                            <!-- / Removed Events -->
                                        </div><!-- end ngIf: !confForm.regEventsEmpty || !confForm.addEventsEmpty || !confForm.waitEventsEmpty || !confForm.removeEventsEmpty -->
                                        <!-- / Registration Submission -->
                                    </div>

                                    <!-- Registration Submission -->
                                    <div class="panel-footer">
                                        <em class="submit-warning ng-binding" ng-bind="confForm.submitText"></em>
                                        <button class="btn btn-final ng-binding" ng-class="{'btn-state-disabled': confForm.addEventsEmpty &amp;&amp; confForm.waitEventsEmpty &amp;&amp; confForm.removeEventsEmpty}" ng-click="confForm.submit();" ng-disabled="confForm.addEventsEmpty &amp;&amp; confForm.waitEventsEmpty &amp;&amp; confForm.removeEventsEmpty" ng-bind="confForm.submitBtnText" style="">Submit</button>
                                    </div>
                                </article>

                            </form>