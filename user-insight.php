<?php
ini_set("memory_limit", "-1");
set_time_limit(0);
require_once '../wp-load.php';

include 'jqueryMaterial.php';
include 'arrayToTable.php';

$uid = 47611;
if(isset($_GET['uid']))
	$uid = $_GET['uid'];

$user = get_user_by('id', $uid);
$level = fb_get_user_level($uid);

include 'materialUserDisplay.php';

//----------
if(isset($_GET['show_tags']))
{
	$utagsSql = 'SELECT * FROM fb_all_infusionsoft_tags WHERE ID IN  (' . get_usermeta($uid, 'Groups') . ');';
	$utags = $wpdb->get_results($utagsSql, ARRAY_A);

	echo "<h3>User Tags</h3>";
	echo '<div class="card card-block card-info">    
	    <p class="card-text">'.$utagsSql.'</p>
	</div>';	
	arrayToTable($utags);
	
	$user_tags = explode(',', get_usermeta($uid, 'Groups'));

	echo "<h4>Missing Tags</h3>";

	$existentTags = [];
	foreach ($utags as $ut) {
		$existentTags[] = $ut['id'];
	}	
	//echo "<h2>";
	
	foreach ($user_tags as $t) {		
		if(!in_array($t, $existentTags))
			echo '<span class="tag tag-danger" style="font-size: 14px;">' . $t . '</span> ';
	}
	//echo "</h2>";

	//$notInTagsSql = 'select * from fb_all_infusionsoft_tags where ID NOT IN ('. $user_tags .');';

}//----------
if(isset($_GET['show_usermeta']))
{
	$umSql = 'SELECT * FROM wp_usermeta WHERE user_id = ' . $uid . ';';
	$um = $wpdb->get_results($umSql, ARRAY_A);

	echo "<h3>User Metas</h3>";
	echo '<div class="card card-block card-warning">    
	    <p class="card-text">'.$umSql.'</p>
	</div>';	
	arrayToTable($um);
}

if(isset($_GET['show_userinfo']))
{
	// $umSql = 'SELECT * FROM wp_usermeta WHERE user_id = ' . $uid . ';';
	// $um = $wpdb->get_results($umSql, ARRAY_A);

	$uinfo = fb_get_userinfo($uid);
//echo "<pre>". print_r($uinfo, true) . "</pre>";die();
	echo "<h3>User Info</h3>";
	echo '<div class="card card-block card-info">    
	</div>';	
	echo "<pre>". print_r($uinfo, true) . "</pre>";//die();
	//arrayToTable($uinfo);
}
//TODO:
//AL
//FB_Activity::
//CALLS
//--GET CALLS -- as coach or student -- if I'm coach search by coach, coach status

if(isset($_GET['show_calls'])){

	$callsStatus = '0,1,2,3,4';
	if(isset($_GET['calls_status'])){
		$callsStatus = $_GET['calls_status'];
	}
	$callsKey = 'student_id';
	if(in_array($level, ['coach', 'ic adviser'])){
		$callsKey = 'coach_id';	
	}

	$callsSql = 'SELECT * FROM wp_fb_coaching_calls WHERE ' . $callsKey . ' = ' . $uid . ' AND status IN ('. $callsStatus.') ORDER BY id DESC LIMIT 100';

	$calls = $wpdb->get_results($callsSql, ARRAY_A);

	//echo "<pre>". print_r($calls, true) . "</pre>";
	echo "<h3>User Calls</h3>";
	echo '<div class="card card-block card-info">    
	    <p class="card-text">'.$callsSql.'</p>
	</div>';
	arrayToTable($calls);	
}

//LMS stuff

