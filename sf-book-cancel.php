<?php 

ini_set("memory_limit", "-1");
set_time_limit(0);

error_reporting(E_ALL);
ini_set("display_errors", 1); 

// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../wp-load.php';

include 'jqueryMaterial.php';

//tmp tomorrow!
$cid = $_GET['cid'];
$ev = fb_srv_googleCancelSlot($cid);

echo "Cancelled $cid? ";
echo is_object($ev);

$gEventStr = '';
if($ev){
  $gEventStr = ',gc:' . $ev->getId();
}
echo " # Helper text $gEventStr";

return;

?>
<h3>Cancelling</h3>
<script>
   jQuery(document).ready(function() {
       $.ajax({
           url: '/wp-admin/admin-ajax.php',
           data: {action: 'cancel_call',  
           method: 'post',          
           coachingcall_id: <?php echo $cid ?>
         },
           success: function (output) {
            $('.res').append(output);
           }
       });
   });
</script>
<div class="res"></div>